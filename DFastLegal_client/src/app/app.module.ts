//Core Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//3rd party
import {NgxPaginationModule, PaginationService, } from 'ngx-pagination'; // <-- import the module

//routing
import {routing} from'./app.routing';

//Components
import { AppComponent } from './app.component';
import { TestComponentComponent } from './components/test-component/test-component.component';
import {HeaderComponent} from'./components/header/header.component';
import {FooterComponent}from'./components/footer/footer.component';
import {HomeComponent}from'./components/home/home.component';
import {ClientInquireComponent}from'./components/client-inquire/client-inquire.component';
import {InquireThankyouComponent}from'./components/inquire-thankyou/inquire-thankyou.component';
import {LawyerInquireComponent}from'./components/lawyer-inquire/lawyer-inquire.component';
import {LawyersComponent}from'./components/lawyers/lawyers.component';
import {LawyerProfileComponent}from'./components/lawyer-profile/lawyer-profile.component';
import {LawyerSignUpComponent} from'./components/lawyer-signup/lawyer-signup.component';
import {LawyerProfileEditComponent}from'./components/lawyer-profile-edit/lawyer-profile-edit.component';
import {SignupModalComponent}from'./components/signup-modal/signup-modal.component';
import {ClientProfileEditComponent}from'./components/client-profile-edit/client-profile-edit.component';
import {QuestionsComponent}from'./components/questions/questions.component';
import {CaseDetailFormComponent}from'./components/case-detail-form/case-detail-form.component';
import {SelectLawyersComponent} from'./components/select-lawyers/select-lawyers.component';
import {ViewLawyerProfileComponent}from'./components/view-lawyer-profile/view-lawyer-profile.component';
import {CaseConfirmComponent}from'./components/case-confirm/case-confirm.component';
import {CaseIssuedComponent}from'./components/case-issued/case-issued.component';
import {ClientCasesComponent}from'./components/client-cases/client-cases.component';
import {CaseDetailComponent}from'./components/case-detail/case-detail.component';
import {CaseResponseComponent}from'./components/case-response/case-response.component';
import {CaseDeclinedComponent}from'./components/case-declined/case-declined.component';
import {CaseSelectPaymentComponent}from'./components/case-select-payment/case-select-payment.component';
import {CaseConfirmPaymentComponent} from'./components/case-confirm-payment/case-confirm-payment.component';
import {PaymentSuccessComponent}from'./components/payment-success/payment-success.component';
import {LawyerCaseComponent}from'./components/lawyer-case/lawyer-case.component';
import {SelectedLawyerProfileComponent}from'./components/selected-lawyer-profile/selected-lawyer-profile.component';
import {HelpAndSupportComponent}from'./components/help-and-support/help-and-support.component';
import {ForgotPasswordComponent}from'./components/forgot-password/forgot-password.component';
import {OurServicesComponent}from'./components/our-services/our-services.component';
import {PricingComponent}from'./components/pricing/pricing.component';
import {FAQSComponent}from'./components/faqs/faqs.component';
import {TempBetaModalComponent}from'./components/temp-beta-modal/temp-beta-modal.component';
import { UnsubscribedComponent } from './components/unsubscribed/unsubscribed.component';



//Child Components
import {ShortLawyerProfileComponent}from'./components/_child-components/short-lawyer-profile/short-lawyer-profile.component';
import {ShortCaseDetailComponent}from'./components/_child-components/short-case-detail/short-case-detail.component';
import {FullCaseDetailComponent} from'./components/_child-components/full-case-detail/full-case-detail.component';
import {FullLawyerProfileComponent}from'./components/_child-components/full-lawyer-profile/full-lawyer-profile.component';
import {ClientProfileComponent}from'./components/_child-components/client-profile/client-profile.component';
import {UserContactComponent}from'./components/_child-components/user-contact/user-contact.component';
import {FullCardDetailComponent}from'./components/_child-components/full-card-detail/full-card-detail.component';
import {FullAddressComponent}from'./components/_child-components/full-address/full-address.component';
import { ContactFeeComponent } from './components/_child-components/contact-fee/contact-fee.component';


//Services
import {UserService}from'./services/user.service';
import {AppLanguageService}from'./services/app-language.service';
import {ParseObjService}from'./services/parse-object.service';
import {ClientSideService}from'./services/client-side.service';
import {NotificationService}from'./services/notification.service';
import {LawyerSideService}from'./services/lawyer-side.service';
import {PaymentService}from'./services/payment.service';
import {EmailService} from'./services/email.service';
import {TagsService}from './services/tags.service';

@NgModule({
    declarations: [
        AppComponent,
        TestComponentComponent,
        HeaderComponent,
        FooterComponent,
        HomeComponent,
        ClientInquireComponent,
        InquireThankyouComponent,
        LawyerInquireComponent,
        LawyersComponent,
        LawyerProfileComponent,
        LawyerSignUpComponent,
        LawyerProfileEditComponent,
        SignupModalComponent,
        ClientProfileEditComponent,
        QuestionsComponent,
        CaseDetailFormComponent,
        SelectLawyersComponent,
        ViewLawyerProfileComponent,
        CaseConfirmComponent,
        CaseIssuedComponent,
        ClientCasesComponent,
        CaseDetailComponent,
        CaseResponseComponent,
        CaseDeclinedComponent,
        CaseSelectPaymentComponent,
        CaseConfirmPaymentComponent,
        PaymentSuccessComponent,
        LawyerCaseComponent,
        SelectedLawyerProfileComponent,
        HelpAndSupportComponent,
        ForgotPasswordComponent,
        OurServicesComponent,
        PricingComponent,
        FAQSComponent,
        TempBetaModalComponent,
        //Child Components
        ShortLawyerProfileComponent,
        ShortCaseDetailComponent,
        FullCaseDetailComponent,
        FullLawyerProfileComponent,
        ClientProfileComponent,
        UserContactComponent,
        FullCardDetailComponent,
        FullAddressComponent,
        ContactFeeComponent,
        UnsubscribedComponent

    ],
    imports: [
        BrowserModule.withServerTransition({"appId":'dfastlegal-ssr'}),
        FormsModule,
        HttpModule,
        NgxPaginationModule,
        routing
    ],
    providers: [
        UserService,
        AppLanguageService,
        ParseObjService,
        ClientSideService,
        NotificationService,
        LawyerSideService,
        PaginationService,
        PaymentService,
        EmailService,
        TagsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
