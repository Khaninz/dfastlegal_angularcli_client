﻿export const EXPERTISES: any[] = [
    { "expValue": "LIQUIDATION", "txt": { "th": "การเลิกกิจการ", "en": "Liquidation law" } },
    { "expValue": "PERMIT_DOCS", "txt": { "th": "การขอใบอนุญาตต่างๆ", "en": "Permission Docs" } },
    { "expValue": "INSURANCE", "txt": { "th": "กฏหมายประกันภัย", "en": "Insurance law" } },
    { "expValue": "INTER_TRADE", "txt": { "th": "กฎหมายการค้าระหว่างประเทศ", "en": "Intl. trade law" } },
    { "expValue": "ENVIRONMENT", "txt": { "th": "กฏหมายสิ่งแวดล้อม", "en": "Environment law" } },
    { "expValue": "PUBLIC", "txt": { "th": "กฏหมายมหาชน", "en": "Public law" } },
    { "expValue": "CRIMINAL", "txt": { "th": "กฏหมายละเมิด/อาญา", "en": "Criminal law" } },
    { "expValue": "BANKRUPTCY", "txt": { "th": "กฏหมายล้มละลายและฟื้นฟูกิจการ", "en": "Bankruptcy law" } },
    { "expValue": "MERGE", "txt": { "th": "การควบรวมกิจการ", "en": "M&A law" } },
    { "expValue": "TAX", "txt": { "th": "กฏหมายภาษี", "en": "Tax law" } },
    { "expValue": "HERITAGE", "txt": { "th": "กฏหมายครอบครัวและมรดก", "en": "Heritage law" } },
    { "expValue": "CONTRACT", "txt": { "th": "กฏหมายสัญญา", "en": "Contract law" } },
    { "expValue": "BANKING", "txt": { "th": "กฎหมายการเงินธนาคาร", "en": "Banking law" } },
    { "expValue": "INTEL_PROP", "txt": { "th": "กฎหมายทรัยย์สินทางปัญญา", "en": "Intellectual prop. law" } },
    { "expValue": "ASSET", "txt": { "th": "กฎหมายทรัพย์สินและที่ดิน", "en": "Property law" } },
    { "expValue": "COMPANY", "txt": { "th": "กฏหมายบริษัท", "en": "Company law" } },
    { "expValue": "LABOR", "txt": { "th": "กฏหมายแรงงาน", "en": "Labor law" } }
];

export const EXPERTISES_W_ALL: any[] = [
    { "expValue": "ALL", "txt": { "th": "ทุกความชำนาญ", "en": "All expertise" } },
    { "expValue": "LIQUIDATION", "txt": { "th": "การเลิกกิจการ", "en": "Liquidation law" } },
    { "expValue": "PERMIT_DOCS", "txt": { "th": "การขอใบอนุญาตต่างๆ", "en": "Permission Docs" } },
    { "expValue": "INSURANCE", "txt": { "th": "กฏหมายประกันภัย", "en": "Insurance law" } },
    { "expValue": "INTER_TRADE", "txt": { "th": "กฎหมายการค้าระหว่างประเทศ", "en": "Intl. trade law" } },
    { "expValue": "ENVIRONMENT", "txt": { "th": "กฏหมายสิ่งแวดล้อม", "en": "Environment law" } },
    { "expValue": "PUBLIC", "txt": { "th": "กฏหมายมหาชน", "en": "Public law" } },
    { "expValue": "CRIMINAL", "txt": { "th": "กฏหมายละเมิด/อาญา", "en": "Criminal law" } },
    { "expValue": "BANKRUPTCY", "txt": { "th": "กฏหมายล้มละลายและฟื้นฟูกิจการ", "en": "Bankruptcy law" } },
    { "expValue": "MERGE", "txt": { "th": "การควบรวมกิจการ", "en": "M&A law" } },
    { "expValue": "TAX", "txt": { "th": "กฏหมายภาษี", "en": "Tax law" } },
    { "expValue": "HERITAGE", "txt": { "th": "กฏหมายครอบครัวและมรดก", "en": "Heritage law" } },
    { "expValue": "CONTRACT", "txt": { "th": "กฏหมายสัญญา", "en": "Contract law" } },
    { "expValue": "BANKING", "txt": { "th": "กฎหมายการเงินธนาคาร", "en": "Banking law" } },
    { "expValue": "INTEL_PROP", "txt": { "th": "กฎหมายทรัยย์สินทางปัญญา", "en": "Intellectual prop. law" } },
    { "expValue": "ASSET", "txt": { "th": "กฎหมายทรัพย์สินและที่ดิน", "en": "Property law" } },
    { "expValue": "COMPANY", "txt": { "th": "กฏหมายบริษัท", "en": "Company law" } },
    { "expValue": "LABOR", "txt": { "th": "กฏหมายแรงงาน", "en": "Labor law" } }
];

export const PROVINCES: any[] = [
    //central
    { "value": "KAMPAENGPETCH",         "part": "CENTRAL", "txt": { "th": "กำแพงเพชร", "en": "Kampaengpetch" } },
    { "value": "CHAINAT",               "part": "CENTRAL", "txt": { "th": "ชัยนาท", "en": "Chainat" } },
    { "value": "NAHKONNAYOK",           "part": "CENTRAL", "txt": { "th": "นครนายก", "en": "Nakhonnayok" } },
    { "value": "NAKHONPRATHOM",         "part": "CENTRAL", "txt": { "th": "นครปฐม", "en": "Nakhonprathom" } },
    { "value": "NAKHONSAWAN",           "part": "CENTRAL", "txt": { "th": "นครวสรรค์", "en": "Nakhonsawan" } },
    { "value": "NONTHABURI",            "part": "CENTRAL", "txt": { "th": "นนทบุรี", "en": "Nonthaburi" } },
    { "value": "PRATHUMTHANI",          "part": "CENTRAL", "txt": { "th": "ปทุมธานี", "en": "Prathumthani" } },
    { "value": "AYUTTHAYA",             "part": "CENTRAL", "txt": { "th": "อยุธยา", "en": "Ayutthaya" } },
    { "value": "PIJITR",                "part": "CENTRAL", "txt": { "th": "พิจิตร", "en": "Pijitr" } },
    { "value": "PISSANULOK",            "part": "CENTRAL", "txt": { "th": "พิษณุโลก", "en": "Pissanulok" } },
    { "value": "PETCHABOON",            "part": "CENTRAL", "txt": { "th": "เพชรบูรณ์", "en": "Petchaboon" } },
    { "value": "LOPBURI",               "part": "CENTRAL", "txt": { "th": "ลพบุรี", "en": "Lopburi" } },
    { "value": "SAMUTPRAKARN",          "part": "CENTRAL", "txt": { "th": "สมุทรปราการ", "en": "Samutprakarn" } },
    { "value": "SAMUTSONGKRAM",         "part": "CENTRAL", "txt": { "th": "สมุทรสงคราม", "en": "Samutsongkram" } },
    { "value": "SAMUTSAKHON",           "part": "CENTRAL", "txt": { "th": "สมุทสาคร", "en": "Samusakorn" } },
    { "value": "SINGHABURI",            "part": "CENTRAL", "txt": { "th": "สิงห์บุรี", "en": "Singhaburi" } },
    { "value": "SUKHOTAI",              "part": "CENTRAL", "txt": { "th": "สูโขทัย", "en": "Sukotai" } },
    { "value": "SUPHANBURI",            "part": "CENTRAL", "txt": { "th": "สุพรรณบุรี", "en": "Suphanburi" } },
    { "value": "SARABURI",              "part": "CENTRAL", "txt": { "th": "สระบุรี", "en": "Saraburi" } },
    { "value": "ANGTHONG",              "part": "CENTRAL", "txt": { "th": "อ่างทอง", "en": "Angthong" } },
    { "value": "UTHAITHANI",            "part": "CENTRAL", "txt": { "th": "อุทัยธานี", "en": "Uthaithani" } },
    { "value": "BANGKOK",               "part": "CENTRAL", "txt": { "th": "กรุงเทพ", "en": "Bangkok" } },

    //northern part
    { "value": "CHIANGMAI", "part": "NORTH", "txt": { "th": "เชียงใหม่",  "en": "Chiangmai" } },
    { "value": "CHIANGRAI", "part": "NORTH", "txt": { "th": "เชียงราย",  "en": "Chiangrai" } },
    { "value": "NARN",      "part": "NORTH", "txt": { "th": "น่าน",      "en": "Narn" } },
    { "value": "PAYAO",     "part": "NORTH", "txt": { "th": "พะเยา",    "en": "Payao" } },
    { "value": "PRAE",      "part": "NORTH", "txt": { "th": "แพร่",      "en": "Prae" } },
    { "value": "MAEHONGSON","part": "NORTH", "txt": { "th": "แม่ฮ่องสอน", "en": "Maehongson" } },
    { "value": "LAMPANG",   "part": "NORTH", "txt": { "th": "ลำปาง",    "en": "Lampang" } },
    { "value": "LAMPOON",   "part": "NORTH", "txt": { "th": "ลำพูน",     "en": "Lampoon" } },
    { "value": "UTARADIT",  "part": "NORTH", "txt": { "th": "อุตรดิตถ์",   "en": "Utaradit" } },
    //north east part
    { "value": "KALASIN",           "part": "N_EAST", "txt": { "th": "กาฬสินธุ์", "en": "Kalasin" } },
    { "value": "KHONKAEN",          "part": "N_EAST", "txt": { "th": "ขอนแก่น", "en": "Khon-kaen" } },
    { "value": "CHAIYAPHUM",        "part": "N_EAST", "txt": { "th": "ชัยภูมิ", "en": "Chaiyaphum" } },
    { "value": "NAKHONPANOM",       "part": "N_EAST", "txt": { "th": "นครพนม", "en": "Nakhonphanom" } },
    { "value": "NAKOHNRATCHASIMA",  "part": "N_EAST", "txt": { "th": "นครราชสีมา", "en": "Nakhonratchasima" } },
    { "value": "BUINGKARN",         "part": "N_EAST", "txt": { "th": "บึงกาฬ", "en": "Buingkarn" } },
    { "value": "BURIRAM",           "part": "N_EAST", "txt": { "th": "บุรีรัมย์", "en": "Buriram" } },
    { "value": "MAHASARAKHAM",      "part": "N_EAST", "txt": { "th": "มหาสารคาม", "en": "Mahasarakham" } },
    { "value": "MUKDAHARN",         "part": "N_EAST", "txt": { "th": "มุกดาหาร", "en": "Mukdaharn" } },
    { "value": "YASOTHORN",         "part": "N_EAST", "txt": { "th": "ยโสธร", "en": "Yasothorn" } },
    { "value": "ROIED",             "part": "N_EAST", "txt": { "th": "ร้อยเอ๊ด", "en": "Roi-ed" } },
    { "value": "LEI",               "part": "N_EAST", "txt": { "th": "เลย", "en": "Lei" } },
    { "value": "SAKOLNAKHON",       "part": "N_EAST", "txt": { "th": "สกลนคร", "en": "Sakonnakorn" } },
    { "value": "SURIN",             "part": "N_EAST", "txt": { "th": "สุรินทร์", "en": "Surin" } },
    { "value": "SRISAKET",          "part": "N_EAST", "txt": { "th": "ศรีสะเกษ", "en": "Srisaket" } },
    { "value": "NONGKAI",           "part": "N_EAST", "txt": { "th": "หนองคาย", "en": "Nongkai" } },
    { "value": "NONGBUALUMPHU",     "part": "N_EAST", "txt": { "th": "หนองบัวลำภู", "en": "Nongbualumphu" } },
    { "value": "UDONTHANI",         "part": "N_EAST", "txt": { "th": "อุดรธานี", "en": "Udonthani" } },
    { "value": "UBONRATCHATHANI",   "part": "N_EAST", "txt": { "th": "อุบลราชธานี", "en": "Ubonratchathani" } },
    { "value": "UMNAJJAROEN",       "part": "N_EAST", "txt": { "th": "อำนาจเจริญ", "en": "Umnajjaroen" } },
    //east
    { "value": "CHANTHABURI",   "part": "EAST", "txt": { "th": "จันทบุรี", "en": "Chanthaburi" } },
    { "value": "CHACHOENGSAO",  "part": "EAST", "txt": { "th": "ฉะเชิงเทรา", "en": "Chachoengsao" } },
    { "value": "CHONBURI",      "part": "EAST", "txt": { "th": "ชลบุรี", "en": "Chonburi" } },
    { "value": "TRAT",          "part": "EAST", "txt": { "th": "ตราด", "en": "Trat" } },
    { "value": "PRACHIBURI",    "part": "EAST", "txt": { "th": "ปราจีนบุรี", "en": "Prachinburi" } },
    { "value": "RAYONG",        "part": "EAST", "txt": { "th": "ระยอง", "en": "Rayong" } },
    { "value": "SAKAEO",        "part": "EAST", "txt": { "th": "สระแก้ว", "en": "Sakaeo" } },
    //west
    { "value": "KANCHANABURI", "part": "WEST", "txt": { "th": "กาญจนบุรี", "en": "Kanchanaburi" } },
    { "value": "TAK", "part": "WEST", "txt": { "th": "ตาก", "en": "Tak" } },
    { "value": "PRACHUAPKHIRIKHAN", "part": "WEST", "txt": { "th": "ประจวบคิรีขันธ์", "en": "Pachuapkhirikhan" } },
    { "value": "PHETCHABURI", "part": "WEST", "txt": { "th": "เพชรบุรี", "en": "Phetchaburi" } },
    { "value": "RATCHABURI", "part": "WEST", "txt": { "th": "ราชบุรี", "en": "Ratchaburi" } },
    //south
    { "value": "KRABI", "part": "SOUTH", "txt": { "th": "กระบี่", "en": "Krabi" } },
    { "value": "CHUMPHON", "part": "SOUTH", "txt": { "th": "ชุมพร", "en": "Chumphon" } },
    { "value": "TRANG", "part": "SOUTH", "txt": { "th": "ตรัง", "en": "Trang" } },
    { "value": "NAKHONSITHAMMARAT", "part": "SOUTH", "txt": { "th": "นครราชสีมา", "en": "Nakhonsithammarat" } },
    { "value": "NARATHIWAT", "part": "SOUTH", "txt": { "th": "นราธิวาส", "en": "Narathiwat" } },
    { "value": "PATTANI", "part": "SOUTH", "txt": { "th": "ปัตตานี", "en": "Pattani" } },
    { "value": "PHANGNGA", "part": "SOUTH", "txt": { "th": "พังงา", "en": "Phangnga" } },
    { "value": "PATTHALUNG", "part": "SOUTH", "txt": { "th": "พัทลุง", "en": "Patthalung" } },
    { "value": "PHUKET", "part": "SOUTH", "txt": { "th": "ภูเก็ต", "en": "Phuket" } },
    { "value": "YALA", "part": "SOUTH", "txt": { "th": "ยะลา", "en": "Yala" } },
    { "value": "RANONG", "part": "SOUTH", "txt": { "th": "ระนอง", "en": "Ranong" } },
    { "value": "SONGKHLA", "part": "SOUTH", "txt": { "th": "สงขลา", "en": "Songkhla" } },
    { "value": "SATUN", "part": "SOUTH", "txt": { "th": "สตูล", "en": "Satun" } },
    { "value": "SURATTHANI", "part": "SOUTH", "txt": { "th": "สุราษฏร์ธานี", "en": "Suratthani" } },





];

export const PROVINCES_W_ALL: any[] = [
    { "value": "ALL", "txt": { "th": "ทุกจังหวัด", "en": "All province" } },
    //central
    { "value": "KAMPAENGPETCH", "part": "CENTRAL", "txt": { "th": "กำแพงเพชร", "en": "Kampaengpetch" } },
    { "value": "CHAINAT", "part": "CENTRAL", "txt": { "th": "ชัยนาท", "en": "Chainat" } },
    { "value": "NAHKONNAYOK", "part": "CENTRAL", "txt": { "th": "นครนายก", "en": "Nakhonnayok" } },
    { "value": "NAKHONPRATHOM", "part": "CENTRAL", "txt": { "th": "นครปฐม", "en": "Nakhonprathom" } },
    { "value": "NAKHONSAWAN", "part": "CENTRAL", "txt": { "th": "นครวสรรค์", "en": "Nakhonsawan" } },
    { "value": "NONTHABURI", "part": "CENTRAL", "txt": { "th": "นนทบุรี", "en": "Nonthaburi" } },
    { "value": "PRATHUMTHANI", "part": "CENTRAL", "txt": { "th": "ปทุมธานี", "en": "Prathumthani" } },
    { "value": "AYUTTHAYA", "part": "CENTRAL", "txt": { "th": "อยุธยา", "en": "Ayutthaya" } },
    { "value": "PIJITR", "part": "CENTRAL", "txt": { "th": "พิจิตร", "en": "Pijitr" } },
    { "value": "PISSANULOK", "part": "CENTRAL", "txt": { "th": "พิษณุโลก", "en": "Pissanulok" } },
    { "value": "PETCHABOON", "part": "CENTRAL", "txt": { "th": "เพชรบูรณ์", "en": "Petchaboon" } },
    { "value": "LOPBURI", "part": "CENTRAL", "txt": { "th": "ลพบุรี", "en": "Lopburi" } },
    { "value": "SAMUTPRAKARN", "part": "CENTRAL", "txt": { "th": "สมุทรปราการ", "en": "Samutprakarn" } },
    { "value": "SAMUTSONGKRAM", "part": "CENTRAL", "txt": { "th": "สมุทรสงคราม", "en": "Samutsongkram" } },
    { "value": "SAMUTSAKHON", "part": "CENTRAL", "txt": { "th": "สมุทสาคร", "en": "Samusakorn" } },
    { "value": "SINGHABURI", "part": "CENTRAL", "txt": { "th": "สิงห์บุรี", "en": "Singhaburi" } },
    { "value": "SUKHOTAI", "part": "CENTRAL", "txt": { "th": "สูโขทัย", "en": "Sukotai" } },
    { "value": "SUPHANBURI", "part": "CENTRAL", "txt": { "th": "สุพรรณบุรี", "en": "Suphanburi" } },
    { "value": "SARABURI", "part": "CENTRAL", "txt": { "th": "สระบุรี", "en": "Saraburi" } },
    { "value": "ANGTHONG", "part": "CENTRAL", "txt": { "th": "อ่างทอง", "en": "Angthong" } },
    { "value": "UTHAITHANI", "part": "CENTRAL", "txt": { "th": "อุทัยธานี", "en": "Uthaithani" } },
    { "value": "BANGKOK", "part": "CENTRAL", "txt": { "th": "กรุงเทพ", "en": "Bangkok" } },

    //northern part
    { "value": "CHIANGMAI", "part": "NORTH", "txt": { "th": "เชียงใหม่", "en": "Chiangmai" } },
    { "value": "CHIANGRAI", "part": "NORTH", "txt": { "th": "เชียงราย", "en": "Chiangrai" } },
    { "value": "NARN", "part": "NORTH", "txt": { "th": "น่าน", "en": "Narn" } },
    { "value": "PAYAO", "part": "NORTH", "txt": { "th": "พะเยา", "en": "Payao" } },
    { "value": "PRAE", "part": "NORTH", "txt": { "th": "แพร่", "en": "Prae" } },
    { "value": "MAEHONGSON", "part": "NORTH", "txt": { "th": "แม่ฮ่องสอน", "en": "Maehongson" } },
    { "value": "LAMPANG", "part": "NORTH", "txt": { "th": "ลำปาง", "en": "Lampang" } },
    { "value": "LAMPOON", "part": "NORTH", "txt": { "th": "ลำพูน", "en": "Lampoon" } },
    { "value": "UTARADIT", "part": "NORTH", "txt": { "th": "อุตรดิตถ์", "en": "Utaradit" } },
    //north east part
    { "value": "KALASIN", "part": "N_EAST", "txt": { "th": "กาฬสินธุ์", "en": "Kalasin" } },
    { "value": "KHONKAEN", "part": "N_EAST", "txt": { "th": "ขอนแก่น", "en": "Khon-kaen" } },
    { "value": "CHAIYAPHUM", "part": "N_EAST", "txt": { "th": "ชัยภูมิ", "en": "Chaiyaphum" } },
    { "value": "NAKHONPANOM", "part": "N_EAST", "txt": { "th": "นครพนม", "en": "Nakhonphanom" } },
    { "value": "NAKOHNRATCHASIMA", "part": "N_EAST", "txt": { "th": "นครราชสีมา", "en": "Nakhonratchasima" } },
    { "value": "BUINGKARN", "part": "N_EAST", "txt": { "th": "บึงกาฬ", "en": "Buingkarn" } },
    { "value": "BURIRAM", "part": "N_EAST", "txt": { "th": "บุรีรัมย์", "en": "Buriram" } },
    { "value": "MAHASARAKHAM", "part": "N_EAST", "txt": { "th": "มหาสารคาม", "en": "Mahasarakham" } },
    { "value": "MUKDAHARN", "part": "N_EAST", "txt": { "th": "มุกดาหาร", "en": "Mukdaharn" } },
    { "value": "YASOTHORN", "part": "N_EAST", "txt": { "th": "ยโสธร", "en": "Yasothorn" } },
    { "value": "ROIED", "part": "N_EAST", "txt": { "th": "ร้อยเอ๊ด", "en": "Roi-ed" } },
    { "value": "LEI", "part": "N_EAST", "txt": { "th": "เลย", "en": "Lei" } },
    { "value": "SAKOLNAKHON", "part": "N_EAST", "txt": { "th": "สกลนคร", "en": "Sakonnakorn" } },
    { "value": "SURIN", "part": "N_EAST", "txt": { "th": "สุรินทร์", "en": "Surin" } },
    { "value": "SRISAKET", "part": "N_EAST", "txt": { "th": "ศรีสะเกษ", "en": "Srisaket" } },
    { "value": "NONGKAI", "part": "N_EAST", "txt": { "th": "หนองคาย", "en": "Nongkai" } },
    { "value": "NONGBUALUMPHU", "part": "N_EAST", "txt": { "th": "หนองบัวลำภู", "en": "Nongbualumphu" } },
    { "value": "UDONTHANI", "part": "N_EAST", "txt": { "th": "อุดรธานี", "en": "Udonthani" } },
    { "value": "UBONRATCHATHANI", "part": "N_EAST", "txt": { "th": "อุบลราชธานี", "en": "Ubonratchathani" } },
    { "value": "UMNAJJAROEN", "part": "N_EAST", "txt": { "th": "อำนาจเจริญ", "en": "Umnajjaroen" } },
    //east
    { "value": "CHANTHABURI", "part": "EAST", "txt": { "th": "จันทบุรี", "en": "Chanthaburi" } },
    { "value": "CHACHOENGSAO", "part": "EAST", "txt": { "th": "ฉะเชิงเทรา", "en": "Chachoengsao" } },
    { "value": "CHONBURI", "part": "EAST", "txt": { "th": "ชลบุรี", "en": "Chonburi" } },
    { "value": "TRAT", "part": "EAST", "txt": { "th": "ตราด", "en": "Trat" } },
    { "value": "PRACHIBURI", "part": "EAST", "txt": { "th": "ปราจีนบุรี", "en": "Prachinburi" } },
    { "value": "RAYONG", "part": "EAST", "txt": { "th": "ระยอง", "en": "Rayong" } },
    { "value": "SAKAEO", "part": "EAST", "txt": { "th": "สระแก้ว", "en": "Sakaeo" } },
    //west
    { "value": "KANCHANABURI", "part": "WEST", "txt": { "th": "กาญจนบุรี", "en": "Kanchanaburi" } },
    { "value": "TAK", "part": "WEST", "txt": { "th": "ตาก", "en": "Tak" } },
    { "value": "PRACHUAPKHIRIKHAN", "part": "WEST", "txt": { "th": "ประจวบคิรีขันธ์", "en": "Pachuapkhirikhan" } },
    { "value": "PHETCHABURI", "part": "WEST", "txt": { "th": "เพชรบุรี", "en": "Phetchaburi" } },
    { "value": "RATCHABURI", "part": "WEST", "txt": { "th": "ราชบุรี", "en": "Ratchaburi" } },
    //south
    { "value": "KRABI", "part": "SOUTH", "txt": { "th": "กระบี่", "en": "Krabi" } },
    { "value": "CHUMPHON", "part": "SOUTH", "txt": { "th": "ชุมพร", "en": "Chumphon" } },
    { "value": "TRANG", "part": "SOUTH", "txt": { "th": "ตรัง", "en": "Trang" } },
    { "value": "NAKHONSITHAMMARAT", "part": "SOUTH", "txt": { "th": "นครราชสีมา", "en": "Nakhonsithammarat" } },
    { "value": "NARATHIWAT", "part": "SOUTH", "txt": { "th": "นราธิวาส", "en": "Narathiwat" } },
    { "value": "PATTANI", "part": "SOUTH", "txt": { "th": "ปัตตานี", "en": "Pattani" } },
    { "value": "PHANGNGA", "part": "SOUTH", "txt": { "th": "พังงา", "en": "Phangnga" } },
    { "value": "PATTHALUNG", "part": "SOUTH", "txt": { "th": "พัทลุง", "en": "Patthalung" } },
    { "value": "PHUKET", "part": "SOUTH", "txt": { "th": "ภูเก็ต", "en": "Phuket" } },
    { "value": "YALA", "part": "SOUTH", "txt": { "th": "ยะลา", "en": "Yala" } },
    { "value": "RANONG", "part": "SOUTH", "txt": { "th": "ระนอง", "en": "Ranong" } },
    { "value": "SONGKHLA", "part": "SOUTH", "txt": { "th": "สงขลา", "en": "Songkhla" } },
    { "value": "SATUN", "part": "SOUTH", "txt": { "th": "สตูล", "en": "Satun" } },
    { "value": "SURATTHANI", "part": "SOUTH", "txt": { "th": "สุราษฏร์ธานี", "en": "Suratthani" } },

];

export const SORT_ORDER: any[] = [
    { "sortValue": "rating", "txt": { "th": "ทุกจังหวัด", "en": "All province" } },
    { "sortValue": "BANGKOK", "txt": { "th": "กรุงเทพ", "en": "Bangkok" } },
    { "sortValue": "CHIANGMAI", "txt": { "th": "เชียงใหม่", "en": "Chiangmai" } },
    { "sortValue": "UDONTHANI", "txt": { "th": "อุดรธานี", "en": "Udonthani" } },

];

export const TITLES: any[] = [
    { "value": "MR", "txt": { "th": "นาย", "en": "Mr." } },
    { "value": "MRS", "txt": { "th": "นาง", "en": "Mrs." } },
    { "value": "MS", "txt": { "th": "นางสาว", "en": "Ms." } }

];

export const VERIFY_STATUS: any = [
    { "value": "WAIT_FOR_DOCS", "txt": { "th": "รอเอกสารเพื่อทำการตรวจสอบ", "en": "Waiting for documents for approval." } },
    { "value": "DOCS_SENT", "txt": { "th": "ส่งเอกสารเรียบร้อย รอการตรวจสอบ", "en": "Documents sent, waiting for approval" } },
    { "value": "APPROVED", "txt": { "th": "ผ่านการตรวจสอบแล้ว", "en": "Submission Approved" } }
];

export const DEGREE: any = [
    { "value": "UNDER", "txt": { "th": "ปริญญาตรี", "en": "Bachelor degree " } },
    { "value": "MASTER", "txt": { "th": "ปริญญาโท", "en": "Master degree" } },
    { "value": "PHD", "txt": { "th": "ปริญญาเอก", "en": "Doctoral degree" } }
];