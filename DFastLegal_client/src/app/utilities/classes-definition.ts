export class Client {
    
    title:string;
    firstName:string;
    lastName:string;
    personalId:number;
    email:string;
    password:string;
    asCompany:boolean;
    companyName:string;
    companyId:number;
    isLawyer:boolean;
    phoneNumber: string;

}

export class Lawyer{
    
    title:string;
    firstName:string;
    lastName:string;
    personalId:number;
    email:string;
    password:string;
    asCompany:boolean;
    companyType:string;
    companyName:string;
    companyId:number;
    isLawyer: boolean;
    phoneNumber: string;
    
}

export class Qualification {
    degree: string;
    institute: string;
    studyField: string;

}

export class Question {
    text: string;
    number: number;
    code: string;
    hint: string;
    answers: any[];
}

export class Address {
    name: string;
    address1: string;
    address2: string;
    province: string;
    district: string;
    country: string;
    postCode: string;
}

export class Card  {
    name: string;
    number: string;
    expiration_month: string;
    expiration_year: string;
    security_code: string;
}