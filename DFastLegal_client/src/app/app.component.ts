import { Component,OnInit } from '@angular/core';
import {Router, Event, NavigationEnd, }from'@angular/router';
import {Location}from'@angular/common';


declare var ga: Function;
declare var Parse: any;
declare var Omise: any;
declare var $:any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

    //google analytic
    private currentRoute: string;

    constructor(
        _router: Router,
        _location: Location
    ) {

        Parse.initialize("sd3f21e65hdf21g6dh5et6h42cv6d5gf4");
        Parse.serverURL = 'https://server.dfastlegal.com/parse'; //This is configure in AWS route53 to target ELB at dfastlegal-elasticbeanstalk-webserver.ap-southeast-1.elasticbeanstalk.com/
        // Parse.serverURL = 'http://localhost:1337/parse'; //This is configure in AWS route53 to target ELB at dfastlegal-elasticbeanstalk-webserver.ap-southeast-1.elasticbeanstalk.com/

        Omise.setPublicKey("pkey_568fz9hhjp92s1gz70c");

        _router.events.subscribe((event: Event) => {
            // Send GA tracking on NavigationEnd event. You may wish to add other 
            // logic here too or change which event to work with
            if (event instanceof NavigationEnd) {

                //naviage to top when load navigation end.
                document.body.scrollTop = 0;

                // When the route is '/', location.path actually returns ''.
                let newRoute = _location.path() || '/';
                // If the route has changed, send the new route to analytics.
                if (this.currentRoute != newRoute) {
                    ga('send', 'pageview', newRoute);
                    this.currentRoute = newRoute;
                }
            }
        });

    }

    ngOnInit(){
        
        // $('#tempBetaModal').modal('show');


    }
}
