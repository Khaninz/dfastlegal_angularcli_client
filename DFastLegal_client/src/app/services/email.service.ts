﻿import { Injectable } from '@angular/core';
import { Http, Response, Request, RequestOptionsArgs, RequestMethod, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import '../utilities/rxjs-operators';

@Injectable()
export class EmailService {

    //Reference https://angular.io/docs/ts/latest/guide/server-communication.html#!#http-client
    //using promise based.
    serverNofificationURL: string = 'https://server.dfastlegal.com';

    // serverNofificationURL:string = 'http://localhost:1337';


    constructor(
        private http: Http

    ) {

    }

    // app.get('/subscribe/:listAddress/:userId', function (req, res)  from js

    subscribe(listAddress: string, userId: string): Promise<any> {
        console.log('the service is called');
        var url = this.serverNofificationURL + '/subscribe/' + listAddress + '/' + userId;
        console.log(url);


        return this.http.get(url)
            .toPromise() //this should work as we already import './rxjs-operators' which include toPromise method already.;
            .then((res: Response) => {
                console.log(res);
                return res.status;
            })
            .catch((error: any) => {
                return error;
            });
    }

    // app.get('/unsubscribe/:listAddress/:userAddress', function (req, res) ==== server.js

    unsubscribe(listAddress: string, userAddress: string): Promise<any> {
        console.log('the service is called');
        var url = this.serverNofificationURL + '/unsubscribe/' + listAddress + '/' + userAddress;
        console.log(url);


        return this.http.get(url)
            .toPromise() //this should work as we already import './rxjs-operators' which include toPromise method already.;
            .then((res: Response) => {
                console.log(res);
                return res.status;
            })
            .catch((error: any) => {
                return error;
            });
    }


    private extractData(res: Response) {
        console.log(res);
        let body = res.json();
        let code = res.status; //code should return wheter the notify is succeed or failed.

        return code;

    }

    private handleError(error: any) { //copy from reference
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Promise.reject(errMsg);
    }


}