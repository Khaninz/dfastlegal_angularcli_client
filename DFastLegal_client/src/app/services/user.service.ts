﻿import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { Observer } from 'rxjs/Observer';
import { Router } from '@angular/router';
import 'rxjs/add/operator/share';

import { Client, Lawyer } from '../utilities/classes-definition';

//service
import { ClientSideService } from '../services/client-side.service';
import { EmailService } from '../services/email.service';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.
declare function goog_report_conversion(params: string); //declare a function in a global scope very similar as declare var for google analytics in app.component.ts

@Injectable()
export class UserService {

    //currentLawyerProfileObserver: Observer<any>;
    //currentLawyerProfile$: Observable<any>
    //currentLawyerProfile: any;

    currentUser: any;
    currentUserObserver: Observer<any>;
    currentUser$: Observable<any>;

    featuredLawyerProfiles: any[] = [];
    featuredLawyerProfiles$: Observable<any>;
    featuredLawyerProfilesObserver: Observer<any>;

    currentLang: string = 'th';
    userLangObserver: Observer<any>;
    userLang$: Observable<any>;

    isLoading: boolean;
    isLoadingObserver: Observer<any>;
    isLoading$: Observable<any>;


    constructor(
        private zone: NgZone,
        private router: Router,
        private clientSide_S: ClientSideService,
        private email_S: EmailService


    ) {
        this.currentUser$ = new Observable((observer: any) => { //this param will be observe from other comp
            this.currentUserObserver = observer;

            //init with current user
            this.currentUser = Parse.User.current();
            this.currentUserObserver.next(this.currentUser);
        }).share(); //IMPORTANT

        this.userLang$ = new Observable((observer: any) => {
            this.userLangObserver = observer;
            this.userLangObserver.next(this.currentLang);//default value
        }).share();
        this.isLoading$ = new Observable((observer: any) => {
            this.isLoadingObserver = observer;
        }).share();

        this.featuredLawyerProfiles$ = new Observable((observer: any) => {

            this.featuredLawyerProfilesObserver = observer;
        }).share();


        //this.currentLawyerProfile$ = new Observable((observer: any) => {
        //    this.currentLawyerProfileObserver = observer;

        //}).share();
    }

    //Home
    getFeaturedLawyers() {
        return this.featuredLawyerProfiles;

    }

    loadFeaturedLawyers() {

        let LawyerProfiles = Parse.Object.extend('LawyerProfiles');
        let query = new Parse.Query(LawyerProfiles);
        query.include("createdBy");
        query.include("qualifications");
        query.equalTo("isFeatured", true);

        query.find().then((featuredLawyerProfiles) => {

            let shuffledProfiles = this.shuffle(featuredLawyerProfiles);

            this.featuredLawyerProfiles = shuffledProfiles;
            this.featuredLawyerProfilesObserver.next(this.featuredLawyerProfiles);

        }, (error) => {

            console.log(error.message);
        });
    }
    //!Home

    getCurrentUser() {
        return Parse.User.current();
    }

    //getCurrentLawyerProfile() {
    //    return this.currentLawyerProfile
    //}

    lawyerSignUp(lawyer: Lawyer): Promise<any> {
        var user = new Parse.User();
        user.set("username", lawyer.email);
        user.set("password", lawyer.password);
        user.set("email", lawyer.email);

        // other fields can be set just like with Parse.Object
        user.set("title", lawyer.title);
        user.set("firstnameTH", lawyer.firstName);
        user.set("lastnameTH", lawyer.lastName);
        user.set("firstnameEN", lawyer.firstName);
        user.set("lastnameEN", lawyer.lastName);
        user.set("personalId", lawyer.personalId);
        user.set("asCompany", lawyer.asCompany);
        user.set("companyName", lawyer.companyName);
        user.set("companyId", lawyer.companyId);
        user.set("isLawyer", lawyer.isLawyer);
        user.set("companyType", lawyer.companyType);

        return user.signUp(null).then((currentUser: any) => {

            this.currentUser = currentUser;
            this.currentUserObserver.next(this.currentUser);

            let LawyerProfiles = Parse.Object.extend('LawyerProfiles');
            let lawyerProfile = new LawyerProfiles();

            lawyerProfile.set("createdBy", currentUser);
            lawyerProfile.set("expertise1", "LIQUIDATION");
            lawyerProfile.set("expertise2", "LIQUIDATION");
            lawyerProfile.set("expertise3", "LIQUIDATION");
            lawyerProfile.set("experienceTH", "");
            lawyerProfile.set("experienceEN", "");
            lawyerProfile.set("useMatchingService", false);
            lawyerProfile.set("useListingService", false);
            lawyerProfile.set("isVerified", false);
            lawyerProfile.set("verifyStatus", "WAIT_FOR_DOCS");
            lawyerProfile.set("operatingProvinces", ["BANGKOK"]);
            lawyerProfile.set("shortDescTH", "");
            lawyerProfile.set("shortDescEN", "");
            lawyerProfile.set("qualifications", []);
            lawyerProfile.set("useMatchingService", true);
            lawyerProfile.set("useListingService", true);

            currentUser.set('lawyerProfile', lawyerProfile);

            return currentUser.save();

        }).then((currentUser: any) => {

            goog_report_conversion('https://www.dfastlegal.com/lawyer-signup');

            this.email_S.subscribe('not_yet_sumitted_docs_lawyers_list@mail.dfastlegal.com', currentUser.id);

            this.currentUserObserver.next(currentUser);
            //this.fetchLawyerProfile(newLawyer);
        });

    }

    logIn(logInEmail: string, logInPassword: string): Promise<any> {
        var emailLowerCase = logInEmail.toLowerCase();

        return Parse.User.logIn(emailLowerCase, logInPassword, {
            success: (user: any) => this.zone.run(() => {

                this.currentUserObserver.next(user);
                //this.fetchLawyerProfile(user);

            }
            ), error: (error: any) => this.zone.run(() => {
                console.log('log in failed');
                console.log(error.message);
            })
        });
    }

    logOutRedirectHome(): Promise<any> {
        return Parse.User.logOut().then(() => {
            console.log('user logged out.')


            this.currentUser = Parse.User.current(); //this will now be null
            this.currentUserObserver.next(this.currentUser);
            //this.fetchLawyerProfile(this.currentUser)

            this.toHome();
            this.clientSide_S.clearCurrentData();

        });
    }

    logOut() {
        Parse.User.logOut().then(() => {
            console.log('user logged out.')

            this.clientSide_S.clearCurrentData();

            this.currentUser = Parse.User.current(); //this will now be null
            this.currentUserObserver.next(this.currentUser);
            //this.fetchLawyerProfile(this.currentUser)
        });
    }

    clientSignUp(client: Client) {

        var user = new Parse.User();
        user.set("username", client.email);
        user.set("password", client.password);
        user.set("email", client.email);

        // other fields can be set just like with Parse.Object
        user.set("title", client.title);
        user.set("firstnameTH", client.firstName);
        user.set("lastnameTH", client.lastName);
        user.set("firstnameEN", client.firstName);
        user.set("lastnameEN", client.lastName);
        user.set("personalId", client.personalId);
        user.set("asCompany", client.asCompany);
        user.set("companyName", client.companyName);
        user.set("companyId", client.companyId);
        user.set("isLawyer", client.isLawyer);
        user.set("phoneNumber", client.phoneNumber);

        return user.signUp(null, {
            success: (newClient: any) => this.zone.run(() => {
                console.log('Client sign up success');
                this.currentUserObserver.next(newClient);
                //close the modal
            }
            ), error: (error: any) => this.zone.run(() => {
                console.log(error.message);
            })
        });
    }

    changeLang(lang: string) {
        this.currentLang = lang;
        this.userLangObserver.next(this.currentLang);
    }

    getCurrentLang() {
        return this.currentLang;
    }

    toHome() {
        let link = ['']
        this.router.navigate(link);
    }

    //fetchLawyerProfile(currentUser: any) {
    //    let isLawyer = currentUser.get('isLawyer');
    //    if (isLawyer) {
    //        let lawyerProfile = currentUser.get('lawyerProfile');
    //        let query = new Parse.Query('LawyerProfiles');
    //        query.include('qualifications');
    //        query.get(lawyerProfile.id).then((lawyerProfile: any) => {

    //            console.log('updating lawyer profile');
    //            this.currentLawyerProfile = lawyerProfile;
    //            this.currentLawyerProfileObserver.next(this.currentLawyerProfile);

    //        }, (error: any) => {
    //            console.log(error.message);
    //        });


    //    } else {
    //        console.log('user is not a lawyer');
    //    }

    //}
    updateUserWithOmiseID(currentUser: any, omiseId: string): Promise<any> {

        currentUser.set('omiseId', omiseId);
        return currentUser.save().then((currentUser) => {
            this.currentUser = currentUser;
            this.currentUserObserver.next(this.currentUser);
        });
    }

    resetPassword(email: string): Promise<any> {
        return Parse.User.requestPasswordReset(email, {
            success: function () {
                // Password reset request was sent successfully
            },
            error: function (error) {
                // Show the error message somewhere
                alert("Error: " + error.code + " " + error.message);
            }
        });
    }

    changeProfilePicture() {

        console.log('changing profile picture');

        let component = this;

        var uploadElement = document.createElement("INPUT");
        uploadElement.setAttribute("type", "file");
        uploadElement.setAttribute("id", "chooseFileInput");
        uploadElement.setAttribute("accept", "image/*");
        // uploadElement.setAttribute("multiple","true");

        uploadElement.click();

        //SUPER WEIRD BUG!!!! .onchange won't fire if not console.log below first!!!! only in iOS
        console.log(uploadElement);

        uploadElement.onchange = (fileInput: any) => {

            console.log('choose image done,uploading.');

            component.isLoading = true;
            component.isLoadingObserver.next(component.isLoading);

            var files = fileInput.target.files; //files input will always come in list wether single or multiple.
            var file = files[0];
            var fileURL = window.URL.createObjectURL(file);
            // console.log(fileURL);

            //create image var to contain src of file
            var image = new Image();
            image.src = fileURL;
            image.crossOrigin = "Anonymous";

            image.onload = function () {
                var canvas = <HTMLCanvasElement>document.createElement('CANVAS');
                let targetSize = 300;

                canvas.height = targetSize;
                canvas.width = targetSize;

                let ctx = <CanvasRenderingContext2D>canvas.getContext('2d');

                let W = image.width;
                let H = image.height;

                if (W < H) {

                    let gap = (H - W) / 2;
                    ctx.drawImage(image, 0, gap, W, W, 0, 0, targetSize, targetSize);

                } else if (H < W) {

                    let gap = (W - H) / 2;
                    ctx.drawImage(image, gap, 0, H, H, 0, 0, targetSize, targetSize);


                } else {
                    ctx.drawImage(image, 0, 0, targetSize, targetSize);
                }

                var imageDataViaCanvas = canvas.toDataURL();

                var parseImage = new Parse.File("profile_image", { base64: imageDataViaCanvas });

                component.currentUser.set("profilePicture", parseImage);
                component.currentUser.save(null, {
                    success: function (user: any) {
                        console.log('upload profile picture success');
                        // component.isLoading = false;
                        // var imageFile = user.get("profilePicture");
                        // var imageFileURL = imageFile.url();
                        // component.profileImageURL = imageFileURL;
                        component.currentUserObserver.next(user);

                    }, error: function (user: any, error: any) {
                        console.log('faile upload profile picture.')
                        console.log(error.message);
                        component.isLoading = false;
                        component.isLoadingObserver.next(component.isLoading);

                    }

                });
            }

        }


    }

    manualChangeProfilePicture(file: any) {

        let component = this;

        //credit to 
        let iDeviceType = navigator.userAgent.match(/iPhone|iPad|iPod/i);
        console.log(iDeviceType);

        component.isLoading = true;
        component.isLoadingObserver.next(component.isLoading);


        var fileURL = window.URL.createObjectURL(file);
        // console.log(fileURL);

        //create image var to contain src of file
        var image = new Image();
        image.src = fileURL;
        image.crossOrigin = "Anonymous";

        image.onload = function () {
            var canvas = <HTMLCanvasElement>document.createElement('CANVAS');
            let targetSize = 300;

            canvas.height = targetSize;
            canvas.width = targetSize;

            let ctx = <CanvasRenderingContext2D>canvas.getContext('2d');

            //rotate image only for iOS
            console.log(iDeviceType);
            if (iDeviceType) {
                console.log(`rotating`);
                ctx.translate(150, 150);
                ctx.rotate(90 * Math.PI / 180);

                let W = image.width;
                let H = image.height;

                if (W < H) {

                    let gap = (H - W) / 2;
                    ctx.drawImage(image, 0, gap, W, W, -150, -150, targetSize, targetSize);

                } else if (H < W) {

                    let gap = (W - H) / 2;
                    ctx.drawImage(image, gap, 0, H, H, -150, -150, targetSize, targetSize);


                } else {
                    ctx.drawImage(image, 0, 0, targetSize, targetSize);
                }

            } else {
                console.log(`not rotate`);

                let W = image.width;
                let H = image.height;

                if (W < H) {

                    let gap = (H - W) / 2;
                    ctx.drawImage(image, 0, gap, W, W, 0, 0, targetSize, targetSize);

                } else if (H < W) {

                    let gap = (W - H) / 2;
                    ctx.drawImage(image, gap, 0, H, H, 0, 0, targetSize, targetSize);


                } else {
                    ctx.drawImage(image, 0, 0, targetSize, targetSize);
                }
            }







            var imageDataViaCanvas = canvas.toDataURL();

            var parseImage = new Parse.File("profile_image", { base64: imageDataViaCanvas });

            component.currentUser.set("profilePicture", parseImage);
            component.currentUser.save(null, {
                success: function (user: any) {
                    console.log('upload profile picture success');
                    // component.isLoading = false;
                    // var imageFile = user.get("profilePicture");
                    // var imageFileURL = imageFile.url();
                    // component.profileImageURL = imageFileURL;
                    component.currentUserObserver.next(user);

                }, error: function (user: any, error: any) {
                    console.log('faile upload profile picture.')
                    console.log(error.message);
                    component.isLoading = false;
                    component.isLoadingObserver.next(component.isLoading);

                }

            });
        }


    }

    //utilities
    shuffle(array: any) {
        var currentIndex = array.length, temporaryValue: any, randomIndex: any;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

}