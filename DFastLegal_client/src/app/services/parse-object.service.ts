import{Injectable}from'@angular/core';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

@Injectable()
export class ParseObjService {

    errorResponse(error:any) {
        console.log(error.message);
    }

    //GET METHODS FOR COMMON
    getCreatedAt(anyObj: any) {
        return anyObj.get('createdAt');
    }

    //get Methods for lawyer profile
    getProfileUser(lawyerProfile: any) { //note: query.include("createdBy") must be execute when query
        try {
            return lawyerProfile.get('createdBy');
        } catch (error){
            console.log(error.message);
        }
        
    }
    getExpertise(lawyerProfile: any, i: number) {
        try {
            var expertiseClassName = 'expertise' + i;
            var expertise = lawyerProfile.get(expertiseClassName);
            return expertise;
        } catch (error) {

        }

    }
    getQualifications(lawyerProfile: any) {
        try {
            return lawyerProfile.get('qualifications');

        } catch (error) {

        }
    }
    getExperience(lawyerProfile: any, lang:string) {
        try {
            return lawyerProfile.get(`experience${lang.toUpperCase()}`);

        } catch (error) {

        }
    }

    //get Methods for user itself
    getUserFirstName(user: any, lang:string){
        try {
            return user.get(`firstname${lang.toUpperCase()}`)

        } catch (error) {

        }
    }

    getUserLastName(user: any,lang:string){
        try {
            return user.get(`lastname${lang.toUpperCase()}`)

        } catch (error) {

        }
    }

    getTelNumber(user: any) {
        try {
            return user.get('phoneNumber');

        } catch (error) {

        }
    }

    getEmail(user: any) {
        try {
            return user.get('email');

        } catch (error) {

        }
    }

    getProfileImageUrl(user: any) {
        try {
            let imageFile = user.get("profilePicture");
            let imageFileURL = imageFile.url();
            return imageFileURL;
        } catch (error) {

        }

    }

    getUserPersonalId(user: any) {
        return user.get('personalId');
    }

    getUserTitle(user: any) {
        return user.get('title');
    }

    getIsLawyer(user: any) {
        try {
            return user.get('isLawyer');
        } catch (error) {
            console.log(error.message);
        }
    }

    getPersonalId(user) {
        try { 
            return user.get('personalId')
        } catch (error) { }
    }

    //get Methods for Question
    getQuestionText(question: any,lang:string) {
        try {
            //return question.get('questionText');
            return question.get(`questionText${lang.toUpperCase()}`);
        } catch (error) {

        }
    }

    getQuestionHint(question: any, lang: string) {
        try {
            return question.get(`questionHint${lang.toUpperCase()}`);

        } catch (error) {

        }
    }

    //get Methods for Answer
    getAnswerText(answer: any, lang: string) {
        try {
            return answer.get(`answerText${lang.toUpperCase()}`);

        } catch (error) {

        }
    }
    getAnswerHint(answer: any, lang: string) {
        try {
            return answer.get(`answerHint${lang.toUpperCase()}`);

        } catch (error) {

        }
    }
    getNextQuestionCode(answer: any, lang: string) {
        try {
            return answer.get('nextQuestionCode');

        } catch (error) {

        }
    }
    getNextQuestion(answer: any) {
        try {
            return answer.get('nextQuestion');

        } catch (error) {

        }
    }
    
    //GET METHODS FOR CLIENT CASE
    getSelectedLawyers(clientCase: any) {
        return clientCase.get('selectedLawyers');
    }
    
    getCaseDetail(clientCase: any) {
        try {
            return clientCase.get('caseDetail');
        } catch (error) { console.log(error.message);}
    }

    getCaseProvinces(clientCase: any) {
        try {
            return clientCase.get('provinces');

        } catch (error) { this.errorResponse(error); }
    }
    getCaseLawyerNumber(clientCase: any) {
        return clientCase.get('lawyerNumber');
    }
    getCaseProgress(clientCase: any) {
        return clientCase.get('questionsProgress');
    }

    //GET METHODS FOR CLIENT PROGRESS
    getCaseForProgress(progress: any) {
        return progress.get('inCase');
    }

    //GET METHODS FOR LAWYER RESPONSE
    getIsResponse(lawyerResponse: any) {
        return lawyerResponse.get('isResponse');
    }

    getIsPaid(lawyerResponse: any) {
        return lawyerResponse.get('isPaid');
    }

    getCaseInResponse(lawyerResponse: any) {
        return lawyerResponse.get('case');
    }

    getClientInResponse(lawyerResponse: any) {
        return lawyerResponse.get('client');

    }

    getRequestedLaywerInResponse(lawyerResponse: any) {
        return lawyerResponse.get('requestedLawyer');

    }

    getResponseContactFee(lawyerResponse:any) {
        try {
            return lawyerResponse.get('amount');

        } catch (error){
            console.log(error.message);
        }
    }


}