import { Injectable } from '@angular/core';

@Injectable()
export class TagsService {
    constructor() {
    }

    AUTHOR = "DFastLegal.com"

    NO_INDEX = {
        "title": {
            "th":"",
            "en":""
        },
        "description":{
            "th":"",
            "en":""
        },
        "keywords":{
            "th":"",
            "en":""
        }
    }

    // title 55 words max
    // description between 150-160

    HOME = {
        "title": {
            "th":"จัดหาทนายความ-ทวงหนี้ ขอใบอนุญาต | DFastLegal.com",
            "en":"Find lawyer - attorney - law advice | DFastLegal.com"
        },
        "description":{
            "th":"DFastLegal.com คือระบบจัดหาทนายและลูกความแบบมีประสิทธิภาพ ลูกความได้ทนายที่ตรงกับปัญหา ทนายความได้ทำงานที่ถนัด อย่างอัตโนมัติและรวดเร็ว ไม่มีค่าคอมมิชชั่น ทันที",
            "en":"DFastLegal.com is a legal matching serivce, where clients meet there lawyer efficiently and quick. Right lawyers work with the right client's problem."
        }

    }

    SERVICES = {
        "title": {
            "th":"บริการของเรา - จัดหาทนายความ | DFastLegal.com",
            "en":"Services - Find lawyer/attorney -advice| DFastLegal.com"
        },
        "description":{
            "th":"DFastLegal.com บริการในการจัดหาทนายและลูกความอย่างมีประสิทธิภาพ ลูกความได้พบกับทนายอย่างง่ายและรวดเร็ว ทนายหาลูกความแบบพาซซีฟ ไม่มีคอมมิชชั่น",
            "en":"DFastLegal.com provide a legal matching service. Client find lawyers quick. Lawyers get clients passivly, No commission."
        }
    }

    PRICING = {
        "title": {
            "th":"การคิดค่าบริการ | DFastLegal.com",
            "en":"Pricing | DFastLegal.com"
        },
        "description":{
            "th":"ลูกความจัดหาทนายความแบบไม่เสียค่าใช้จ่าย ทนายความได้ติดต่อกับลูกความแบบไม่เสียคอมมิชชั่น",
            "en":"Client find a lawyer for free. Lawyer get clients passively and no fee base on commission."
        }
    }
    
    FAQS = {
        "title": {
            "th":"คำถามที่พบบ่อย | DFastLegal.com",
            "en":"FAQS | DFastLegal.com"
        },
        "description":{
            "th":"คำถามที่พบบ่อย เกี่ยวกับการจัดหาลุกความและทนายความ",
            "en":"Frequently asked questions about our matching service."
        }
    }

    LAWYERS = {
        "title": {
            "th":"ทนายที่ร่วมงานกับเรา | DFastLegal.com",
            "en":"Our partner lawyers | DFastLegal.com"
        },
        "description":{
            "th":"ทนายที่ทำงานร่วมกับเราในการ แก้ไขปัญหาลูกความอย่างรวดเร็ว",
            "en":"Our partner lawyers who joined us to solve our client's problem quick and efficient."
        }
    }
    
    HELP_SUPPORT = {
        "title": {
            "th":"สนับสนุนการบริการ | DFastLegal.com",
            "en":"Help and support | DFastLegal.com"
        },
        "description":{
            "th":"ข้อตกลงการใช้งาน - นโยบายความเป็นส่วนตัว - การสั่งซื้อสินค้า/บริการ",
            "en":"Term of use - Privacy policy - Purchase of order"
        }
    }

    LAWYER_PROFILE_EDIT = {
        "title": {
            "th":"โปรไฟล์ทนายความ - แก้ไข | DFastLegal.com",
            "en":"Lawyer profile - edit | DFastLegal.com"
        },
        "description":{
            "th":"แก้ไขโปรไฟล์สำหรับทนายความ ชื่อ ที่อยู่ เบอร์ติดต่อ ความถนัด ประสบการณ์ ",
            "en":"Profile edit for lawyers. E.g. Name, Address, Contact, Expertise and Experiences"
        }
    }

    CLIENT_PROFILE_EDIT = {
        "title": {
            "th":"โปรไฟล์ลูกความ  -  แก้ไข | DFastLegal.com",
            "en":"Client profile - edit | DFastLegal.com"
        },
        "description":{
            "th":"แก้ไขโปรไฟล์สำหรับลูกความ",
            "en":"Profile edit for lawyer"
        }
    }
}