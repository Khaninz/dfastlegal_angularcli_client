﻿import { Injectable } from '@angular/core';

@Injectable()
export class AppLanguageService {
    constructor() {
    }

    //CTRL + M,M in visual studio to collapse all

    //visual studio code
    //CTRL + K , CTRL + 0 fold all
    //CTRL + K , CTRL + J unfold all
    //template
    LABEL = {
        "th": "",
        "en": "",
        "jp": "",
        "cn": ""
    }

    //COMMON
    TITLE = {
        "th": "คำนำหน้า",
        "en": "Title",
        "jp": "",
        "cn": ""
    }
    MR = {
        "th": "นาย",
        "en": "Mr.",
        "jp": "",
        "cn": ""
    }
    MRS = {
        "th": "นาง",
        "en": "Mrs.",
        "jp": "",
        "cn": ""
    }
    MS = {
        "th": "นางสาว",
        "en": "Ms.",
        "jp": "",
        "cn": ""
    }
    F_NAME = {
        "th": "ชื่อ",
        "en": "First name",
        "jp": "",
        "cn": ""
    }
    L_NAME = {
        "th": "นามสกุล",
        "en": "Last name",
        "jp": "",
        "cn": ""
    }
    EMAIL = {
        "th": "อีเมลล์",
        "en": "Email",
        "jp": "",
        "cn": ""
    }
    FIRM = {
        "th": "บริษัทกฏหมาย",
        "en": "Law firm",
        "jp": "",
        "cn": ""
    }
    OFFICE = {
        "th": "สำนักงานกฏหมาย",
        "en": "Law office",
        "jp": "",
        "cn": ""
    }
    SEARCH = {
        "th": "ค้นหา",
        "en": "Search",
        "jp": "",
        "cn": ""
    }
    EXPERTISE = {
        "th": "ความถนัด",
        "en": "Expertises",
        "jp": "",
        "cn": ""
    }
    LAWYER = {
        "th": "ทนาย",
        "en": "Lawyer",
        "jp": "",
        "cn": ""
    }
    RATINGS = {
        "th": "ระดับความนิยม",
        "en": "Ratings",
        "jp": "",
        "cn": ""
    }
    PROFILE = {
        "th": "โปรไฟล์",
        "en": "Profile",
        "jp": "",
        "cn": ""
    }
    QUALIFICATIONS = {
        "th": "คุณสมบัติการศึกษา",
        "en": "Qualifications",
        "jp": "",
        "cn": ""
    }
    CONTACTS = {
        "th": "ช่องทางการติดต่อ",
        "en": "Contacts",
        "jp": "",
        "cn": ""
    }
    MOBILE = {
        "th": "เบอร์โทรศัพท์",
        "en": "Mobile no.",
        "jp": "",
        "cn": ""
    }
    EXPERIENCE = {
        "th": "ประสบการณ์การทำงาน",
        "en": "Work Experience",
        "jp": "",
        "cn": ""
    }
    BACK = {
        "th": "ย้อนกลับ",
        "en": "Back",
        "jp": "",
        "cn": ""
    }
    CALL_NOW = {
        "th": "โทรทันที",
        "en": "Call now",
        "jp": "",
        "cn": ""
    }
    STATUS = {
        "th": "สถานะ",
        "en": "Status",
        "jp": "",
        "cn": ""
    }
    SAVE = {
        "th": "บันทึก",
        "en": "Save",
        "jp": "",
        "cn": ""
    }
    CONTACT_INFO = {
        "th": "ช่องทางการติดต่อ",
        "en": "Contact information",
        "jp": "",
        "cn": ""
    }
    INSTITUTE = {
        "th": "สถาบันการศึกษา (กรุณาใส่เป็นภาษาอังกฤษ)",
        "en": "Institute",
        "jp": "",
        "cn": ""
    }
    ADD = {
        "th": "เพิ่ม",
        "en": "Add",
        "jp": "",
        "cn": ""
    }
    PW = {
        "th": "พาสเวิร์ด",
        "en": "Password",
        "jp": "",
        "cn": ""
    }
    CONFIRM_PW = {
        "th": "ยืนยันพาสเวิร์ด",
        "en": "Confirm Password",
        "jp": "",
        "cn": ""
    }
    REGISTER = {
        "th": "ลงทะเบียน",
        "en": "Register",
        "jp": "",
        "cn": ""
    }
    ID = {
        "th": "หมายเลขบัตรประชาชน",
        "en": "Personal ID or Passport no.",
        "jp": "",
        "cn": ""
    }
    APPLY_AS_COMP = {
        "th": "ดำเนินการในฐานะบริษัท",
        "en": "Operate as company",
        "jp": "",
        "cn": ""
    }
    COMP_NAME = {
        "th": "ชื่อบริษัท",
        "en": "Company name",
        "jp": "",
        "cn": ""
    }
    COMP_ID = {
        "th": "เลขทะเบียนนิติบุคคล",
        "en": "Company ID",
        "jp": "",
        "cn": ""
    }
    COMP_DOCS = {
        "th": "เอกสารรับรองบริษัท",
        "en": "Company documents",
        "jp": "",
        "cn": ""
    }
    LANGUAGE = {
        "th": "ภาษา",
        "en": "Language",
        "jp": "",
        "cn": ""
    }
    YOUR_CASE = {
        "th": "เคสของคุณ",
        "en": "Your cases",
        "jp": "",
        "cn": ""
    }
    CASE_ID = {
        "th": "เคสไอดี",
        "en": "Case ID",
        "jp": "",
        "cn": ""
    }
    CASE_STATUS = {
        "th": "ทนายติดต่อแล้ว",
        "en": "Lawyer responded",
        "jp": "",
        "cn": ""
    }
    CASE_DETAIL = {
        "th": "รายละเอียดของคดี",
        "en": "Case detail",
        "jp": "",
        "cn": ""
    }
    CASE_PROVINCES = {
        "th": "จังหวัด",
        "en": "Provinces",
        "jp": "",
        "cn": ""
    }
    CASE_CREATED_AT = {
        "th": "เวลาที่สร้าง",
        "en": "Created at",
        "jp": "",
        "cn": ""
    }
    SELECTED_LAWYERS = {
        "th": "ทนายที่คุณเลือก",
        "en": "Selected Lawyers",
        "jp": "",
        "cn": ""
    }
    LAWYER_PROFILE = {
        "th": "โปรไฟล์ทนาย",
        "en": "Lawyer Profile",
        "jp": "",
        "cn": ""
    }
    QUESTION = {
        "th": "คำถาม",
        "en": "Question",
        "jp": "",
        "cn": ""
    }
    NEXT = {
        "th": "ต่อไป",
        "en": "Next",
        "jp": "",
        "cn": ""
    }
    DETAIL = {
        "th": "รายละเอียด",
        "en": "Detail",
        "jp": "",
        "cn": ""
    }
    LAWYER_REGIST = {
        "th": "สมัครในฐานะทนาย",
        "en": "Lawyer registration",
        "jp": "",
        "cn": ""
    }
    WORK_WITH_US_H = {
        "th": "ทำงานร่วมกับเรา",
        "en": "Working with us.",
        "jp": "",
        "cn": ""
    }
    WORK_WITH_US = {
        "th": "ในการทำงานร่วมกับเรา เพียงคุณส่งข้อมูลของท่านมาเพื่อให้เรายืนยันตัวตนของท่าน เมื่อยืนยันสำเร็จคุณจะมีหน้าโปรไฟล์ของท่านเอง ท่านสามารถแก้ไขข้อมูลความถนัดและรายละเอียดที่ท่านต้องการแสดงได้ในหน้านี้",
        "en": "Please send your information for registration, upon success you will have access to your profile page where you can set you expertiese and submit qualification for apporval.",
        "jp": "",
        "cn": ""
    }
    BILLING_NAME = {
        "th": "ชื่อ",
        "en": "Name",
        "jp": "",
        "cn": ""
    }
    ADDRESS1 = {
        "th": "ที่อยู่1",
        "en": "Address1",
        "jp": "",
        "cn": ""
    }
    ADDRESS2 = {
        "th": "ที่อยู่2",
        "en": "Address2",
        "jp": "",
        "cn": ""
    }
    PROVINCE = {
        "th": "จังหวัด",
        "en": "Province",
        "jp": "",
        "cn": ""
    }
    DISTRICT = {
        "th": "เขต/อำเภอ",
        "en": "District",
        "jp": "",
        "cn": ""
    }
    COUNTRY = {
        "th": "ประเทศ",
        "en": "Country",
        "jp": "",
        "cn": ""
    }
    POST_CODE = {
        "th": "รหัสไปรษณีย์",
        "en": "Post code",
        "jp": "",
        "cn": ""
    }
    LOG_OUT = {
        "th": "ออกจากระบบ",
        "en": "Log out",
        "jp": "",
        "cn": ""
    }
    BAHT = {
        "th": "บาท",
        "en": "Baht",
        "jp": "",
        "cn": ""
    }
    COMPANY = {
        "th": "บริษัท",
        "en": "Company",
        "jp": "",
        "cn": ""
    }

    //Laywer Profile Edit page
    USE_MATCHING = {
        "th": "บริการในการจัดหาและส่งข้อมูลของลูกความด้วยระบบอัตโนมัติ",
        "en": "Matching service, we will notify and sent you potential client information automatically.",
        "jp": "",
        "cn": ""
    }
    USE_LISTING = {
        "th": "บริการในการลิสติ้งรายชื่อของคุณเพื่อให้ลูกความพบคุณในเวบไซต์",
        "en": "Listing service, show your profile in our listing page and let client able to contact you directly.",
        "jp": "",
        "cn": ""
    }
    YOUR_SERVICES = {
        "th": "บริการที่คุณต้องการ",
        "en": "Your services",
        "jp": "",
        "cn": ""
    }
    SHORT_DESC_H = {
        "th": "คำอธิบายอย่างสั้นเกี่ยวกับคุณ",
        "en": "Your short desctiption",
        "jp": "",
        "cn": ""
    }
    ADD_QUALIFICATION = {
        "th": "เพิ่มคุณสมบัติการศึกษา",
        "en": "Add your qualificaiton",
        "jp": "",
        "cn": ""
    }
    DEGREE_LEVEL = {
        "th": "ระดับการศึกษา",
        "en": "Degree level",
        "jp": "",
        "cn": ""
    }
    FIELD = {
        "th": "สาขา (กรุณาใส่เป็นภาษาอังกฤษ)",
        "en": "Field of study",
        "jp": "",
        "cn": ""
    }
    EVIDENCE_FILE = {
        "th": "ไฟล์เอกสารปริญญาหรือทรานสคริป",
        "en": "Degree approval or Transcript file",
        "jp": "",
        "cn": ""
    }
    CHANGE_PW = {
        "th": "เปลี่ยนพาสเวิร์ด",
        "en": "Change Password",
        "jp": "",
        "cn": ""
    }
    CURRENT_PW = {
        "th": "พาสเวิร์ดปัจจุบัน",
        "en": "Current password",
        "jp": "",
        "cn": ""
    }
    NEW_PW = {
        "th": "พาสเวิร์ดใหม่",
        "en": "New password",
        "jp": "",
        "cn": ""
    }
    CONFIRM_NEW_PW = {
        "th": "ยืนยันพาสเวิร์ดใหม่",
        "en": "Confirm new password",
        "jp": "",
        "cn": ""
    }
    SHORT_DESC_HINT = {
        "th": "คำอธิบายโดยย่อจะเป็นสิ่งแรกและสิ่่งสำคัญที่ลูกความจะทราบเกี่ยวกับตัวท่าน",
        "en": "Your short description is a first and important message to your potential client.",
        "jp": "",
        "cn": ""
    }
    ADD_QUALIFICATION_WARN = {
        "th": "ทุกครั้งที่เพิ่มคุณสมบัติการศึกษา/ตั๋วทนาย/เอกสารบริษัท ระบบจะตั้งสถานะของคุณเป็น 'รอการตรวจสอบเอกสาร'",
        "en": "Adding any qualification/license/company documents will set your status to 'waiting for approval'",
        "jp": "",
        "cn": ""
    }
    FILE_IS_REQUIRED = {
        "th": "คุณจำเป็นต้องอัพโหลดเอกสารเพื่อการตรวจสอบคุณสมบัติ",
        "en": "File for you qualification is required for verification.",
        "jp": "",
        "cn": ""
    }
    FILE_SIZE_WARN = {
        "th": "กรุณาใช้ไฟล์ที่มีขนาดเล็กกว่า 3MB ค่ะ",
        "en": "Please upload file with less than 3MB",
        "jp": "",
        "cn": ""

    }
    LAWYER_TICKET_H = {
        "th": "ตั๋วทนาย",
        "en": "Lawyer license",
        "jp": "",
        "cn": ""
    }
    LAWYER_TICKET_EXP = {
        "th": "คุณจะถูกจับคู่ให้กับลูกความที่มีความจำเป็นในการขึ้นศาลเป็นการเพิ่มเติม",
        "en": "Additionally, you will be matched with client who require an action in the court.",
        "jp": "",
        "cn": ""
    }
    LAWYER_TICKET_FILE = {
        "th": "ไฟล์ตั๋วทนาย",
        "en": "Lawyer license file",
        "jp": "",
        "cn": ""
    }
    WAIT_FOR_APPROVAL = {
        "th": "สถานะ: รอตรวจสอบเอกสาร",
        "en": "Status: wait for document approval",
        "jp": "",
        "cn": ""
    }
    APPROVE_EXP = {
        "th": "กรุณาส่งเอกสารตามหัวข้อด้านล่างเพื่อการตรวจสอบ",
        "en": "Please submit document below for approval",
        "jp": "",
        "cn": ""
    }
    APPROVE_QUAL = {
        "th": "เอกสารคุณวุฒิปริญญา",
        "en": "Your qualification documents",
        "jp": "",
        "cn": ""
    }
    HERE = {
        "th": "ที่นี่",
        "en": "Here",
        "jp": "",
        "cn": ""
    }
    APPROVE_LICENSE = {
        "th": "เอกสารตั๋วทนาย(ถ้ามี)",
        "en": "Your lawyer license(if any)",
        "jp": "",
        "cn": ""
    }
    APPROVE_PHONE_NUMBER = {
        "th": "เบอร์โทรศัพท์ของคุณ มีความสำคัญอย่างมากเพื่อให้ลูกความสามารถเข้าถึงคุณได้ทันที",
        "en": "Your Phone number, very important for client to contact you directly.",
        "jp": "",
        "cn": ""
    }
    APPROVE_UPON_SUCCESS = {
        "th": "เมื่อการตรวจสอบดำเนินการเรียบร้อย กล่องข้อความนี้จะหายไป หมายความว่าโปรไฟล์ของท่านได้รับการยืนยันและแสดงในระบบของเรา หากไม่สำเร็จเราจะมีการอีเมล์เพื่อขอข้อมูลเพิ่มจากคุณ",
        "en": "Upon approval success, this text box will disappear and means that your profile is approved and shown in the system. If not success, we will contact you for more information.",
        "jp": "",
        "cn": ""
    }
    CHANGE_PROFILE_PIC = {
        "th": "กดที่รูปภาพเพื่อเปลี่ยน",
        "en": "click picture to change",
        "jp": "",
        "cn": ""
    }
    CENTER_PROFILE_PIC = {
        "th": "กรุณาใช้ภาพที่ใบหน้าของท่านอยู่ตรงกลางของภาพ",
        "en": "use a picture that your face are at centered",
        "jp": "",
        "cn": ""
    }

    DOCS_VERIFICATION_ONLY = {
        "th": "ลูกความจะไม่เห็นเอกสารเหล่านี้ เราใช้เอกสารเหล่านี้เพื่อการตรวจสอบเท่านั้น",
        "en": "these documents will not be shown to client, for verification only.",
        "jp": "",
        "cn": ""
    }
    FILE_LIMIT_SIZE = {
        "th": "ไฟล์ต้องมีขนาดเล็กกว่า 3 เมกกะไบต์ ",
        "en": "file must be less than 3MB",
        "jp": "",
        "cn": ""
    }




    //HOME page
    SEVEN_WORDS = {
        "th": "ที่ที่ลูกความและทนายพบกันอย่างมีประสิทธิภาพ",
        "en": "Where million clients meet greate lawyers, quick!",
        "jp": "",
        "cn": ""
    }
    CALL_TO_ACTION = {
        "th": "",
        "en": "",
        "jp": "",
        "cn": ""
    }
    FIND_LAWYER_NOW = {
        "th": "เริ่มค้นหาทนายทันที!",
        "en": "Find lawyer now!",
        "jp": "",
        "cn": ""
    }
    

    CLI_BTN = {
        "th": "ลูกความ",
        "en": "Client",
        "jp": "",
        "cn": ""
    }
    LAW_BTN = {
        "th": "ทนาย",
        "en": "Lawyer",
        "jp": "",
        "cn": ""
    }
    CLI_ACTION = {
        "th": "ฝากปัญหาของท่านไว้ที่นี่",
        "en": "Leave your problem with us",
        "jp": "",
        "cn": ""
    }
    LAW_ACTION = {
        "th": "ทนาย: สมัครร่วมงานกับเรา",
        "en": "Lawyer: Work with us",
        "jp": "",
        "cn": ""
    }
    CLI_CNT_1_H = {
        "th": "รวดเร็วและไม่ยุ่งยาก",
        "en": "Quick and hassle free!",
        "jp": "",
        "cn": ""
    }
    CLI_CNT_1 = {
        "th": "คุณสามารถเข้าถึงและพูดคุยกับทนายหลายๆท่านได้อย่างรวดเร็ว โดยที่ไม่ต้องยุ่งยากกับการหาข้อมูลรายบุคคลหรืออธิบายข้อมูลซ้ำๆให้กับทนาย เพราะเราทราบว่าทุกปัญหามีความซับซ้อนที่ใช้เวลาในการอธิบายที่รอการแก้ไขอยู่ ",
        "en": "You can access too many lawyers quick, without any hassle. No repetition of information given, No more dig down in directories of lawyer just to find a lawyer. Because your problem is too much of a problem already!",
        "jp": "",
        "cn": ""
    }
    CLI_CNT_2_H = {
        "th": "เน้นที่การแก้ปัญหาให้คุณ",
        "en": "Aim to solve problem",
        "jp": "",
        "cn": ""
    }
    CLI_CNT_2 = {
        "th": "เรานำข้อมูลของคุณไปวิเคราะห์โดยอัตโนมัติ เพื่อให้คุณได้เลือกทนายที่ตรงกับปัญหาที่คุณมีอยู่ ขจัดปัญหาการทำงานที่ไม่ตรงความสามารถของทนาย ลดระยะเวลาในการจัดการปัญหาของคุณให้เร็วที่สุด ",
        "en": "Our system provide you with a lawyer that expertise on your problem. Increase efficiency on problem solving and shorten the time for you to solve the problem.",
        "jp": "",
        "cn": ""
    }
    CLI_CNT_3_H = {
        "th": "งบประมาณที่ชัดเจน",
        "en": "Plan on your budget",
        "jp": "",
        "cn": ""
    }
    CLI_CNT_3 = {
        "th": "ก่อนดำเนินการเราสามารถแจ้งคุณล่วงหน้าเกี่ยวกับงบประมาณที่จำเป็นต่อการดำเนินการของทนาย เพื่อให้คุณใช้เป็นเครื่องมือประกอบการตัดสินใจและวางแผนในการแก้ปัญหาของคุณได้ทันที",
        "en": "Before proceed to work with a lawyer, we estimate a budget for you in advance. this is to provide you a better decision making and help you plan how to solve your problem out immediately.",
        "jp": "",
        "cn": ""
    }
    CLI_BRW_LAWS = {
        "th": "ดูรายชื่อทนาย",
        "en": "Lawyer Profiles",
        "jp": "",
        "cn": ""
    }
    LAW_CNT_1_H = {
        "th": "เข้าถึงได้ง่ายและรวดเร็ว",
        "en": "Easy and Quick Access, Passively",
        "jp": "",
        "cn": ""
    }
    LAW_CNT_1 = {
        "th": "สำหรับคุณที่เป็นทนายความที่มีความเชี่ยวชาญด้านกฎหมาย แต่คุณไม่อาจเข้าถึงลูกความได้โดยง่าย เราช่วยคุณในการหาลูกความให้เหมาะกับงานด้านกฎหมายที่คุณถนัด โดยคุณไม่ต้องเสียเวลาในการหาลูกความด้วยตนเอง เป็นอีกทางเลือกหนึ่งที่คุณจะได้ทำงานโดยอิสระและไม่ต้องพึ่งพาการติดต่อจากเพื่อนหรือคนรู้จักเพียงอย่างเดียว",
        "en": "You have legal practice but you have difficulty in access to clients. We help you find clients that are fit for your legal practice without wasting your time in looking for the clients. We provide alternative for you to work independently, without clinging on the sole connections of your friends or acquaintance.",
        "jp": "",
        "cn": ""
    }
    LAW_CNT_2_H = {
        "th": "ร่วมเป็นทนายความกับเรา",
        "en": "Join DFastlegal",
        "jp": "",
        "cn": ""
    }
    LAW_CNT_2 = {
        "th": "สมัครเป็นทนายความกับ ดีฟาสท์ลีกอล โดยไม่เสียค่าใช้จ่าย เราช่วยคุณได้พบกับลูกความได้ง่าย สะดวกและรวดเร็ว เราเป็นเครื่องมือช่วยคุณเก็บข้อมูลต่างๆ ปัญหาและความต้องการทางกฎหมายของลูกความ และถ้าตรงกับความถนัดของคุณ เราจะแนะนำลูกความนั้นให้กับคุณ และเรายังเป็นพื้นที่ให้คุณได้แสดงคุณวุฒิทางการศึกษา ประสบการณ์และผลงานของตนเอง",
        "en": "Join Dfastlegal, free. We will help you easily, convenient and quickly connect to clients and match your legal expertise with the legal need of clients. We collect client’s information and learn their legal problems and needs. If they are fit for you, we will introduce them to you. And, we offer you space to show your educational background, experience and works.",
        "jp": "",
        "cn": ""
    }
    LAW_CNT_3_H = {
        "th": "ไม่มีการเสียค่าคอมมิชชั่น",
        "en": "No commission fee",
        "jp": "",
        "cn": ""
    }
    LAW_CNT_3 = {
        "th": "เราดำเนินการให้บริการในรูปแบบจ่ายตามจำนวนที่ได้คุยกับลูกความจริง ซึ่งมีค่าใช้จ่ายต่ำกว่าการคิดค่าบริการแบบคอมมิชชั่นอย่างมาก ซึ่งค่าคอมมิชชั่นแบบปกติของท่านหนึ่งครั้ง อาจจะมีมูลค่าเทียบเท่ากับจำนวนครั้งในการเข้าถึงลูกความของท่านได้มากกว่าพันครั้งทีเดียว",
        "en": "We provide our service as per contact base, which is much lower than your normal commision fee. Your one normal commission fee could worth thousands of contacts to your potential clients!",
        "jp": "",
        "cn": ""
    }
    FEAT_LAWYERS = {
        "th": "ทนายที่ร่วมงานกับเรา",
        "en": "Featured lawyers",
        "jp": "",
        "cn": ""
    }

    //Client inquire page 
    CLI_INQ_H = {
        "th": "กรุณากรอกข้อมูลเบื้องต้นและข้อมูลเกี่ยวกับปัญหาของท่าน",
        "en": "Please fill in the form, and explain your problem.",
        "jp": "",
        "cn": ""
    }
    Q_CASE = {
        "th": "กรุณาอธิบายปัญหาของท่านอย่างละเอียด",
        "en": "Please explain you problem throughly",
        "jp": "",
        "cn": ""
    }
    Q_CASE_PLH = {
        "th": "ระบุรายละเอียดปัญหาของท่านที่นี่...",
        "en": "Explaing you problem here...",
        "jp": "",
        "cn": ""
    }
    CLI_INQ_ACTION = {
        "th": "ดำเนินการฝากเรื่องไว้กับเรา",
        "en": "Leave your problem here.",
        "jp": "",
        "cn": ""
    }
    Q_PROV = {
        "th": "กรุณาระบุพื้นที่จังหวัดที่ท่านต้องการดำเนินการหรือต้องเกี่ยวข้องในการดำเนินการ",
        "en": "Please specify city or regoin, that your case occured or related",
        "jp": "",
        "cn": ""
    }

    //Lawyer Inquire page
    LI_DES_H = {
        "th": "ทำงานร่วมกับเรา",
        "en": "Working with us",
        "jp": "",
        "cn": ""
    }
    LI_DES = {
        "th": "ในการทำงานร่วมกับเรา เพียงคุณส่งข้อมูลของท่านมาเพื่อให้เรายืนยันตัวตนของท่าน เมื่อยืนยันสำเร็จคุณจะมีหน้าโปรไฟล์ของท่านเอง ท่านสามารถแก้ไขข้อมูลความถนัดและรายละเอียดที่ท่านต้องการแสดงได้ในหน้านี้",
        "en": "",
        "jp": "",
        "cn": ""
    }
    LI_LAW_INFO = {
        "th": "รายละเอียดของคุณ",
        "en": "Your information",
        "jp": "",
        "cn": ""
    }
    IS_COMP = {
        "th": "ดำเนินการแบบ บริษัท/ออฟฟิศ",
        "en": "Apply as law firm/office",
        "jp": "",
        "cn": ""
    }
    LI_INQ_ACTION = {
        "th": "ลงทะเบียน",
        "en": "Register",
        "jp": "",
        "cn": ""
    }

    //case response page
    HELLO = {
        "th": "สวัสดีครับ",
        "en": "Hello!",
        "jp": "こんにちは",
        "CH": "你好"
    }
    NO_USER_ALERT = {
        "th": "กรุณาเข้าสู่ระบบเพื่อดำเนินการ",
        "en": "Please log in first to proceed."
    }
    WRONG_LAWYER_ALERT = {
        "th": "ขออภัยค่ะ คุณไม่ได้เข้าสู่ระบบด้วยทนายที่ถูกร้องขอ ระบบจะออกจากระบบให้โดยอัตโนมัติ",
        "en": "Sorry, you are not logged in with requested lawyer account, system will automatically logout."
    }
    DECLINE_RES_THANKS = {
        "th": "ขอบคุณสำหรับการตอบกลับของท่าน",
        "en": "thank you for your response."
    }
    CONTACT_FEE = {
        "th": "ค่าข้อมูลการติดต่อ",
        "en": "Contact fee",
        "jp": "",
        "cn": ""
    }
    SHOPPING_CART = {
        "th": "สินค้าในตะกร้า",
        "en": "Shopping cart",
        "jp": "",
        "cn": ""
    }


    //payment page
    SELECT_CARD = {
        "th": "เลือก/เพิ่ม บัตรเครดิต",
        "en": "Select/Add credit card"
    }
    SELECT_ADDRESS = {
        "th": "เลือก/เพิ่ม ที่อยู่ใบเสร็จ",
        "en": "Select/Add billing address"
    }
    PAYMENT_HEADER = {
        "th": "กรุณาเลือกช่องทางการชำระเงิน",
        "en": "Select payment method",
        "jp": "",
        "cn": ""
    }
    EMPTY_CARD = {
        "th": "กรุณาเพิ่มข้อมูลบัตรเครดิตด้านล่าง",
        "en": "Pleasee add you credit card information below",
        "jp": "",
        "cn": ""
    }
    EMPTY_ADDRESS = {
        "th": "กรุณาเพิ่มที่อยุ่ใบเสร็จของท่านด้านล่าง",
        "en": "Please add you billing address below.",
        "jp": "",
        "cn": ""
    }
    ADD_CARD = {
        "th": "เพิ่มบัตรเครดิต",
        "en": "Add credit card",
        "jp": "",
        "cn": ""
    }
    ADD_ADDRESS = {
        "th": "เพิ่มที่อยู่",
        "en": "Add address",
        "jp": "",
        "cn": ""
    }
    ADDRESS_MODAL_H = {
        "th": "เลือกที่อยู่ใบเสร็จ",
        "en": "Select billing address",
        "jp": "",
        "cn": ""
    }
    CARD_MODAL_H = {
        "th": "เลือกบัตรเครดิต",
        "en": "Choose credit card",
        "jp": "",
        "cn": ""
    }

    //Inquire thank you page.
    INQ_THANK_H = {
        "th": "ขอบคุณสำหรับข้อมูลของท่าน",
        "en": "thank you for your information.",
        "jp": "",
        "cn": ""
    }
    INQ_THANK = {
        "th": "ขณะนี้เรากำลังเร่งพัฒนาแอพพลิเคชั่นเพื่อให้ตอบสนองกับความต้องการของท่าน เราจะแจ้งให้ท่านทราบเมื่อแอพพลิเคชั่นของเราพร้อมให้บริการแล้วอีกครั้งค่ะ",
        "en": "We are currently developig our appliction to suit you needs, we will inform you again when our app is ready.",
        "jp": "",
        "cn": ""
    }
    INQ_THANK_BTN = {
        "th": "กลับสู่หน้าแรก",
        "en": "Back to main page",
        "jp": "",
        "cn": ""
    }

    //Lawyers page
    SRY_NO_RESULT = {
        "th": "ขออภัยค่ะ ไม่พบทนายตามเงื่อนไขการค้นหาของท่านค่ะ",
        "en": "Sorry, No result found.",
        "jp": "",
        "cn": ""
    }

    //Header
    LOGIN = {
        "th": "เข้าสู่ระบบ",
        "en": "Login",
        "jp": "",
        "cn": ""
    }
    FOR_CLIENT = {
        "th": "สำหรับลูกความ",
        "en": "For client",
        "jp": "",
        "cn": ""
    }
    CLIENT_SIGNUP = {
        "th": "ลูกความ: สมัครที่นี่",
        "en": "Client: Sign up!",
        "jp": "",
        "cn": ""
    }
    FOR_LAWYER = {
        "th": "สำหรับทนาย",
        "en": "For attorney",
        "jp": "",
        "cn": ""
    }
    LAWYER_SIGNUP = {
        "th": "ทนาย:สมัครทันที!",
        "en": "Attorney: Sign up!",
        "jp": "",
        "cn": ""
    }

    //Footer
    HELP_SUPPORT = {
        "th": "สนับสนุนการบริการ",
        "en": "Help & Support",
        "jp": "",
        "cn": ""
    }
    PRIVACY_POLICY = {
        "th": "นโยบายความเป็นส่วนตัว",
        "en": "Privacy policy",
        "jp": "",
        "cn": ""
    }
    TERM_OF_USE = {
        "th": "ข้อตกลงการใช้งาน",
        "en": "Term of use",
        "jp": "",
        "cn": ""
    }
    PAYMENT_POLICY = {
        "th": "นโยบายการชำระเงิน",
        "en": "Payment policy",
        "jp": "",
        "cn": ""
    }
    ABOUT_US = {
        "th": "เกี่ยวกับเรา",
        "en": "About us",
        "jp": "",
        "cn": ""
    }
    VISION = {
        "th": "วิสัยทัศน์",
        "en": "Our vision",
        "jp": "",
        "cn": ""
    }
    TEAM = {
        "th": "ทีม",
        "en": "Our team",
        "jp": "",
        "cn": ""
    }
    JOIN_US = {
        "th": "ร่วมงานกับเรา",
        "en": "Join us",
        "jp": "",
        "cn": ""
    }
    COMP_ADDRESS_LINE1 = {
        "th": "24/30 ถ.วิภาวดีรังสิต แขวงสนามบิน",
        "en": "24/30 Viphavadi-Rangsit Rd.",
        "jp": "",
        "cn": ""
    }
    COMP_ADDRESS_LINE2 = {
        "th": "เขตดอนเมือง กรุงเทพฯ 10210",
        "en": "Airport dist., Donmuang, Bangkok 10210",
        "jp": "",
        "cn": ""
    }

    CREDIT1 = {
        "th": "คณิน ชมภูศรี",
        "en": "Khanin Chomphusri",
        "jp": "",
        "cn": ""
    }
    CREDIT2 = {
        "th": "สิริไพฑูรย์ สวัสดิศักดิ์",
        "en": "Siriphaitun Sawasdisak",
        "jp": "",
        "cn": ""
    }

    //Questions page
    PROCEDURE = {
        "th": "ขั้นตอนการทำงาน",
        "en": "Procedure",
        "jp": "",
        "cn": ""
    }

    PROCEDURE_DETAIL = {
        "th": "ทางเราจะขอข้อมูลของคุณเพื่อนำไปช่วยหาทนายที่ตรงกับปัญหาของคุณ แบบสอบถามใช้เวลาเพียง 5 นาที และหลังจากที่คุณให้ข้อมูลเสร็จเราสามารถคัดเลือกทนายที่ตรงกับปัญหาของคุณเพื่อให้คุณได้เลือกทันที ท่านสามารถเลือกทนายๆหลายๆท่านได้ แต่จะมีทนายตอบกลับพอดีกับจำนวนทนายที่คุณได้กำหนดไว้",
        "en": "We will gather information regard to your problem. this questionaire will take only 5 minutes. Afterward, we will provide you with lawyers that match your problem immediately. You can choose as many lawyers as you like but amount lawyer to be reponse will not exceed the limit you preferred.",
        "jp": "",
        "cn": ""
    }

    Q_EXPLAIN = {
        "th": "อธิบายคำถาม",
        "en": "Question explanation",
        "jp": "",
        "cn": ""
    }
    A_EXPLAIN = {
        "th": "อธิบายคำตอบ",
        "en": "Answer explanation",
        "jp": "",
        "cn": ""
    }
    ONLY_FOR_CLIENT = {
        "th": "ขออภัยค่ะ บริการนี้สำหรับลูกความเท่านั้นค่ะ :)",
        "en": "Sorry, this service is only for client. :)",
        "jp": "",
        "cn": ""
    }
    PLEASE_LOGIN = {
        "th": "กรูณาล๊อคอินหรือสมัครเข้าใช้บริการเพื่อดำเนินการต่อ",
        "en": "Please login or sign up to proceed.",
        "jp": "",
        "cn": ""
    }
    PLEASE_SELECT_ANSWER = {
        "th": "กรุณาเลือกคำตอบเพื่อดำเนินการต่อค่ะ",
        "en": "Please select an answer to proceed",
        "jp": "",
        "cn": ""
    }
    //Case detail form page
    CASE_DETAIL_FORM_H = {
        "th": "รายละเอียดของปัญหาและความต้องการของคุณ",
        "en": "Detailed information and requirement for your problem.",
        "jp": "",
        "cn": ""
    }
    PLS_ADD_DETAIL = {
        "th": "กรุณาอธิบายปัญหาของคุณอย่างละเอียด เพื่อเป็นข้อมูลให้ทางทนายใช้ประกอบการพิจารณาในการให้คำปรึกษา",
        "en": "Please describe you problem in detail, this will be used by our lawyers for consideration and help your problem.",
        "jp": "",
        "cn": ""
    }
    DETAIL_HERE = {
        "th": "ระบุรายละเอียดปัญหาของท่านที่นี่",
        "en": "Describe your problem here...",
        "jp": "",
        "cn": ""
    }
    NO_CONTACT_PLS = {
        "th": "กรุณาอย่าใส่อีเมล์,เบอร์โทรศัพท์ หรือช่องทางใดๆที่ใช้ในการติดต่อที่นี่ ขอบคุณค่ะ :)",
        "en": "Please do not put any of your contact information here. thank you :)",
        "jp": "",
        "cn": ""
    }
    SELECT_PROVINCE = {
        "th": "กรุณาเลือกพื้นที่จังหวัดในการดำเนินการ",
        "en": "Please select province of operation",
        "jp": "",
        "cn": ""
    }
    DESCRIBE_PROVINCE = {
        "th": "จังหวัดที่ท่านเลือกคือขอบเขตที่ท่านต้องการให้ทนายดำเนินการหรือปรึกษาติดต่อตลอดการทำงาน",
        "en": "Specified province is province that you want lawyers to operate for you problem.",
        "jp": "",
        "cn": ""
    }
    SELECT_LAWYER_NUM = {
        "th": "กรุณาเลือกจำนวนทนายที่ท่านต้องการปรึกษาเบื้องต้นก่อนตัดสินใจเลือกทนายในการดำเนินการ",
        "en": "Please select number of lawyers for you consideration before selection.",
        "jp": "",
        "cn": ""
    }
    DESCRIBE_LAWYER_NUM = {
        "th": "จำนวนที่ท่านกำหนดคือจำนวนทนายที่จะมีการติดต่อพูดคุยให้คำปรึกษากับท่าน เพื่อให้ท่านมีตัวเลือกในการตัดสินใจว่าทนายท่านใดจะเหมาะสมในการทำคดีกับท่าน",
        "en": "the number you specified is the amount of lawyer who will contact you for your consideration. You can select who will suitable for your case the most.",
        "jp": "",
        "cn": ""
    }
    TO_SELECT_LAWYER = {
        "th": "ดำเนินการเลือกทนาย",
        "en": "Proceed to lawyer selection",
        "jp": "",
        "cn": ""
    }
    ERR_DETAIL_REQ = {
        "th": "กรุณาใส่รายละเอียดของปัญหา",
        "en": "Case detail is required",
        "jp": "",
        "cn": ""
    }
    SAVE_DRAFT = {
        "th": "บันทึกร่าง",
        "en": "Save draft",
        "jp": "",
        "cn": ""
    }
    PROCEED = {
        "th": "ดำเนินการต่อ",
        "en": "Proceed",
        "jp": "",
        "cn": ""
    }

    //Select lawyers page
    THX_FOR_INFO_H = {
        "th": "ขอบคุณสำหรับข้อมูลของท่าน",
        "en": "thank you for your information.",
        "jp": "",
        "cn": ""
    }
    THX_FOR_INFO = {
        "th": "เราได้ทำการคัดเลือกทนายที่มีความเชียวชาญในปัญหาของท่าน กรุณาเลือกทนายที่ท่านสนใจโดยการการอ่านโปรไฟล์ของทนายแต่ละท่าน คุณสามารถเลือกทนายได้มากเท่าที่ท่านต้องการ แต่ว่าจะมีทนายตอบกลับตามจำนวนที่ทำกำหนดไว้เท่านั้น ทั้งนี้เพื่อให้ทนายแต่ละท่านได้พิจารณาในการตกลงสนใจในปัญหาของท่านเช่นเดียวกัน",
        "en": "We have select lawyers who has expertise that match your problem, please browse through their profile and select whom you feel comfortable. You can select as many lawyers as you like but upto the number you have limited will contact you back.",
        "jp": "",
        "cn": ""
    }
    FINISH_SELECTION = {
        "th": "เลือกเสร็จแล้ว",
        "en": "Finish selection",
        "jp": "",
        "cn": ""
    }
    LAWYER_LIST = {
        "th": "รายชื่อทนาย",
        "en": "Lawyer list",
        "jp": "",
        "cn": ""
    }
    SELECT = {
        "th": "เลือก",
        "en": "Select",
        "jp": "",
        "cn": ""
    }
    PLS_SELECT_LAWYER = {
        "th": "กรุณาเลือกทนายเพื่อดำเนินการต่อค่ะ",
        "en": "Please select lawyer to proceed.",
        "jp": "",
        "cn": ""
    }

    //Case confirm page
    CONFIRM_CASE = {
        "th": "กรณายืนยันข้อมูลของท่าน",
        "en": "Please confirm your information",
        "jp": "",
        "cn": ""
    }
    CONFIRM = {
        "th": "ยืนยัน",
        "en": "Confirm",
        "jp": "",
        "cn": ""
    }
    SEND_VERIFY_CODE = {
        "th": "ส่งรหัสการยืนยันตัวตน",
        "en": "Send verificaion code",
        "jp": "",
        "cn": ""
    }
    NO_SMS = {
        "th": "ไม่ได้รับ sms? ตรวจสอบเบอ์ติดต่อที่นี่",
        "en": "No sms? confirm your number here.",
        "jp": "",
        "cn": ""
    }
    SEND_AGAIN = {
        "th": "ส่งรหัสยืนยันอีกครั้ง",
        "en": "send code again.",
        "jp": "",
        "cn": ""
    }
    VERIFICATION_CODE = {
        "th": "รหัสยืนยัน",
        "en": "Verification code",
        "jp": "",
        "cn": ""
    }
    PROCESS_INFO_H = {
        "th": "ข้อมูลเพิ่มเติมในการดำเนินการ",
        "en": "More information in process",
        "jp": "",
        "cn": ""
    }
    MIN_PROCESS_FEE = {
        "th": "ค่าใช้จ่ายขั้นต่ำ",
        "en": "Minimum lawyer fee",
        "jp": "",
        "cn": ""
    }
    MIN_PROCESS_EXP_H = {
        "th": "ปัจจัยของราคา",
        "en": "Lawyer fee factor",
        "jp": "",
        "cn": ""
    }
    FEE_REMARK = {
        "th": "ค่าใช้จ่ายที่กล่าวถึงคือค่าใช้จ่ายเริ่มต้นโดยประมาณเพื่อเป็นข้อมูลเบื้องต้นในการตัดสินใจ และเป็นค่าดำเนินการที่ทางลูกความให้กับทนายโดยตรงเท่านั้น โดยที่ dfastlegal.com ไม่ได้มีการเก็บค่าใช้จ่ายใดๆจากท่าน",
        "en": "this fee is estimated to support in decision making, and is only for lawyer who you proceed with. dfastleal.com does not charge any fee from you.",
        "jp": "",
        "cn": ""
    }

    //Case issued page
    WAIT_FOR_CONTACT_H = {
        "th": "กรุณารอการติดต่อกลับ",
        "en": "Please wait for you resonse",
        "jp": "",
        "cn": ""
    }
    WAIT_FOR_CONTACT_INFO = {
        "th": "ท่านจะได้รับการติดต่อจากทนายที่ท่านเลือกภายใน 1 วัน เราจะทำการติดต่อทนายท่านอื่นให้โดยอัตโนมัติ หากไม่มีทนายท่านใดตอบรับปัญหาของท่านในเวลาที่กำหนด",
        "en": "You will be contacted within 1 day by you seleted lawyers, If none of selected lawyer response to your case we will find more lawyers for you automatically.",
        "jp": "",
        "cn": ""
    }
    TO_FIRST_PAGE = {
        "th": "กลับสู่หน้าแรก",
        "en": "Back to first page",
        "jp": "",
        "cn": ""
    }
    TO_YOUR_CASES = {
        "th": "ไปยังหน้าเคสของคุณ",
        "en": "Go to your cases page",
        "jp": "",
        "cn": ""
    }

    //Lawyer-case page
    NO_REQUESTED_H = {
        "th": "คุณยังไม่มีคำร้องจากลูกความค่ะ",
        "en": "You do not have client requested, just yet.",
        "jp": "",
        "cn": ""
    }
    NO_REQUESTED = {
        "th": "กรุณายืนยันอีกครั้งว่า คุณได้ส่งเอกสารทุกอย่างเพื่อนการรับรอง เมื่อรับรองแล้วคุณจึงสามารถใช้บริการของระบบเราได้ทั้งหมด รวมถึงการเขียนประสบการณ์และคำอธิบายอย่างสั้นของคุณให้ชัดเจนและน่าเชื่อถือ ซึ่งจะช่วยเพิ่มความมั่นใจของลูกความในการติดต่อคุณเป็นอย่างมาก",
        "en": "Please be patience, and make sure you have all document submitted for appoval. Upon approval you can fully use our service. Also please optimize your short description and experience to be clear and precise which will improve client's confident in contacting you.",
        "jp": "",
        "cn": ""
    }
    THANK_YOU = {
        "th": "ขอบคุณค่ะ :)",
        "en": "thank you. :)",
        "jp": "",
        "cn": ""
    }
    ACCEPTED_CASE = {
        "th": "เคสที่ตอบรับ",
        "en": "Accepted casws",
        "jp": "",
        "cn": ""
    }
    PENDING_CASE = {
        "th": "เคสที่รอการตอบกลับ",
        "en": "Requested cases",
        "jp": "",
        "cn": ""
    }
    DECLINED_CASE = {
        "th": "เคสที่ปฏิเสธ",
        "en": "Declined cases",
        "jp": "",
        "cn": ""
    }

    //Case response page
    CASE_RESPONSE = {
        "th": "การตอบรับเคสของคุณ",
        "en": "Your case response",
        "jp": "",
        "cn": ""
    }
    CLIENT_PROFILE = {
        "th": "โปรไฟล์ลูกความ",
        "en": "Client profile",
        "jp": "",
        "cn": ""
    }
    ACCEPT_CASE = {
        "th": "รับดำเนินการ",
        "en": "Accept",
        "jp": "",
        "cn": ""
    }
    DECLINE_CASE = {
        "th": "ปฏิเสธ",
        "en": "Decline",
        "jp": "",
        "cn": ""
    }
    CASE_DECLINED_H = {
        "th": "คุณได้ทำการปฏิเสธเคสนี้",
        "en": "You have declined this case.",
        "jp": "",
        "cn": ""
    }
    CASE_DECLINED_CNT = {
        "th": "ขอบคุณสำหรับการตอบกลับของคุณค่ะ",
        "en": "thank you for your response.",
        "jp": "",
        "cn": ""
    }
    MORE_CASE_INFO = {
        "th": "รายละเอียดเพิ่มเติม",
        "en": "More informtaion",
        "jp": "",
        "cn": ""
    }
    CLIENT_PURPOSE = {
        "th": "จุดประสงค์ของลูกความ",
        "en": "Client purpose",
        "jp": "",
        "cn": ""
    }
    CLIENT_PERSON = {
        "th": "ดำเนินการในฐานะ",
        "en": "Client person type",
        "jp": "",
        "cn": ""
    }
    CASE_TYPE = {
        "th": "ประเภทของคดี",
        "en": "Case type",
        "jp": "",
        "cn": ""
    }
    //case select payment
    SELECTED_CARD = {
        "th": "บัตรเครดิตที่ใช้",
        "en": "Selected card",
        "jp": "",
        "cn": ""
    }
    BILLING_ADDRESS = {
        "th": "ที่อยู่ใบเสร็จ",
        "en": "Billing address",
        "jp": "",
        "cn": ""
    }
    PROCEED_TO_CHECKOUT = {
        "th": "ไปยังหน้ายืนยันชำระเงิน",
        "en": "Proceed to checkout",
        "jp": "",
        "cn": ""
    }
    CARD_HOLDER_NAME = {
        "th": "ชื่อเจ้าของบัตร",
        "en": "Card holder name",
        "jp": "",
        "cn": ""
    }
    CARD_NUMBER = {
        "th": "หมายเลขบัตรเครดิต",
        "en": "Card no.",
        "jp": "",
        "cn": ""
    }
    EXPIRATION_DATE = {
        "th": "วันหมดอายุ",
        "en": "Expiration date",
        "jp": "",
        "cn": ""
    }
    SCV = {
        "th": "รหัสความปลอดภัย",
        "en": "SCV",
        "jp": "",
        "cn": ""
    }
    ORDER_CONFIRMATION = {
        "th": "ยืนยันการสั่งซื้อ",
        "en": "Order confirmation",
        "jp": "",
        "cn": ""
    }

    //confirm payment page
    PAY = {
        "th": "ชำระเงิน",
        "en": "Check out",
        "jp": "",
        "cn": ""
    }
    AGREED_TO = {
        "th": "ฉันได้อ่านและยอมรับ",
        "en": "I've read and agree to",
        "jp": "",
        "cn": ""
    }
    TERM_N_CONDS = {
        "th": "เงื่อนไขและข้อตกลงในการให้บริการ",
        "en": "terms & conditions",
        "jp": "",
        "cn": ""
    }
    AGREE_REQUIRE = {
        "th": "กรุณายอมรับเงื่อนไขและข้อตกลงในการบริการเพื่อดำเนินการต่อ",
        "en": "Please accetp term & conditions to proceed",
        "jp": "",
        "cn": ""
    }


    //payment success page
    PAY_SUCCESS_H = {
        "th": "การชำระเงินเรียบร้อย",
        "en": "Purchase success",
        "jp": "",
        "cn": ""
    }
    PAY_THANKYOU = {
        "th": "ขอบคุณที่ใช้บริการค่ะ",
        "en": "thank you for your support",
        "jp": "",
        "cn": ""
    }
    PAY_SUCCESS_INFO = {
        "th": "คุณสามารถติดต่อและดูข้อมูลเกี่ยวกับลูกความของท่านจากปุ่มด้านล่างได้ทันทีค่ะ",
        "en": "You can contact client and see profile immediately at the button below",
        "jp": "",
        "cn": ""
    }
    SEE_CONTACT = {
        "th": "ติดต่อลูกความทันที",
        "en": "Contact client now",
        "jp": "",
        "cn": ""
    }

    PRIVACY_POLICY_CNT = {
        "th": `[TBD]`,

        "en": `[TBD]`


    }
    TERM_OF_USE_CNT = {
        "th": "[TBD]",
        "en": "[TBD}",
        "jp": "",
        "cn": ""
    }
    PAYMenT_POLICY_CNT = {
        "th": "",
        "en": "",
        "jp": "",
        "cn": ""
    }
    RETURN_POLICY = {
        "th": "นโยบายการคืนเงิน",
        "en": "Return Policy",
        "jp": "",
        "cn": ""
    }
    RETURN_POLICY_CNT = {
        "th": "เราจะทำการคืนเงินเต็มจำนวน เมื่อได้รับการแจ้งจากทนาย ว่าไม่สามารถติดต่อลูกความ ด้วยช่องทางการติดต่อที่แจ้งไว้ได้ และหลังจากที่ทางเรายืนยันแล้วว่าช่องทางดังกล่าวไม่สามารถติดต่อได้จริง",
        "en": "We will provide lawyers with full refund when we are notifid about non-contactable contacts and we confirm that these contacts are non-contactable.",
        "jp": "",
        "cn": ""
    }
    PURCHASE_OF_ORDER = {
        "th": "การสั่งซื้อสินค้า/บริการ",
        "en": "Purchase of order",
        "jp": "",
        "cn": ""
    }


    //forgot password
    FORGOT_PW = {
        "th": "ลืมรหัสผ่าน",
        "en": "Forgot password",
        "jp": "",
        "cn": ""
    }
    FORGOT_PW_CONTENT = {
        "th": "กรุณาระบุอีเมล์ที่ท่านใช้ในการล๊อคอิน ระบบจะส่งลิงค์ในการเปลี่ยนพาสเวิร์ดไปที่อีเมล์นี้ค่ะ",
        "en": "Please provide us an email address you use to login. A link to reset your password will be sent to this email.",
        "jp": "",
        "cn": ""
    }
    FORGOT_PW_SUBMIT = {
        "th": "ส่งอีเมล์เพื่อเปลี่ยนพาสเวิร์ด",
        "en": "Send reset passwored request email.",
        "jp": "",
        "cn": ""
    }
    FORGOT_PW_SENT = {
        "th": "ระบบได้ทำการส่งอีเมลล์เพื่อทำการเปลี่ยนพาสFเวิร์ดเรียบร้อยค่ะ",
        "en": "a link to reset password has been sent.",
        "jp": "",
        "cn": ""
    }

    //Client case page
    NO_CLIENT_CASE = {
        "th": "คุณยังไม่มีเคสใดๆที่คุณสร้างขึ้นมาค่ะ คุณสามารถสร้างเคสของคุณได้ที่นี่ค่ะ",
        "en": "You don't have any case yet, you can create your case here.",
        "jp": "",
        "cn": ""
    }
    CREATE_CLIENT_CASE = {
        "th": "สร้างเคสของคุณที่นี่",
        "en": "Create you case here",
        "jp": "",
        "cn": ""
    }

    //client sign up modal
    PHONE_REQ_EXP = {
        "th": "เรามีความจำเป็นต้องใช้ในการยืนยันตัวตนของท่านเพื่อรับการช่วยเหลือจากทนายอย่างรวดเร็วเท่านั้น",
        "en": "Contact number is required only for verification and immediate support from lawyers.",
        "jp": "",
        "cn": ""
    }

    //Our services page
    OUR_SERVICES = {
        "th": "บริการของเรา",
        "en": "Our services",
        "jp": "",
        "cn": ""
    }
    OUR_SERVICES_H = {
        "th": "DFastLegal มีการให้บริการดังนี้",
        "en": "DFastLegal provide services as follow",
        "jp": "",
        "cn": ""
    }
    MATCHING_SERVICE = {
        "th": "แมทชิ่ง: บริการสำหรับลูกความที่มีปัญหาทางกฏหมายและทนายที่มีความสามารภตรงกับปัญหา ได้ติดต่อกันอย่างรวดเร็ว เพื่อเพิ่มประสิทธิภาพในการดำเนินการของทั้งสองฝ่าย",
        "en": "Matching: Service provided is to provide fast connection  for normal people who face any legal problems and for lawyers who have expertise matched with the types of legal issues of clients in order to increase proficiency of any undertakings of both clients and lawyers.",
        "jp": "",
        "cn": ""
    }
    LISTING_SERVICE = {
        "th": "ลิสติ้ง: บริการสำหรับทนายที่ต้องการแสดงประวัติในระบบ เพื่อให้ลูกความสามารถติดต่อเข้าถึงได้อย่างง่ายและรวดเร็วในกรณีที่ต้องการมองหาทนายด้วยตัวเอง",
        "en": "Listing: Service for lawyers who wish to show their professional profiles in this system in order to provide quick and easy access to clients in the case that clients wish to seek for a lawyer by themselves.",
        "jp": "",
        "cn": ""
    }

}
