﻿import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import { Router, ActivatedRoute } from '@angular/router';

import { Client, Lawyer } from '../utilities/classes-definition';
import { EXPERTISES, PROVINCES } from '../utilities/constant';
import { ParseObjService } from './parse-object.service';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

@Injectable()
export class ClientSideService {

    lawyerProfiles: any[] = [];
    lawyerProfiles$: Observable<any>;
    lawyerProfilesObserver: Observer<any>;

    lawyersForCase: any[] = [];
    lawyersForCase$: Observable<any>;
    lawyersForCaseObserver: Observer<any>;

    selectedLawyerProfilesId: Set<any> = new Set();

    currentClientCase: any;
    currentClientCase$: Observable<any>;
    currentClientCaseObserver: Observer<any>;

    allClientCase: any[] = [];
    allClientCase$: Observable<any>;
    allClientCaseObserver: Observer<any>;

    provinceInputArray: string[];
    expertiseInput: string;

    viewingLawyer: any;
    latestCaseType: any = "";

    QUESTIONS: any[] = [];
    ANSWERS: any[] = [];
    clientProgress: any[] = [];

    lang: string;
    params$: any;

    constructor(
        private parseObjService: ParseObjService,
        private router: Router,
        private route: ActivatedRoute
    ) {

        this.lawyerProfiles$ = new Observable((observer: any) => { //this param will be observe from other comp
            this.lawyerProfilesObserver = observer;

        }).share(); //IMPORTANT

        this.lawyersForCase$ = new Observable((observer: any) => {
            this.lawyersForCaseObserver = observer;
        }).share();

        this.currentClientCase$ = new Observable((observer: any) => {
            this.currentClientCaseObserver = observer;
        }).share();

        this.allClientCase$ = new Observable((observer: any) => {
            this.allClientCaseObserver = observer;
        }).share();


    }



    //Lawyers Listing page
    getCurrentLawyerProfiles() {
        return this.lawyerProfiles;
    }
    getExpertiseObjects() {
        return EXPERTISES;
    }
    getProvinces() {
        return PROVINCES;
    }
    getExpertiseInput() {
        return this.expertiseInput;
    }
    getProvinceInput() {
        return this.provinceInputArray;
    }
    getFirstLawyerProfiles() {
        let LawyerProfiles = Parse.Object.extend("LawyerProfiles");
        let query = new Parse.Query(LawyerProfiles);
        query.include("createdBy");
        query.include("qualifications");
        query.equalTo('isVerified', true);
        query.equalTo('useListingService', true);

        query.find({
            success: (lawyerProfiles: any) => {
                console.log('getting done');
                var shuffledProfiles = this.shuffle(lawyerProfiles);

                this.lawyerProfiles = shuffledProfiles;

                this.lawyerProfilesObserver.next(this.lawyerProfiles);

                console.log(this.lawyerProfiles.length);
                for (let i = 0; i <= this.lawyerProfiles.length; i++) {
                    console.log(this.lawyerProfiles[i].get('createdBy'));

                }
            }, error: (error: any) => {
                console.log(error.message);
            }
        });
    }
    filterLawyers(expertiseInput: string, provinceInputArray: string[]) {

        this.expertiseInput = expertiseInput; //just to make app remember value
        this.provinceInputArray = provinceInputArray; //just to make app remember value

        console.log(`finding... ${expertiseInput} in ${provinceInputArray}`);

        let compoundQuery = this.getCompoundQueryFromInput(expertiseInput, provinceInputArray);

        compoundQuery.equalTo('useListingService', true);

        compoundQuery.find({
            success: (lawyerProfiles: any) => {
                let shuffledProfiles = this.shuffle(lawyerProfiles);
                this.lawyerProfiles = shuffledProfiles;

                this.lawyerProfilesObserver.next(this.lawyerProfiles);

                console.log(this.lawyerProfiles.length);

            }, error: (error: any) => {
                console.log(error.message);
            }
        });

    }
    getViewingLawyer(profileId: string) {
        return this.lawyerProfiles.find(lawyerProfile => lawyerProfile.id === profileId);
    }
    //!Lawyers Listing page

    //QuestionsPage
    initQuestionsAnswersData() {
        let Questions = Parse.Object.extend('Questions');
        let questionsQuery = new Parse.Query(Questions);
        questionsQuery.include('answers');
        let Answers = Parse.Object.extend('Answers');
        let answersQuery = new Parse.Query(Answers);
        answersQuery.include(['nextQuestion.answers']);
        answersQuery.include('caseType');

        return questionsQuery.find().then((questions: any[]) => {
            this.QUESTIONS = questions;
            console.log(this.QUESTIONS);
            return answersQuery.find();
        }).then((answers: any[]) => {
            this.ANSWERS = answers;
            console.log(this.ANSWERS);
        }, (error: any) => {
            console.log(error.message);
        });
    }

    getPreLoadedQuestions() {
        return this.QUESTIONS;
    }
    getPreLoadedAnswers() {
        return this.ANSWERS;
    }
    getLatestQuestion(currentUser: any): Promise<any> {

        if (currentUser == null) {
            console.log('no current user');
            return this.QUESTIONS.find(question => question.id == '5bM4U2IN0s'); //id of a first question

        } else {
            console.log(currentUser.id);
            //query for progress
            let query = new Parse.Query('ClientProgress');
            query.equalTo('createdBy', currentUser);
            query.equalTo('isDone', false);
            query.include(['inQuestion.answers']);
            query.include('hasAnswered');
            query.addAscending('createdAt'); //progress will be sort by lastest at position .length - 1

            return query.find().then((progresses: any[]) => {
                this.clientProgress = progresses;
                if (this.clientProgress.length == 0) {
                    //return first answer
                    return this.QUESTIONS.find(question => question.id == '5bM4U2IN0s');
                } else {
                    let latestProgress = this.clientProgress[this.clientProgress.length - 1];
                    let latestAnswer = latestProgress.get('hasAnswered');

                    if (latestAnswer.get('isLastAnswer')) {
                        //redirect to case detail
                        console.log('This is a last question');
                        // this.toCaseDetailPage();
                        this.toSelectLawyersPage();
                    } else {
                        let nextQuestion = latestAnswer.get('nextQuestion');
                        return this.QUESTIONS.find(question => question.id == nextQuestion.id);
                    }

                }

            }, (error: any) => {

            });
        }
        //return questions of latets progress
        //if no progress
        //return first question
    }

    getLastQuestion(currentCase: any) {

        this.clientProgress = currentCase.get('questionsProgress');

        if (this.clientProgress.length == 0) {

            return this.QUESTIONS.find(question => question.id == '5bM4U2IN0s'); // no progress, return first question

        } else {
            let lastProgress = this.clientProgress[this.clientProgress.length - 1];
            let lastAnswer = lastProgress.get('hasAnswered');
            console.log(lastAnswer);

            if (lastAnswer.get('isLastAnswer')) {


                // this.toCaseDetailPage();
                console.log('this is a last question');
                this.toSelectLawyersPage();


            } else {

                let lastQuestion = lastAnswer.get('nextQuestion');
                console.log(lastQuestion);
                // let lastQuestion = lastProgress.get('inQuestion');
                return this.QUESTIONS.find(question => question.id == lastQuestion.id);

            }

        }


    }

    getFirstQuestion() {
        return this.QUESTIONS.find(question => question.id == '5bM4U2IN0s');
    }

    submitAnswer(currentUser: any, currentQuestion: any, selectedAnswer: any, currentClientCase: any): Promise<any> {

        let ClientProgress = Parse.Object.extend('ClientProgress');
        let progress = new ClientProgress();
        progress.set('inCase', currentClientCase);
        progress.set('createdBy', currentUser);
        progress.set('inQuestion', currentQuestion);
        progress.set('hasAnswered', selectedAnswer);
        progress.set('questionText', this.parseObjService.getQuestionText(currentQuestion, 'TH'));//only for easier look up in table, not affect user xp
        progress.set('answerText', this.parseObjService.getAnswerText(selectedAnswer, 'TH'));//only for easier look up in table, not affect user xp
        progress.set('isDone', false);

        this.clientProgress = currentClientCase.get('questionsProgress');
        this.clientProgress.push(progress);


        currentClientCase.set('questionsProgress', this.clientProgress);
        return currentClientCase.save().then((clientCase) => {

            console.log(`case updated with progress`);

            if (selectedAnswer.get('isLastAnswer')) {

                // this.toCaseDetailPage();
                this.submitCaseType().then(() => {

                    this.toSelectLawyersPage();


                });
            } else {

                let nextQuestion = selectedAnswer.get('nextQuestion');
                return this.QUESTIONS.find(question => question.id == nextQuestion.id);

            }

        }, (error: any) => {

            console.log(error.message);

        });


    }
    getClientProgress() {
        return this.clientProgress;
    }
    toPreviousQuestion() {

        this.deleteLatestProgress().then((clientCase) => {
            console.log(clientCase);
            this.currentClientCase = clientCase;
            this.currentClientCaseObserver.next(this.currentClientCase);
        }, (error: any) => {

            console.log(error.message);

        });

    }

    deleteLatestProgress() {

        let latestProgress = this.clientProgress[this.clientProgress.length - 1];
        return latestProgress.destroy().then((deletedProgress) => {

            console.log('progress deleted');
            this.clientProgress.pop();
            this.currentClientCase.set('questionsProgress', this.clientProgress);
            return this.currentClientCase.save();

        });

    }




    queryClientCase(currentUser: any) {
        console.log('querying case.');
        let ClientCases = Parse.Object.extend('ClientCases');
        let query = new Parse.Query(ClientCases);
        query.equalTo('isIssued', false);
        query.equalTo('createdBy', currentUser);
        query.include('caseType');
        query.include(['questionsProgress.hasAnswered.nextQuestion.answers']);
        query.include(['questionsProgress.inQuestion.answers']);
        query.include(['selectedLawyers.createdBy']);
        return query.first().then((clientCase: any) => {

            if (clientCase) {

                console.log(clientCase.id);
                this.currentClientCase = clientCase;
                this.clientProgress = this.currentClientCase.get('questionsProgress');
                this.currentClientCaseObserver.next(this.currentClientCase);

            } else {

                this.createNewCase(currentUser);
            }
        });
    }

    createNewCase(currentUser) {

        console.log('creating new case');
        let ClientCases = Parse.Object.extend('ClientCases');
        let clientCase = new ClientCases();

        clientCase.set('createdBy', currentUser);
        clientCase.set('questionsProgress', []);
        clientCase.set('notifySent', false);
        clientCase.set('selectedLawyers', []);
        clientCase.set('isIssued', false);
        clientCase.set('preferredLawyer', 3);
        clientCase.set('lawyerResponded', 0);
        clientCase.set('lawyerAccepted', 0);
        clientCase.set('provinces', ["BANGKOK"]);
        clientCase.set('caseDetail', "");
        clientCase.set('isDone', false);

        clientCase.save().then((clientCase) => {

            this.currentClientCase = clientCase;
            this.currentClientCaseObserver.next(this.currentClientCase);

        });

    }
    //!QuestionsPage

    //Case detail form page
    getExistingClientCase(currentUser: any): Promise<any> {
        //get a case that already created by user.
        let ClientCases = Parse.Object.extend('ClientCases');
        let query = new Parse.Query(ClientCases);
        query.equalTo('isIssued', false);
        query.equalTo('createdBy', currentUser);
        query.include('caseType');
        query.include('questionsProgress');
        query.include(['selectedLawyers.createdBy']);
        return query.first().then((clientCase: any) => {

            if (clientCase) {

                this.currentClientCase = clientCase;

                this.currentClientCaseObserver.next(this.currentClientCase);

                let selectedLawyerProfiles = this.currentClientCase.get('selectedLawyers');
                for (let lawyerProfile of selectedLawyerProfiles) {
                    this.selectedLawyerProfilesId.add(lawyerProfile.id);
                }

                return this.currentClientCase;//return value after then in promise duay na ja

            } else {
                console.log(`user does not have case yet`);
            }
        }, (error: any) => {
            console.log(error.message);
        });
    }

    submitCaseDetail(clientCase: any, currentUser: any, caseDetail: string, selectedProvinces: string[], prefLawyerNumber: number): Promise<any> {
        // console.log(this.clientProgress);

        // let latestProgress = this.clientProgress[this.clientProgress.length - 1];
        // let latestAnswer = latestProgress.get('hasAnswered');
        // let caseType = latestAnswer.get('caseType');
        // console.log(caseType);
        // clientCase.set('caseType', caseType);

        clientCase.set('caseDetail', caseDetail);
        clientCase.set('provinces', selectedProvinces);
        // clientCase.set('caseType', caseType);
        clientCase.set('preferredLawyer', prefLawyerNumber);

        return clientCase.save().then((clientCase: any) => {

            console.log(clientCase.get('caseType'));
            this.currentClientCase = clientCase;
            // this.latestCaseType = clientCase.get('caseType');
            this.currentClientCaseObserver.next(this.currentClientCase);

            for (let progress of this.clientProgress) {

                progress.set('inCase', this.currentClientCase);
            }

            return Parse.Object.saveAll(this.clientProgress);


        }).then(() => {
            console.log('update progresses success');

        });

    }

    submitCaseType() {
        let latestProgress = this.clientProgress[this.clientProgress.length - 1];
        let latestAnswer = latestProgress.get('hasAnswered');
        let caseType = latestAnswer.get('caseType');
        console.log(caseType);
        this.currentClientCase.set('caseType', caseType);

        return this.currentClientCase.save();

    }

    getCurrentCase() {
        return this.currentClientCase;
    }
    //!Case detail form page

    findLawyersForCase(currentCase: any): Promise<any> {
        console.log(currentCase.get('caseType'))
        console.log(this.latestCaseType);


        if (this.latestCaseType != currentCase.get('caseType')) { //if case type is change, find new lawyers
            //Disable match with provices for first phase.
            //let caseProvince = currentCase.get('provinces');
            let caseProvince = ['ALL'];
            let caseTypeObj = currentCase.get('caseType');
            let caseType = caseTypeObj.get('type');

            console.log(`${caseProvince}, ${caseType}`);
            this.latestCaseType = caseTypeObj; //is store to check when returning to comp


            let compoundQuery = this.getCompoundQueryFromInput(caseType, caseProvince);

            compoundQuery.equalTo('useMatchingService', true);

            return compoundQuery.find({
                success: (lawyerProfiles: any) => {
                    let shuffledProfiles = this.shuffle(lawyerProfiles);
                    this.lawyersForCase = shuffledProfiles;

                    this.lawyersForCaseObserver.next(this.lawyersForCase);

                    console.log(this.lawyersForCase.length);

                }, error: (error: any) => {
                    console.log(error.message);
                }
            });

        } else {//case type not change, do nothing

            this.lawyersForCaseObserver.next(this.lawyersForCase);
            console.log('no case type change');
            return
        }



    }

    toggleSelectedLawyerProfile(profile: any) {
        if (this.selectedLawyerProfilesId.has(profile.id)) {
            //if already has remove it.
            this.selectedLawyerProfilesId.delete(profile.id);
        } else {
            //still not have so add it
            this.selectedLawyerProfilesId.add(profile.id);
        }

        return this.selectedLawyerProfilesId;

    }
    getLawyersForCase() {
        return this.lawyersForCase;
    }
    getSelectedLawyersId() {
        return this.selectedLawyerProfilesId;
    }
    toSummaryPage(): Promise<any> {
        let selectedLawyersProfile: any[] = [];
        this.selectedLawyerProfilesId.forEach((value) => {

            //console.log(value);
            //var obj = {
            //    "__type": "Pointer",
            //    "className": "LawyerProfiles",
            //    "objectId": value
            //}

            //pointerArray.push(obj);
            let lawyerProfile = this.lawyersForCase.find(profile => profile.id == value);
            selectedLawyersProfile.push(lawyerProfile)

        });

        this.currentClientCase.set('selectedLawyers', selectedLawyersProfile);
        //save selected laywer to case
        return this.currentClientCase.save().then((clientCase: any) => {//save
            console.log('case save success:' + clientCase.id);

            this.currentClientCase = clientCase;
            return this.currentClientCase;
            //redirect to summary page.

        });
    }
    //!Select Lawwyers page

    //View lawyer profile page
    getViewingLawyerInCase(profileId: string) {
        console.log(profileId);
        return this.lawyersForCase.find(profile => profile.id == profileId);
    }
    selectLawyerProfile(profileId: string) {
        this.selectedLawyerProfilesId.add(profileId);
        window.history.back();

    }
    //!View lawyer profile page

    //Confirm case page
    issueCase(): Promise<any> {

        let questionProgress = this.parseObjService.getCaseProgress(this.currentClientCase);
        for (let progress of questionProgress) {
            progress.set('isDone', true);
        }

        let objToSave = questionProgress;

        this.currentClientCase.set('isIssued', true);

        objToSave.push(this.currentClientCase);

        return Parse.Object.saveAll(objToSave);

    }
    //!Confirm case page

    //Client-cases
    getAllClientCase() {
        return this.allClientCase;
    }

    loadAllClientCase(currentUser: any) {
        let query = new Parse.Query('ClientCases');
        query.equalTo('createdBy', currentUser);
        query.equalTo('isIssued', true);
        query.include(['selectedLawyers.createdBy']);
        query.include('caseType');
        query.descending('createdAt');

        query.find().then((clientCases: any[]) => {
            this.allClientCase = clientCases;
            this.allClientCaseObserver.next(this.allClientCase);
        });
    }
    //!Client-cases

    //utilites
    shuffle(array: any) {
        var currentIndex = array.length, temporaryValue: any, randomIndex: any;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
    toCaseDetailPage() {
        // let link = ['/case-detail-form'];
        // this.router.navigate(link);
        let url: string = this.router.url;
        let lang = url.slice(1, 3); //a work around to get url param, as subscribe to params is not working in service.
        this.router.navigateByUrl(`/${lang}/case-detail-form`);
    }
    toSelectLawyersPage() {
        let url: string = this.router.url;
        let lang = url.slice(1, 3); //a work around to get url param, as subscribe to params is not working in service.
        this.router.navigateByUrl(`/${lang}/select-lawyers/${this.currentClientCase.id}`);

    }
    getCompoundQueryFromInput(expertiseInput: string, provinceInputArray: string[]) {

        let LawyerProfiles = Parse.Object.extend("LawyerProfiles");
        let queryExpertise1 = new Parse.Query(LawyerProfiles);
        let queryExpertise2 = new Parse.Query(LawyerProfiles);
        let queryExpertise3 = new Parse.Query(LawyerProfiles);

        //queryExpertise1.include("qualifications");
        //queryExpertise2.include("qualifications");
        //queryExpertise3.include("qualifications");

        if (expertiseInput == "ALL") {
            //do nothings
        } else {
            queryExpertise1.equalTo("expertise1", expertiseInput);
            queryExpertise2.equalTo("expertise2", expertiseInput);
            queryExpertise3.equalTo("expertise3", expertiseInput);
        }

        let compoundQuery = new Parse.Query.or(queryExpertise1, queryExpertise2, queryExpertise3);

        if (provinceInputArray[0] == 'ALL') {//do not add filter in provinces // assuming there is only one value in an array
            //do nothings
        } else {//add filter to query in province
            compoundQuery.containedIn("operatingProvinces", provinceInputArray);
        }

        compoundQuery.include('createdBy');
        compoundQuery.include('qualifications');

        compoundQuery.equalTo('isVerified', true);

        return compoundQuery;

    }

    clearCurrentData() {
        this.currentClientCase = null;
        this.lawyersForCase = [];
        this.clientProgress = [];
        this.currentClientCase = undefined;
        this.latestCaseType = undefined;
        this.selectedLawyerProfilesId = new Set();

        console.log('clear client data');
        console.log(this.currentClientCase);
        this.currentClientCaseObserver.next(this.currentClientCase);
    }

    clearSelectedLawyerIds() {
        console.log('clear lawyer selection');
        this.selectedLawyerProfilesId = new Set();

    }

    genVerificationCode(clientCase: any): Promise<any> {

        var a = Math.floor((Math.random() * 10));
        var b = Math.floor((Math.random() * 10));
        var c = Math.floor((Math.random() * 10));
        var d = Math.floor((Math.random() * 10));

        var code = "" + a + b + c + d;

        clientCase.set('verificationCode', code.toString());

        return clientCase.save().then((clientCase) => {
            this.currentClientCaseObserver.next(clientCase);
            return clientCase;
        });
    }
}