﻿import {Injectable, NgZone}from'@angular/core';
import {Observable}from'rxjs/Observable'
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import {Router}from'@angular/router';

import {Client, Lawyer, Address}from'../utilities/classes-definition';
import {EXPERTISES, PROVINCES} from '../utilities/constant';
import {ParseObjService}from'./parse-object.service';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

@Injectable()
export class LawyerSideService {


    lawyerResponse: any;
    lawyerResponse$: Observable<any>;
    lawyerResponseObserver: Observer<any>

    billingAddresses: any[] = [];
    billingAddresses$: Observable<any>;
    billingAddresssesObserver: Observer<any>

    allLawyerResponse: any[] = [];
    allLawyerResponse$: Observable<any>;
    allLawyerResponseObserver: Observer<any>;

    constructor(
        private parseObjService: ParseObjService,
        private router: Router
    ) {


        this.lawyerResponse$ = new Observable((observer: any) => {
            this.lawyerResponseObserver = observer;
        }).share();

        this.billingAddresses$ = new Observable((observer: any) => {
            this.billingAddresssesObserver = observer;
        }).share();

        this.allLawyerResponse$ = new Observable((observer: any) => {
            this.allLawyerResponseObserver = observer;
        }).share();

    }

    getLawyerResponse() {
        return this.lawyerResponse;
    }

    loadLawyerResponse(responseId: string) {

        let query = new Parse.Query('LawyerResponse');
        query.include(['case.questionsProgress.hasAnswered.caseType']);
        query.include(['case.questionsProgress.inQuestion']);
        query.include('client');
        query.include(['requestedLawyer.createdBy']);

        query.get(responseId).then((lawyerResponse: any) => {
            this.lawyerResponse = lawyerResponse;

            this.lawyerResponseObserver.next(this.lawyerResponse);

        });
    }

    getLawyerResponseById(responseId: string): Promise<any> {

        console.log(responseId);

        let query = new Parse.Query('LawyerResponse');
        query.include(['case.questionsProgress.hasAnswered.caseType']);
        query.include(['case.questionsProgress.inQuestion']);
        query.include('client');
        query.include(['requestedLawyer.createdBy']);

        return query.get(responseId);
    }

    declineCase(lawyerResponse: any): Promise<any> {

        lawyerResponse.set('isResponse', true);
        lawyerResponse.set('isAccept', false);

        return lawyerResponse.save().then((lawyerReponse) => {
            //replace in all response and update to observer
            this.lawyerResponse = lawyerResponse;

            this.allLawyerResponseObserver.next(this.allLawyerResponse);
        });

    }

    acceptCase(lawyerResponse: any): Promise<any> {

        lawyerResponse.set('isResponse', true);
        lawyerResponse.set('isAccept', true);

        return lawyerResponse.save().then((lawyerReponse) => {

            this.lawyerResponse = lawyerReponse;
            //this.lawyerResponseObserver.next(this.lawyerResponse);
            if (this.allLawyerResponse.length > 0) {
                //update list if any
                this.allLawyerResponseObserver.next(this.allLawyerResponse);

            } else {
                //other wise do nothing
            }
        });
    }


    //for case-accept-payment page
    getBillingAddresses() {
        return this.billingAddresses;
    }

    loadBillingAddresses(currentUser: any) {
        console.log(currentUser);
        let query = new Parse.Query('BillingAddresses');

        query.equalTo('createdBy', currentUser);
        console.log(`start finding...`);

        query.find().then((billingsAddresses) => {

            this.billingAddresses = billingsAddresses;
            this.billingAddresssesObserver.next(this.billingAddresses);

        });
    }

    addAddress(inputAddress: Address, currentUser: any) {

        let BillingAddresses = Parse.Object.extend('BillingAddresses');

        let billingAddress = new BillingAddresses();
        billingAddress.set('addressObj', inputAddress);
        billingAddress.set('createdBy', currentUser);

        billingAddress.save().then((billingAddress) => {
            this.billingAddresses.push(billingAddress);
            this.billingAddresssesObserver.next(this.billingAddresses);
        });
    }

    deleteAddress(address: any, index: number) {

        address.destroy().then(() => {
            this.billingAddresses.splice(index, 1);
            this.billingAddresssesObserver.next(this.billingAddresses);
        });

    }

    //!for case-accept-payment page

    //for lawyer-case page
    getAllLawyerResponse() {
        return this.allLawyerResponse;
    }

    loadAllLawyerResponse(currentUser) {

        let query = new Parse.Query('LawyerResponse');

        query.equalTo('requestedLawyer', currentUser);

        query.include(['case.questionsProgress.hasAnswered.caseType']);
        query.include(['case.questionsProgress.inQuestion']);
        query.include('client');
        query.include(['requestedLawyer.createdBy']);

        query.descending('createdAt');

        query.find().then((allLawyerResponse) => {

            this.allLawyerResponse = allLawyerResponse;
            this.allLawyerResponseObserver.next(this.allLawyerResponse);


        }, (error) => {
            console.log(`error load all response: ${error.message}`);
        })
    }
    //!for lawyer-case page

}