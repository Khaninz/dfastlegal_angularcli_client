﻿import {Injectable, NgZone}from'@angular/core';
import {Observable}from'rxjs/Observable'
import { Observer } from 'rxjs/Observer';
import '../utilities/rxjs-operators';
import {Router}from'@angular/router';
import {Http, Response, Request, RequestOptionsArgs, RequestMethod, Headers}from'@angular/http';


import {Client, Lawyer, Address, Card}from'../utilities/classes-definition';
import {EXPERTISES, PROVINCES} from '../utilities/constant';

import {ParseObjService}from'./parse-object.service';
import {UserService}from'./user.service';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.
declare var Omise: any;


@Injectable()
export class PaymentService {


    userCards: any[] = [];
    userCards$: Observable<any>;
    userCardsOberserver: Observer<any>

    //for confirmation page.
    lawyerResponseToPay: any;
    selectedCard: any;
    selectedAddress: any;

    paymentServerURL: string = 'https://server.dfastlegal.com/payment'; 

    constructor(
        private parseObjService: ParseObjService,
        private router: Router,
        private http: Http,
        private user_S: UserService
    ) {


        this.userCards$ = new Observable((observer: any) => {
            this.userCardsOberserver = observer;
        }).share();


    }

    getUserCards() {
        return this.userCards;
    }

    loadUserCards(currentUser: any) {

        let omiseId = currentUser.get('omiseId');

        if (omiseId) {//already have id, load cards

            //get card
            console.log(`omise ID: ${omiseId}`);
            console.log(this.paymentServerURL + "/retrieveCards/" + omiseId);
            this.http.get(this.paymentServerURL + "/retrieveCards/" + omiseId)
                .toPromise()
                .then((res: Response) => {
                    //console.log(res);
                    //console.log(res.json());
                    console.log(res);
                    console.log(res.json().data);

                    this.userCards = res.json().data;
                    this.userCardsOberserver.next(this.userCards);
                });


        } else {//if not have yet, create new one.
            //create omise customer
            let userId = currentUser.id;

            this.http.get(this.paymentServerURL + '/create/' + userId)
                .toPromise()
                .then((res: Response) => {
                    //update user with omise ID
                    console.log(res);
                    this.user_S.updateUserWithOmiseID(currentUser, res.json().id).then(() => {
                        //newly create user does not have card

                    });

                }).catch((error: any) => {

                });

        }
    }

    addCard(currentUser: any, cardTokenId: string) {

        //REFERECE FORMAT
        //var card = {
        //    "name": card_form.holder_name.value,
        //    "number": card_form.number.value,
        //    "expiration_month": card_form.expiration_month.value,
        //    "expiration_year": card_form.expiration_year.value,
        //    "security_code": card_form.security_code.value
        //};

        let omiseId = currentUser.get('omiseId');

        this.http.get(`${this.paymentServerURL}/attach-card/${omiseId}/${cardTokenId}`)
            .toPromise()
            .then((res: Response) => {
                if (res.status == 200) {
                    console.log(res);
                    console.log(res.json().cards.data);

                    this.userCardsOberserver.next(res.json().cards.data);

                } else {
                    console.log(`added card failed`);
                    console.log(res);

                }

            }).catch((error: any) => {
                console.log(error.message);
            });


        //Omise.createToken("card", card, (statusCode, response) => {

        //    if (statusCode == 200) {
        //        console.log(`Create token success, add card to user`);

        //        let cardTokenId = response.id;
        //        let omiseId = currentUser.get('omiseId');

        //        this.http.get(`${this.paymentServerURL}/attach-card/${omiseId}/${cardTokenId}`)
        //            .toPromise()
        //            .then((res:Response) => {
        //                console.log(res);

        //                this.loadUserCards(currentUser);

        //            }).catch((error: any) => {
        //                console.log(error.message);
        //            });

        //    } else {
        //        console.log(response.message);
        //    }


        //});


    }

    deleteCard(currentUser: any, card: any, index: number): Promise<any> {

        let cardId = card.id;
        let omiseId = currentUser.get('omiseId');

        return this.http.get(`${this.paymentServerURL}/delete-card/${omiseId}/${cardId}`)
            .toPromise().then((res) => {
                if (res.status == 200) {
                    this.userCards.splice(index, 1);
                    this.userCardsOberserver.next(this.userCards);
                } else {
                    console.log(`delete card failed`);
                    console.log(res);
                }

            }
            );

    }


    setCardDetail(lawyerResponse: any, selectedCard: any, selectedAddress):Promise<any> {

        this.lawyerResponseToPay = lawyerResponse;
        this.selectedCard = selectedCard;
        this.selectedAddress = selectedAddress;


        // let link = ['/case-confirm-payment', this.lawyerResponseToPay.id];
        // this.router.navigate(link)
        console.log('card set')
        return;
    }

    pay(currentUser: any):Promise<any> {
        
        let omiseId = currentUser.get('omiseId');
        let amountNumber = this.lawyerResponseToPay.get('amount');
        let amountString = String(amountNumber);
        let cardId = this.selectedCard.id;
        let responseId = this.lawyerResponseToPay.id;

        //charge the card
        //end ponit REFRENCE//paymentRouter.get('/pay-case/:omiseID/:cardID/:amount/:responseId', function (req, res) {

        return this.http.get(`${this.paymentServerURL}/pay-case/${omiseId}/${cardId}/${amountString}/${responseId}`)
            .toPromise()
            .then((res: Response) => {
                if (res.status = 200) {
                    //success then update the response
                    this.lawyerResponseToPay.set('isPaid', true);                //isPaid to true
                    this.lawyerResponseToPay.set('billingAddress', this.selectedAddress);                //billing address with addressk

                    let addressObj = this.selectedAddress.get('addressObj');
                    this.lawyerResponseToPay.set('billingAddressObj', addressObj);

                    return this.lawyerResponseToPay.save();

                } else {

                    //error
                    alert(`error with status code ${res.status}`);
                }
            });

        //update then direct to lawyer case history lawyer-case-dashboard

    }


    getLawyerResponseToPay() {
        return this.lawyerResponseToPay;
    }
    getSelectedCard() {
        return this.selectedCard;
    }
    getSelectedAddress() {
        return this.selectedAddress;
    }
}