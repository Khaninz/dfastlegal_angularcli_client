﻿import {Component, OnInit, OnDestroy} from'@angular/core';
import {ActivatedRoute, Router}from'@angular/router'


import {AppLanguageService} from'../../services/app-language.service';
import {UserService}from'../../services/user.service';

@Component({
    selector: 'payment-success',
    templateUrl: './payment-success.component.html'
})

export class PaymentSuccessComponent implements OnInit, OnDestroy {

    lang: string;
    params$:any;

    responseId: string;

    constructor(

        private userService: UserService,
        public txt: AppLanguageService,
        private router: Router,
        private route: ActivatedRoute
    ) { //call a service here, not New HeroService
    }

    ngOnInit() {
        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })

        this.responseId =this.route.snapshot.params['responseId'];


    }

    ngOnDestroy() {

    }

    toCaseResponse() {
        // let link = ['/case-response', this.responseId];
        // this.router.navigate(link);
        this.router.navigateByUrl(`/${this.lang}/case-response/${this.responseId}`);
    }

}