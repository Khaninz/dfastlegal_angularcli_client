import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, RouterLink, ParamMap, Router } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Meta, Title } from "@angular/platform-browser";

//services
import { UserService } from '../../services/user.service'
import { AppLanguageService } from '../../services/app-language.service'
import { TagsService } from '../../services/tags.service'

declare var $: any;

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styles: [`
        .selected{
            color:#335578;
            font-weight:500;
            font-size:1.4rem;
        }`]
})
export class HomeComponent implements OnInit, OnDestroy {
    langSub: any;
    lang: string;

    featuredLawyers: any[] = [];
    featuredLawyersSub: any;

    clientSelected: boolean;

    params$: any;
    urlLang: string;

    constructor(

        private userService: UserService,

        public txt: AppLanguageService,
        private http: Http,
        private route: ActivatedRoute,
        private router: Router,
        private meta: Meta,
        private title: Title,
        private tags: TagsService
    ) { //call a service here, not New HeroService

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');

            title.setTitle(`${tags.HOME.title[this.lang]}`);
            meta.addTags([
                { name: 'author', content: `${this.tags.AUTHOR}` },
                { name: 'description', content: `${this.tags.HOME.description[this.lang]}` },
                
            ]);
        })

        // console.log(`${this.tags.HOME.desciption[this.lang]}`);
    }

    ngOnInit() {

        this.clientSelected = true;
        //GET
        // this.lang = this.userService.getCurrentLang();
        this.featuredLawyers = this.userService.getFeaturedLawyers();

        //SUB
        // this.langSub = this.userService.userLang$.subscribe((lang) => {
        //     // this.lang = lang;
        //     // console.log(this.router.url);
        //     this.router.navigateByUrl(`/${lang}`);
        // });

        this.featuredLawyersSub = this.userService.featuredLawyerProfiles$.subscribe((featuredLawyers) => {

            this.featuredLawyers = featuredLawyers;
            console.log(this.featuredLawyers);

        });

        //Check & Load
        if (this.featuredLawyers.length == 0) {

            this.userService.loadFeaturedLawyers();
        } else {


        }

        $('#featuredLawyerCarousel').carousel({
            cycle: true,
            interval: 4000
        });
    }

    ngOnDestroy() {
        // this.langSub.unsubscribe();
        this.featuredLawyersSub.unsubscribe();
    }

    testSendMail() {
        console.log(`sending mail`);
        this.http.get('http://localhost:1337/test-mail').subscribe(() => {
            console.log(`something done`);
        });

    }

    checkFirstLawyer(lawyer) {
        if (lawyer.id == this.featuredLawyers[0].id) {

            return true;
        } else {
            return false;
        }

    }

}