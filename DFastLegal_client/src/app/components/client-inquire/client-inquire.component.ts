﻿import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router}from'@angular/router';

//services
import {UserService} from '../../services/user.service'
import {AppLanguageService}from'../../services/app-language.service'

declare var Parse: any;

@Component({
    selector: 'client-inquire',
    templateUrl: './client-inquire.component.html',
    styles: [`
     .inActive {
      background-color: gray;
    }

    `]
})

export class ClientInquireComponent implements OnInit, OnDestroy {
    langSub: any;
    lang: string;

    signUpTitle: string = 'MR';
    signUpFirstName: string;
    signUpLastName: string;
    signUpEmail: string;
    caseDetail: string;
    provinces: string;
    isLoading: boolean = false;

    constructor(
        private userService: UserService,
        public txt: AppLanguageService,
        private router: Router
    ) {
    }

    ngOnInit() {

        this.lang = this.userService.getCurrentLang();

        this.langSub = this.userService.userLang$.subscribe((lang) => {
            this.lang = lang;
        });
    }

    ngOnDestroy() {
        this.langSub.unsubscribe();
    }

    submit() {
        this.isLoading = true

        if (this.signUpFirstName && this.signUpLastName && this.signUpEmail && this.caseDetail) {
            let EarlyClients = Parse.Object.extend('EarlyClients');
            let earlyClient = new EarlyClients();

            earlyClient.set('title', this.signUpTitle);
            earlyClient.set('firstName', this.signUpFirstName);
            earlyClient.set('lastName', this.signUpLastName);
            earlyClient.set('email', this.signUpEmail);
            earlyClient.set('caseDetail', this.caseDetail);

            earlyClient.save().then((earlyClient: any) => {
                this.isLoading = false;

                //redirect to thank you.
                this.toThankYou();
            }, (error: any) => {
                alert('มีเหตุขัดข้อง กรุณาลองอีกครั้ง');
                this.isLoading = false;
            });
        } else {
            this.isLoading = false;
            alert('คุณยังระบุข้อมูลไม่ครบถ้วนค่ะ');
        }
    }

    toThankYou() {
        
        this.router.navigate(['/inquire-thankyou']);
    }
}