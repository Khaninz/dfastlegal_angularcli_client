﻿import {Component, OnInit, OnDestroy}from'@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

import {ParseObjService}from'../../services/parse-object.service';
import {NotificationService}from'../../services/notification.service';
import {ClientSideService} from'../../services/client-side.service';
import {UserService}from'../../services/user.service';
import {AppLanguageService}from'../../services/app-language.service';
import {LawyerSideService}from'../../services/lawyer-side.service';

import {EXPERTISES, PROVINCES_W_ALL}from'../../utilities/constant';

@Component({
    selector: 'case-response',
    templateUrl: './case-response.component.html',
    styles: [`
     .selected {
      background-color: gray;
    }

    `]
})

export class CaseResponseComponent implements OnInit, OnDestroy {
    //common prop in most components
    lang: string;
    params$:any;
    EXPERTISES: any[] = [];
    isLoading: boolean;
    currentUser: any;
    currentUserSub: any;
    PROVINCES: any[] = [];

    //specific properties of component
    responseId: string;
    //lawyerResponse: any;
    //lawyerResponseSub: any;
    lawyerResponse: any;
    allLawyerResponse: any[] = [];
    allLawyerResponseSub: any;
    //response status
    isResponse: boolean;
    isAccept: boolean;
    isPaid: boolean;
    isTooLate: boolean;

    client: any;
    case: any;
    requestedLawyer: any;

    //display property
    caseId: string;
    caseDetail: string;
    caseProvinces: string[] = [];
    caseLawyerNumber: number;
    clientIntention: string;

    constructor(
        private parseObj_S: ParseObjService,
        private router: Router,
        private notifyServ: NotificationService,
        private clientSideService: ClientSideService,
        private userService: UserService,
        private route: ActivatedRoute,
        private lawyerSide_S: LawyerSideService,
        public txt: AppLanguageService

    ) {
    }

    ngOnInit() {
        this.isLoading = false;

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })

        ////get id from params
        this.responseId = this.route.snapshot.params['responseId'];

        this.currentUser = this.userService.getCurrentUser();

        this.EXPERTISES = EXPERTISES;
        this.PROVINCES = PROVINCES_W_ALL;

        //get
        this.allLawyerResponse = this.lawyerSide_S.getAllLawyerResponse();
        console.log(this.allLawyerResponse.length);

        //sub


        this.currentUserSub = this.userService.currentUser$.subscribe((user) => {
            this.currentUser = user;
        });

        this.allLawyerResponseSub = this.lawyerSide_S.allLawyerResponse$.subscribe((allLawyerResponse) => {

            this.isLoading = false;
            this.allLawyerResponse = allLawyerResponse;
            this.setModelToProp();
        }, (error) => {
            this.isLoading = false;
            console.log(`error in sub for all response`);
        });



        //check
        if (this.allLawyerResponse.length == 0) {
            this.isLoading = true;


            console.log('load direct with id; no matter with current user or not');//because it could be different user when enter the link.
            this.lawyerSide_S.getLawyerResponseById(this.responseId).then((lawyerResponse) => {

                this.isLoading = false;

                this.lawyerResponse = lawyerResponse;
                console.log(this.lawyerResponse);

                this.isResponse = this.lawyerResponse.get('isResponse');
                this.isAccept = this.lawyerResponse.get('isAccept');
                this.isPaid = this.lawyerResponse.get('isPaid');
                this.isTooLate = this.lawyerResponse.get('isTooLate');
                this.case = this.lawyerResponse.get('case');
                this.client = this.lawyerResponse.get('client');

                console.log(`isResponse:${this.isResponse}, isAccept:${this.isAccept}, isPaid:${this.isPaid}`);


            }, (error) => {
                this.isLoading = false;
                console.log(error.message);
            });

        } else {
            this.setModelToProp();
        }

    }

    setModelToProp() {
        console.log(`setting model to prop`);

        this.lawyerResponse = this.allLawyerResponse.find(lawyerRespone => lawyerRespone.id == this.responseId);

        //check first before set model to prop
        this.checkCorrectLawyer(this.lawyerResponse);

        this.isResponse = this.lawyerResponse.get('isResponse');
        this.isAccept = this.lawyerResponse.get('isAccept');
        this.isPaid = this.lawyerResponse.get('isPaid');
        this.case = this.lawyerResponse.get('case');
        this.client = this.lawyerResponse.get('client');
        this.isTooLate = this.lawyerResponse.get('isTooLate');

        console.log(`isResponse:${this.isResponse}, isAccept:${this.isAccept}, isPaid:${this.isPaid}`);

    }

    ngOnDestroy() {
        this.allLawyerResponseSub.unsubscribe();
    }

    goBack() {
        window.history.back();
    }

    declineCase() {

        if (this.currentUser) {//check user

            if (this.checkCorrectLawyer(this.lawyerResponse)) {
                let r = confirm('Are your sure you want to decline? This action cannot be undone.');

                if (r == true) {
                    this.isLoading = true;
                    this.lawyerSide_S.declineCase(this.lawyerResponse).then(() => {



                    }, (error) => { console.log(error.message); });
                } else {
                    console.log('cancel decline');
                }
            } else {
                alert(`Please login with correct lawyer to proceed`);
                //log out with out redirect
                this.userService.logOut();

            }


        } else {
            alert('please login to response');
        }



    }

    acceptCase() {

        if (this.currentUser) {//check user

            if (this.checkCorrectLawyer(this.lawyerResponse)) {
                this.isLoading = true;
                this.lawyerSide_S.acceptCase(this.lawyerResponse).then(() => {

                    this.toPayment(this.lawyerResponse);
                }, (error) => {
                    console.log(error.message);
                });

            } else {
                alert(`Please login with correct lawyer to proceed`);
                //log out with out redirect
                this.userService.logOut();

            }


        } else {
            alert('please login to response');
        }


    }


    checkCorrectLawyer(lawyerResponse: any) {

        let requestedLawyer = lawyerResponse.get('requestedLawyer');

        if (this.currentUser.id == requestedLawyer.id) {
            console.log('You are the requested lawyer');
            return true;
        } else {

            console.log('Sorry, you are not the requsted lawyer... Please login with the correct lawyer');
            return false;
        }
    }

    //Navigation
    toCaseDeclined() {

        this.router.navigateByUrl(`/${this.lang}/case-declined`)
    }
    toHome() {

        this.router.navigateByUrl(`/${this.lang}`);
    }
    toPayment(lawyerResponse) {

        this.router.navigateByUrl(`/${this.lang}/case-select-payment/${lawyerResponse.id}`)
    }

    //get methods
    getTel(user: any) {
        return user.get('phoneNumber');
    }

}