import { Component, OnInit, OnDestroy } from '@angular/core';

import { UserService } from '../../services/user.service'
import { AppLanguageService } from '../../services/app-language.service';
import { Router,ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
    selector: 'header',
    templateUrl: './header.component.html',
    providers: [],
    styles: [
        `
      .active {
      font-weight: 900;
      
       
    }
        .lang{
    color:white;
}
`
    ]
})
export class HeaderComponent implements OnInit, OnDestroy {

    logInEmail: string;
    logInPassword: string;
    currentUser: any;
    lang: string;
    isLoading: boolean = false;

    errorMsg: string;

    //subscription
    userSub: any;
    langSub: any;

    constructor(

        private userService: UserService,
        public txt: AppLanguageService,
        private router: Router,
        private route: ActivatedRoute

    ) {

    }

    ngOnInit() {

        //console.log(this.isLoading);
        //console.log('init!');
        this.userSub = this.userService.currentUser$.subscribe((user) => {
            console.log(user);
            this.currentUser = user;
            this.isLoading = false;
        });

        this.lang = this.userService.getCurrentLang();

        this.langSub = this.userService.userLang$.subscribe((lang) => {
            this.lang = lang;
        });

        this.currentUser = this.userService.getCurrentUser();


    }

    ngOnDestroy() {
        this.userSub.unsubscribe();
        this.langSub.unsubscribe();
    }

    logIn() {

        console.log(this.logInEmail);
        // console.log(this.logInPassword);

        this.isLoading = true;

        this.userService.logIn(this.logInEmail, this.logInPassword).then((user: any) => {

            this.errorMsg = "";

        }, (error: any) => {
            this.isLoading = false;

            this.errorMsg = error.message;
            alert(`Sorry: ${this.errorMsg} (Code ${error.code})`);
        });

        this.logInEmail = "";
        this.logInPassword = "";
    }

    toggleLoading() {
        this.isLoading = !this.isLoading;
        console.log(this.isLoading);
    }

    logOut() {

        this.isLoading = true;
        this.userService.logOutRedirectHome().then(() => {

        }, (error: any) => {
            this.isLoading = false;

        });

        // Parse.User.logOut().then(() => {
        //     headerComponent.currentUser = Parse.User.current();  // this will now be null
        // });

    }

    changeLang(lang: string) {

        let urlString = this.router.url;
        urlString = urlString.slice(3,urlString.length); //remove first three chars
        urlString = `/${lang}${urlString}`; //add first three char
        this.router.navigateByUrl(urlString); //navigate bu url
        // console.log(this.lang);
        
        //deprecrated
        this.userService.changeLang(lang);
    }

    activeLang(lang: string) {
        if (lang == this.lang) {
            return true;
        } else {
            return false;
        }
    }

    //GET METHODS for parse objects
    getEmail(User: any) {
        try {
            return User.get("email");
        } catch (error) {
            console.log(error.message);
        }
    }

    getFirstName(user: any) {
        try {
            return user.get(`firstname${this.lang.toUpperCase()}`);
        } catch (error) {
            console.log(error.message);
        }
    }

    isLawyer(user: any) {
        try {
            //console.log(User.get("isLawyer"));
            return user.get("isLawyer");
        } catch (error) { console.log(error.message) }
    };


    //navigation
    toHome() {
        let link = ['Home', {}]
        //this.router.navigate(link);
    }

    toLawyerSignUp() {
        let link = ['LawyerSignUp', {}]
        //this.router.navigate(link);
    }

    toLawyerProfileEdit() {
        let link = ['LawyerProfileEdit', {}]
        //this.router.navigate(link);
    }

    toServices() {

        this.router.navigateByUrl(`/${this.lang}/services`);
    }

    toPricing() {
        this.router.navigateByUrl(`/${this.lang}/pricing`);
        

    }

    toFAQS() {
        this.router.navigateByUrl(`/${this.lang}/faqs`);
        
    }

}