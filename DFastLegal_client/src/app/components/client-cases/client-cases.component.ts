﻿import {Component, OnInit, OnDestroy}from'@angular/core';
import {Router,ActivatedRoute}from'@angular/router';

import {ParseObjService}from'../../services/parse-object.service';
import {ClientSideService}from'../../services/client-side.service';
import {UserService}from'../../services/user.service';
import {AppLanguageService} from'../../services/app-language.service';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

import {PROVINCES_W_ALL} from'../../utilities/constant';

@Component({
    selector: 'client-cases',
    templateUrl: './client-cases.component.html',

})
export class ClientCasesComponent implements OnInit, OnDestroy {

    //common properties
    isLoading: boolean;
    lang: string;
    params$:any;
    curruntUser: any;
    PROVINCES: any[];
    //component specific propertis
    allClientCase: any[];
    allClientCaseSub: any;

    withLawyerContact: boolean;

    constructor(
        private clientSide_S: ClientSideService,
        private user_S: UserService,
        private parseObj_S: ParseObjService,
        private router: Router,
        public txt: AppLanguageService,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {

        //THE PATTERN
        this.withLawyerContact = false;


        //commmont get
        this.curruntUser = this.user_S.getCurrentUser();
        this.lang = this.user_S.getCurrentLang();
        this.PROVINCES = PROVINCES_W_ALL;

        //subscriptions
        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })
        this.allClientCaseSub = this.clientSide_S.allClientCase$.subscribe((allClientCase) => {
            this.allClientCase = allClientCase;
            console.log(this.allClientCase);
            this.isLoading = false;
        }, (error) => { console.log(error.message);});

        //get from service
        this.allClientCase = this.clientSide_S.getAllClientCase();
        
        //if empty
        if (this.allClientCase.length == 0) {
            //load data to service, and update subscription
            this.isLoading = true;
            this.clientSide_S.loadAllClientCase(this.curruntUser);
        }
        
    }

    ngOnDestroy() {
        this.allClientCaseSub.unsubscribe();
    }

    //Get METHODS
    getCaseDetail(clientCase:any) {
        return this.parseObj_S.getCaseDetail(clientCase);
    }

    getCaseProvinces(clientCase: any) {
        let provinces = this.parseObj_S.getCaseProvinces(clientCase);
        let allProvinceString: string = "";
        for (let province of provinces) {
            let provinceObj = this.PROVINCES.find(Province => Province.value == province);
            let provinceLangString = provinceObj.txt[this.lang];

            allProvinceString = allProvinceString + provinceLangString + " ";
        }
        return allProvinceString;
    }

    getCreatedAt(clientCase: any) {
        let createdAt = this.parseObj_S.getCreatedAt(clientCase);

        let date = new Date(createdAt);
       

        return date.toLocaleString('en-US');

        //String s = formatter.format(List.get(i).getCreatedAt());

    }

    //navigation
    toCaseDetail(clientCase: any) {
        console.log(clientCase);
        let caseId = clientCase.id;
        let link = ['/case-detail', caseId]
        this.router.navigate(link);
    }
}