﻿import {Component, OnInit, OnDestroy}from'@angular/core';
import {Router,ActivatedRoute} from '@angular/router';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

import {ParseObjService}from'../../services/parse-object.service';
import {NotificationService}from'../../services/notification.service';
import {ClientSideService} from'../../services/client-side.service';
import {UserService}from'../../services/user.service';
import {AppLanguageService}from'../../services/app-language.service';

import {EXPERTISES}from'../../utilities/constant';

@Component({
    selector: 'case-confirm',
    templateUrl: './case-confirm.component.html',
    styles: [`
     .selected {
      background-color: gray;
    }

    `]

})

export class CaseConfirmComponent implements OnInit, OnDestroy {

    //common prop in most components
    lang: string;
    params$:any;
    EXPERTISES: any[];
    isLoading: boolean;
    currentUser: any;

    //specific properties of component
    selectedLawyerProfiles: any[] = [];
    currentClientCase: any;
    currentClientCaseSub: any;
    verifyCodeInput: string;
    verificationSent: boolean = false;

    //display property
    caseDetail: string;
    caseProvinces: string[];
    caseLawyerNumber: number;

    //for pagination
    p:any;

    constructor(
        private parseObjService: ParseObjService,
        private router: Router,
        private notification_S: NotificationService,
        private clientSideService: ClientSideService,
        private userService: UserService,
        public txt: AppLanguageService,
        private route:ActivatedRoute

    ) {

    }

    ngOnInit() {

        this.EXPERTISES = EXPERTISES;
        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })

        //get data from service
        this.currentUser = this.userService.getCurrentUser();

        this.currentClientCase = this.clientSideService.getCurrentCase();

        //subscription
        this.currentClientCaseSub = this.clientSideService.currentClientCase$.subscribe((clientCase) => {
            console.log('updating current case');
            this.currentClientCase = clientCase;
            this.isLoading = false;

            if(this.currentClientCase){
                            this.selectedLawyerProfiles = this.currentClientCase.get('selectedLawyers');
            console.log(this.selectedLawyerProfiles);
            this.displayCase(this.currentClientCase);

            }else{//update with empty clientCase after cleared data upon issued case.

                console.log('case is now undefined');
            }

            

        }, (error) => {
            console.log(error.message);
            this.isLoading = false;
        });



        //check if exist?
        if (this.currentClientCase) {
            //display
            this.displayCase(this.currentClientCase);
            this.selectedLawyerProfiles = this.currentClientCase.get('selectedLawyers');
            console.log(this.selectedLawyerProfiles);
        } else {
            //if not get it via service
            this.isLoading = true;
            this.clientSideService.getExistingClientCase(this.currentUser);
        }

    }

    ngOnDestroy() {
        this.currentClientCaseSub.unsubscribe();
    }

    confirmCase() {

        if (this.verifyCodeInput == this.currentClientCase.get('verificationCode')) {

            //alert('code is correct');
            this.isLoading = true;
            this.clientSideService.issueCase().then(() => {

                console.log('all object save!');

                
                this.notification_S.notifyCase(this.currentClientCase.id).then((resCode) => {

                    if (resCode == 200) {
                        console.log('notifications sent');
                        this.currentClientCase.set('notifySent', true);
                        this.currentClientCase.save().then((clientCase: any) => {

                            this.clientSideService.clearCurrentData();
                            this.isLoading = false;
                            this.toCaseIssued();
                        });
                    } else {
                        console.log('fail sending notifications');
                        //do nothing let the server handle with cloude code or something
                        //let the user pass through any way for good UX.
                        this.isLoading = false;
                        this.toCaseIssued();
                    }
                    //update case status notifySent to true;

                    

                }, (error) => { console.log(error.message) });

                

            }, (error) => { console.log(error.message) });

        } else {
            alert('Please enter a correct verification code');
        }


    }

    toCaseIssued() {
        console.log('navigating....');
        this.router.navigateByUrl(`/${this.lang}/case-issued`);
    }

    testNotification() {

        //most simple form of request to server.
        //promised based.

        console.log('testing notification');

        this.notification_S.notifyCase(this.currentClientCase.id)
            .then((resCode: any) => {

                console.log(resCode);
                //if rescode success
                //update case status notifySent to true;

            }, (error: any) => {
                console.log(error);
            });

    }

    goBack() {
        this.router.navigateByUrl(`${this.lang}/select-lawyers/${this.currentClientCase.id}`);
    }

    //GET METHODS
    getProfileImage(profile: any) {
        let lawyer = this.parseObjService.getProfileUser(profile);
        return this.parseObjService.getProfileImageUrl(lawyer);
    }

    getLawyerName(profile: any) {
        let user = this.parseObjService.getProfileUser(profile);
        let firstname = user.get(`firstname${this.lang}`);
        let lastname = user.get(`lastname${this.lang}`);
        let fullName = firstname + " " + lastname;
        return fullName;
    }

    getExpertise(profile: any, i: any) {
        try {
            let expValue = profile.get(`expertise${i}`);
            let expertise = this.EXPERTISES.find(expertise => expertise.expValue === expValue);
            return expertise.txt[this.lang];
        } catch (error) {
            console.log(error.message);
        }
    }

    getShortDesc(lawyerProfile: any) {
        return lawyerProfile.get(`shortDesc${this.lang}`);
    }

    displayCase(currentClientCase: any) {
        this.caseDetail = this.parseObjService.getCaseDetail(currentClientCase);
        this.caseProvinces = this.parseObjService.getCaseProvinces(currentClientCase);
        this.caseLawyerNumber = this.parseObjService.getCaseLawyerNumber(currentClientCase);

    }

    sendVerificationCode(user: any, clientCase: any) {

        if (user.get('phoneNumber')) {
            this.isLoading = true;
            this.clientSideService.genVerificationCode(clientCase).then((clientCase) => {
                console.log(this.currentClientCase.get('verificationCode'));
                this.isLoading = true;
                this.notification_S.sendClientVerificationCode(user, clientCase).then(() => {

                    this.isLoading = false;

                    console.log('notification sent to client');
                    this.verificationSent = true;

                }, (error) => {
                    this.isLoading = false;
                });

            }, (error) => {
                this.isLoading = false;
            });

        } else {
            alert('please provide your phone number for verification');
            // this.router.navigate(['/client-profile-edit']);
            this.router.navigateByUrl(`/${this.lang}/client-profile-edit`);
        }

        
    }

    toggleVerificationSent() {
        this.verificationSent = !this.verificationSent;
        this.verifyCodeInput = "";
    }

    getProgressImage() {
        return `img/progress bar-3-${this.lang.toUpperCase()}.svg`;
    }
}