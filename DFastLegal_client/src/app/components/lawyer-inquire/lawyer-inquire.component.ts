﻿import {Component,} from '@angular/core';
import {Router} from'@angular/router';

//services
import {UserService} from '../../services/user.service'
import {AppLanguageService}from'../../services/app-language.service'

declare var Parse: any;

@Component({
    selector: 'lawyer-inquire',
    templateUrl: './lawyer-inquire.component.html',
})

export class LawyerInquireComponent {

    langSub: any;
    lang: string;

    isComp: boolean = false;
    signUpTitle: string = 'MR';
    isLoading: boolean = false;
    signUpFirstName: string;
    signUpLastName: string;
    signUpEmail: string;
    companyType: string = 'firm';
    signUpCompName: string;

    constructor(
        private router: Router,
        private userService: UserService,
        public txt: AppLanguageService
    ) {
    }

    ngOnInit() {
        console.log(this.lang);

        this.lang = this.userService.getCurrentLang();

        this.langSub = this.userService.userLang$.subscribe((lang) => {
            this.lang = lang;
        });
    }

    ngOnDestroy() {
        this.langSub.unsubscribe();
    }

    submit() {
        
        if (this.signUpFirstName && this.signUpLastName && this.signUpEmail) {
            let EarlyLawyers = Parse.Object.extend('EarlyLawyers');
            let earlyLawyer = new EarlyLawyers();

            earlyLawyer.set('title', this.signUpTitle);
            earlyLawyer.set('firstName', this.signUpFirstName);
            earlyLawyer.set('lastName', this.signUpLastName);
            earlyLawyer.set('email', this.signUpEmail);
            earlyLawyer.set('isComp', this.isComp);
            earlyLawyer.set('compType', this.companyType);
            //earlyLawyer.set('compName', this.signUpCompName);

            this.isLoading = true;

            earlyLawyer.save().then((earlyLawyer: any) => {
                this.isLoading = false;
                this.toThankyou();
            });

        } else {
            alert('คุณยังกรอกข้อมูลไม่ครบค่ะ');
        }
        
    }

    toThankyou() {
        let link = ['/inquire-thankyou', {}];
        this.router.navigate(link);
    }
}