﻿import {Component, OnInit, OnDestroy}from'@angular/core';
import {Router,ActivatedRoute}from'@angular/router';

import {AppLanguageService}from'../../services/app-language.service';
import {UserService}from'../../services/user.service';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

@Component({
    selector: 'case-confirm',
    templateUrl: './case-issued.component.html',
    styles: []
})

export class CaseIssuedComponent implements OnInit, OnDestroy {

    lang: string;
    params$:any;

    constructor(
        public txt: AppLanguageService,
        private user_S: UserService,
        private route:ActivatedRoute
    ) {

    }

    ngOnInit() {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })


    }

    ngOnDestroy() {
    }

    getProgressImage() {
        return `img/progress bar-4-${this.lang.toUpperCase()}.svg`;
    }
}