import {Component,OnInit, OnDestroy}from'@angular/core';
import {Router,ActivatedRoute}from'@angular/router';

import {ParseObjService}from'../../services/parse-object.service';
import {ClientSideService}from'../../services/client-side.service';
import {UserService}from'../../services/user.service';
import {AppLanguageService} from'../../services/app-language.service';
import {Meta,Title} from'@angular/platform-browser';
import {TagsService} from'../../services/tags.service';


declare var Parse: any; //must be declare to be able to use Parse JS sdk.

import {PROVINCES_W_ALL,EXPERTISES_W_ALL} from'../../utilities/constant';

@Component({
    selector: 'lawyers',
    templateUrl: './lawyers.component.html'

})
export class LawyersComponent implements OnInit, OnDestroy{


    isLoading: boolean;

    lawyerProfiles: any[] = [];
    lawyerProfilesSub: any;

    expertiseObjects: any[];
    provinceObjects: any[];

    lang: string;
    params$:any;

    p:any;

    //search input properties
    expertiseInput: string
    provinceInput: string;
    provinceInputArray: string[]
    sortInput: string;



    constructor(
        private router: Router,
        private parseObjService: ParseObjService,
        private clientSideService: ClientSideService,
        private userService: UserService,
        public txt: AppLanguageService,
        private route: ActivatedRoute,
        private meta:Meta,
        private title:Title,
        private tags:TagsService

    ) {
        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');

            title.setTitle(`${tags.LAWYERS.title[this.lang]}`);
            meta.addTags([
                { name: 'author', content: `${this.tags.AUTHOR}` },
                { name: 'description', content: `${this.tags.LAWYERS.description[this.lang]}` },
                
            ]);
        })

    }

    ngOnInit() {

        this.isLoading = true;
        
        this.expertiseObjects = EXPERTISES_W_ALL;
        this.provinceObjects = PROVINCES_W_ALL;

        this.provinceInputArray = this.clientSideService.getProvinceInput();

        this.expertiseInput = this.clientSideService.getExpertiseInput();
        if (this.expertiseInput == null) {
            this.expertiseInput = this.expertiseObjects[0].expValue;
        }
        if (this.provinceInputArray == null) {
            this.provinceInput = this.provinceObjects[0].value;
        }

        //subscribe to ClientSideService
        this.lawyerProfilesSub = this.clientSideService.lawyerProfiles$.subscribe((lawyerProfiles) => {

            this.lawyerProfiles = lawyerProfiles;
            this.isLoading = false;

        });

        //subscribe to UserService
        this.lawyerProfiles = this.clientSideService.getCurrentLawyerProfiles();

        if (this.lawyerProfiles.length == 0) {//if there is no profiles query for new 

            console.log('Getiing lawyer profiles');

            this.clientSideService.getFirstLawyerProfiles();
        } else { //do nothing
            console.log('already has profiles, no need to load new');
            this.isLoading = false;
        }


    }

    ngOnDestroy() {
        this.lawyerProfilesSub.unsubscribe();
    }

    startFilter() {

        this.isLoading = true;
        this.provinceInputArray = [this.provinceInput];
        this.clientSideService.filterLawyers(this.expertiseInput, this.provinceInputArray);
        //compoundQuery.include("createdBy");
        
    }

}