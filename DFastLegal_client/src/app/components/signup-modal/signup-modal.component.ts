import{Component,OnInit,OnDestroy}from'@angular/core';

import{UserService}from'../../services/user.service'
import {Client}from'../../utilities/classes-definition'
import {AppLanguageService}from'../../services/app-language.service';

declare var $: any;

@Component({
    selector: 'signup-modal',
    templateUrl: './signup-modal.component.html',
    providers:[]
})
export class SignupModalComponent implements OnInit, OnDestroy { 

    lang: string;
    langSub: any;
    isLoading: boolean;

    signUpErrorMsg: string;

    signUpTitle: string ="MR";
    signUpFirstName:string;
    signUpLastName:string;
    signUpEmail:string;
    signUpPassword:string;
    signUpConfirmPassword:string;
    signUpPersonalId:number;
    asCompany:boolean;
    signUpCompName:string;
    signUpCompId: number;
    signUpPhoneNumber: string;
    
    constructor(
        private userService: UserService,
        public txt: AppLanguageService
        
    ) { }

    ngOnInit() {

        this.isLoading = false;

        this.lang = this.userService.getCurrentLang();
        this.langSub = this.userService.userLang$.subscribe((lang) => {
            this.lang = lang;
        });

    }

    ngOnDestroy() {
        this.langSub.unsubscribe();
    }
    
    signUp(){
            // console.log(this.signUpPassword);
            // console.log(this.signUpConfirmPassword);
        if (this.signUpPassword === this.signUpConfirmPassword){
            
            let emailLowerCase = this.signUpEmail.toLowerCase();
            let isLawyer:boolean = false;

            var client = new Client();
            client.title = this.signUpTitle;
            client.email = emailLowerCase;
            client.phoneNumber = this.signUpPhoneNumber;
            client.password = this.signUpPassword;
            client.firstName = this.signUpFirstName;
            client.lastName = this.signUpLastName;
            client.personalId = this.signUpPersonalId;
            client.asCompany = this.asCompany;
            client.companyName = this.signUpCompName;
            client.companyId = this. signUpCompId;
            client.isLawyer = false;
            
            console.log(client);

            this.isLoading = true;

            this.userService.clientSignUp(client).then((currentUser: any) => {
                this.isLoading = false;

                $('#signUpModal').modal('toggle');
                //close modal when finish
                //let modal = document.getElementById('signUpModal');
                //modal.style.display = 'none';

            }, (error: any) => {
                this.isLoading = false;
                console.log(error.message);
                this.signUpErrorMsg = error.message;
            });
            
            
        }else{
            console.log('password does not match')
        }
        
    }
    
    
}