﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { AppLanguageService } from '../../services/app-language.service';
import { UserService } from '../../services/user.service';
import { Meta, Title } from '@angular/platform-browser';
import { TagsService } from '../../services/tags.service';


@Component({

    templateUrl: './help-and-support.component.html',
    styles: [`
        .active{
            color:#335578;
            font-weight:600;
        }
    `]
})

export class HelpAndSupportComponent {

    lang: string;
    params$: any;

    active: string;
    activeContent: string;
    activeHeader: string;

    constructor(
        public txt: AppLanguageService,
        private user_S: UserService,
        private route: ActivatedRoute,
        private meta: Meta,
        private title: Title,
        private tags: TagsService

    ) {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');


            title.setTitle(`${tags.HELP_SUPPORT.title[this.lang]}`);
            meta.addTags([
                { name: 'author', content: `${this.tags.AUTHOR}` },
                { name: 'description', content: `${this.tags.HELP_SUPPORT.description[this.lang]}` },

            ]);

            this.active = params.get('content'); 
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            
        })

    }

    ngOnInit() {


    }

    scrollToTop(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    }

    ngOnDestroy() {
    }

    isActive(activeContent: string) {

        if (this.active == activeContent) {
            return true;
        } else {
            return false
        }

    }

}