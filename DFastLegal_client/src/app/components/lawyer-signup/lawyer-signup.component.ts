import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';


import { Lawyer } from '../../utilities/classes-definition';
import { UserService } from '../../services/user.service';
import { AppLanguageService } from '../../services/app-language.service';


@Component({
    selector: 'lawyer-signup',
    templateUrl: './lawyer-signup.component.html'
})


export class LawyerSignUpComponent implements OnInit, OnDestroy {

    isLoading: boolean;
    error: any;

    lang: string;
    params$: any;

    signUpTitle: string = "MR";
    signUpFirstName: string;
    signUpLastName: string;
    signUpEmail: string;
    signUpPassword: string;
    signUpConfirmPassword: string;
    signUpPersonalId: number;
    asCompany: boolean;
    companyType: string;
    signUpCompName: string;
    signUpCompId: number;

    constructor(

        private userService: UserService,
        public txt: AppLanguageService,
        private router: Router,
        private route: ActivatedRoute


    ) {

    }

    ngOnInit() {
        this.isLoading = false;

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');
        })

    }

    ngOnDestroy() {

    }


    lawyerSignUp() {

        this.error = null;

        if (this.signUpPassword == this.signUpConfirmPassword) {

            var lawyer = new Lawyer();

            lawyer.title = this.signUpTitle;
            lawyer.firstName = this.signUpFirstName;
            lawyer.lastName = this.signUpLastName;
            lawyer.personalId = this.signUpPersonalId;
            lawyer.email = this.signUpEmail;
            lawyer.password = this.signUpPassword;
            lawyer.asCompany = this.asCompany;
            lawyer.companyType = this.companyType;
            lawyer.companyName = this.signUpCompName;
            lawyer.companyId = this.signUpCompId;
            lawyer.isLawyer = true;

            this.isLoading = true;

            this.userService.lawyerSignUp(lawyer).then((lawyer) => {
                this.isLoading = false;
                console.log('sign up success and return to sign up component!');
                console.log(lawyer);
                //success direct to edit profile page.

                this.toProfileEdit();


            }, (error) => {
                this.isLoading = false;
                this.error = error;
                console.log(error.message);
            });

        } else {

            console.log('password does not match.');
        }

    }

    toProfileEdit() {
        // let link = ['/lawyer-profile-edit'];
        // this.router.navigate(link);
        this.router.navigateByUrl(`/${this.lang}/lawyer-profile-edit`);
    }

}