﻿import {Component,OnInit,OnDestroy} from'@angular/core';
import {ActivatedRoute} from'@angular/router';

import {AppLanguageService} from'../../services/app-language.service';
import {UserService}from'../../services/user.service';


@Component({
    selector: 'case-declined',
    templateUrl: './case-declined.component.html'
})

export class CaseDeclinedComponent implements OnInit, OnDestroy {

    lang: string;
    params$:any;

    constructor(

        private userService: UserService,
        public txt: AppLanguageService,
        private route: ActivatedRoute
    ) { //call a service here, not New HeroService
    }

    ngOnInit() {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })

    }

    ngOnDestroy() {
    }

}