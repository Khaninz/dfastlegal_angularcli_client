﻿import {Component, OnInit, OnDestroy}from'@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

import {ParseObjService}from'../../services/parse-object.service';
import {NotificationService}from'../../services/notification.service';
import {ClientSideService} from'../../services/client-side.service';
import {UserService}from'../../services/user.service';
import {AppLanguageService}from'../../services/app-language.service';
import {LawyerSideService}from'../../services/lawyer-side.service';
import {PaymentService}from'../../services/payment.service';

import {EXPERTISES, PROVINCES_W_ALL}from'../../utilities/constant';

@Component({
    selector: 'case-response',
    templateUrl: './case-confirm-payment.component.html',
    styles: [`
     .selected {
      background-color: gray;
    }

    `]
})

export class CaseConfirmPaymentComponent implements OnInit, OnDestroy {
    //common prop in most components
    lang: string;
    params$:any;
    EXPERTISES: any[] = [];
    isLoading: boolean;
    currentUser: any;
    PROVINCES: any[] = [];

    //specific properties of component
    responseId: string;
    lawyerResponse: any;
    selectedAddress: any;
    selectedCard: any;
    hasAcceptedToTerm: boolean;
    errorMsg: string;

    client: any;
    case: any;
    requestedLawyer: any;


    constructor(
        private parseObj_S: ParseObjService,
        private router: Router,
        private notifyServ: NotificationService,
        private clientSideService: ClientSideService,
        private userService: UserService,
        private route: ActivatedRoute,
        private lawyerSide_S: LawyerSideService,
        private payment_S: PaymentService,
        public txt: AppLanguageService

    ) {
    }

    ngOnInit() {
        this.isLoading = false;

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })

        ////get id from params
        this.responseId = this.route.snapshot.params['responseId'];
        console.log(this.responseId);

        this.currentUser = this.userService.getCurrentUser();

        this.EXPERTISES = EXPERTISES;
        this.PROVINCES = PROVINCES_W_ALL;

        this.lawyerResponse = this.payment_S.getLawyerResponseToPay();
        this.selectedCard = this.payment_S.getSelectedCard();
        this.selectedAddress = this.payment_S.getSelectedAddress();

        console.log(this.lawyerResponse);
        console.log(this.selectedCard);
        console.log(this.selectedAddress);

        //subscription



        if (this.lawyerResponse && this.selectedCard && this.selectedAddress) {
            //all exsist do nothing

            this.client = this.lawyerResponse.get('client');
            this.case = this.lawyerResponse.get('case');


        } else {
            //naviagate back to select-payment
            this.toSelectPayment();
        }

    }

    ngOnDestroy() {

    }

    goBack() {
       this.router.navigateByUrl(`/${this.lang}/case-select-payment/${this.lawyerResponse.id}`);
    }

    pay() {

        if (this.hasAcceptedToTerm) {

            //check if case is already full
            this.case.fetch().then((currentCase) => {

                let preferredLawyer = currentCase.get('preferredLawyer');
                let lawyerAccepted = currentCase.get('lawyerAccepted');

                if (lawyerAccepted >= preferredLawyer) {

                    alert(`We are sorry, contacted lawyers have reach client's preferred limit.`);
                    //Do something with it
                    //set response with isTooLate to true.
                    this.lawyerResponse.set('isTooLate', true);
                    this.lawyerResponse.save().then(() => {

                        this.toLawyerCase();
                    });

                } else {
                    this.isLoading = true;
                    this.payment_S.pay(this.currentUser).then((lawyerResponse) => {

                        //payment and update lawyer response success
                        this.isLoading = false;
                        console.log(`payment success`);
                        console.log(lawyerResponse);

                        //redirect to lawyer's case
                        //To thank you page

                        this.case.increment('lawyerAccepted');
                        this.case.save().then(() => {
                            this.toPaymentSuccess();

                        });

                    }, (error) => {
                        this.isLoading = false;
                        console.log(error.message);
                    });
                }

            });



        } else {

            this.errorMsg = this.txt.AGREE_REQUIRE[this.lang];

        }



    }

    //Navigation
    toSelectPayment() {

        this.router.navigateByUrl(`/${this.lang}/case-select-payment/${this.responseId}`);

    }

    toPaymentSuccess() {


        this.router.navigateByUrl(`/${this.lang}/payment-success/${this.responseId}`);

    }

    toLawyerCase() {

        this.router.navigateByUrl(`/${this.lang}/lawyer-case`);
    }

    getAddressObj(address) {
        return address.get('addressObj');
    }

}