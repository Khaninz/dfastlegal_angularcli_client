﻿import {Component, OnInit, OnDestroy} from'@angular/core';
import {ActivatedRoute, Router}from'@angular/router'


import {AppLanguageService} from'../../services/app-language.service';
import {UserService}from'../../services/user.service';
import {LawyerSideService}from'../../services/lawyer-side.service';

@Component({
    selector: 'lawyer-case',
    templateUrl: './lawyer-case.component.html'
})

export class LawyerCaseComponent implements OnInit, OnDestroy {


    //common property
    lang: string;
    params$:any;
    currentUser: any;
    currentUserSub: any;
    isLoading: boolean;

    //component specific property
    allLawyerResponse: any[] = [];
    allLawyerResponseSub: any;
    lawyerResponse: any;

    //type of response
    pendingResponse: any[] = [];
    acceptedResponse: any[] = [];
    declinedResponse: any[] = [];
    tooLateResponse: any[] = [];

    constructor(

        private userService: UserService,
        public txt: AppLanguageService,
        private router: Router,
        private route: ActivatedRoute,
        private lawyerSide_S: LawyerSideService
    ) { //call a service here, not New HeroService
    }

    ngOnInit() {

        //set common property
        this.isLoading = false;
        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })

        //get data from service
        this.currentUser = this.userService.getCurrentUser();
        this.allLawyerResponse = this.lawyerSide_S.getAllLawyerResponse();

        //subscriptions
        this.currentUserSub = this.userService.currentUser$.subscribe((currentUser) => {
            this.currentUser = currentUser;
        });

        this.allLawyerResponseSub = this.lawyerSide_S.allLawyerResponse$.subscribe((allLawyerResponse) => {
            this.allLawyerResponse = allLawyerResponse;

            this.categorizeResponse();


            this.isLoading = false;
        });


        //If empty, load it
        if (this.allLawyerResponse.length == 0) {
            this.isLoading = true;
            this.lawyerSide_S.loadAllLawyerResponse(this.currentUser);

        } else {

            this.categorizeResponse();

        }

    }

    categorizeResponse() {
        for (let response of this.allLawyerResponse) {
            console.log(response);
            let isResponse = response.get('isResponse');
            let isAccept = response.get('isAccept');
            let isTooLate = response.get('isTooLate');

            console.log(isResponse);
            console.log(isAccept);

            if (isResponse) {

                if (isAccept) {

                    if (isTooLate) {

                        this.tooLateResponse.push(response);

                    } else {
                        this.acceptedResponse.push(response);

                    }

                    
                } else {

                    this.declinedResponse.push(response)
                }

            } else {
                this.pendingResponse.push(response);
            }

        }

        console.log(`all response: ${this.allLawyerResponse.length}`);
        console.log(`accepted: ${this.acceptedResponse.length}, declined: ${this.declinedResponse.length}, pending: ${this.pendingResponse.length}, missed: ${this.tooLateResponse.length}
        `);
    }


    toLawyerCaseDetail(lawyerResponse) {

        let responseId = lawyerResponse.id;
        let link = ['/lawyer-case', responseId];
        this.router.navigate(link);
    }

    ngOnDestroy() {
        this.currentUserSub.unsubscribe();
        this.allLawyerResponseSub.unsubscribe();
    }

    getClientCase(lawyerRespone: any) {
        return lawyerRespone.get(`case`);
    }

}