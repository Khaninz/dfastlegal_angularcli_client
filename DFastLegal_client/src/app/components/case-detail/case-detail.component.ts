﻿import {Component, OnInit, OnDestroy}from'@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

import {ParseObjService}from'../../services/parse-object.service';
import {NotificationService}from'../../services/notification.service';
import {ClientSideService} from'../../services/client-side.service';
import {UserService}from'../../services/user.service';
import {AppLanguageService}from'../../services/app-language.service';


import {EXPERTISES, PROVINCES_W_ALL}from'../../utilities/constant';

@Component({
    selector: 'case-confirm',
    templateUrl: './case-detail.component.html',
    styles: [`
     .selected {
      background-color: gray;
    }

    `]
})

export class CaseDetailComponent implements OnInit, OnDestroy {

    //common prop in most components
    lang: string;
    params$:any;
    EXPERTISES: any[] = [];
    isLoading: boolean;
    currentUser: any;
    PROVINCES: any[] = [];

    //specific properties of component
    caseId: string;
    allClientCase: any[] = [];
    allClientCaseSub: any;
    selectedClientCase: any;
    selectedLawyerProfiles: any[] = [];

    //display property
    caseDetail: string;
    caseProvinces: string[] = [];
    caseLawyerNumber: number;

    //for pagination
    p:any;

    constructor(
        private parseObjService: ParseObjService,
        private router: Router,
        private notifyServ: NotificationService,
        private clientSideService: ClientSideService,
        private userService: UserService,
        private route: ActivatedRoute,
        public txt: AppLanguageService

    ) {

    }

    ngOnInit() {

        this.EXPERTISES = EXPERTISES;
        this.PROVINCES = PROVINCES_W_ALL;
        //get id from params
        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })
        this.caseId = this.route.snapshot.params['caseId'];
        console.log(this.caseId);

        //get data from service
        this.currentUser = this.userService.getCurrentUser();

        this.allClientCase = this.clientSideService.getAllClientCase();
        
        //subscription
        this.allClientCaseSub = this.clientSideService.allClientCase$.subscribe((allClientCase) => {
            this.allClientCase = allClientCase;

            this.selectedClientCase = this.allClientCase.find(clientCase => clientCase.id == this.caseId);
            //console.log(this.selectedClientCase);

            this.displayCase(this.selectedClientCase);


            this.selectedLawyerProfiles = this.parseObjService.getSelectedLawyers(this.selectedClientCase);
            //console.log(this.selectedLawyerProfiles);

            this.isLoading = false;
        }, error => console.log(error.message));

        //check if exist?
        console.log(this.allClientCase.length);
        if (this.allClientCase.length == 0) {
            this.isLoading = true;
            this.clientSideService.loadAllClientCase(this.currentUser);
        } else {
            //display it
            this.selectedClientCase = this.allClientCase.find(clientCase => clientCase.id == this.caseId);
            console.log(this.selectedClientCase);
            this.displayCase(this.selectedClientCase)

            this.selectedLawyerProfiles = this.parseObjService.getSelectedLawyers(this.selectedClientCase);
            console.log(this.selectedLawyerProfiles);

        }


    }

    ngOnDestroy() {
        this.allClientCaseSub.unsubscribe();
    }

    goBack() {
        window.history.back();
    }

    //GET METHODS
    getProfileImage(profile: any) {
        let lawyer = this.parseObjService.getProfileUser(profile);
        return this.parseObjService.getProfileImageUrl(lawyer);
    }

    getLawyerName(profile: any) {
        let user = this.parseObjService.getProfileUser(profile);
        let firstname = user.get(`firstname${this.lang}`);
        let lastname = user.get(`lastname${this.lang}`);
        let fullName = firstname + " " + lastname;
        return fullName;
    }

    getExpertise(profile: any, i: any) {
        try {
            let expValue = profile.get(`expertise${i}`);
            let expertise = this.EXPERTISES.find(expertise => expertise.expValue === expValue);
            return expertise.txt[this.lang];
        } catch (error) {
            console.log(error.message);
        }
    }

    getShortDesc(lawyerProfile: any) {
        return lawyerProfile.get(`shortDesc${this.lang}`);
    }

    displayCase(currentClientCase:any) {
        this.caseDetail = this.parseObjService.getCaseDetail(currentClientCase);
        this.caseProvinces = this.parseObjService.getCaseProvinces(currentClientCase);
        this.caseLawyerNumber = this.parseObjService.getCaseLawyerNumber(currentClientCase);

    }

    getCaseProvinces(clientCase: any) {
        let provinces = this.parseObjService.getCaseProvinces(clientCase);
        let allProvinceString: string = "";
        for (let province of provinces) {
            let provinceObj = this.PROVINCES.find(Province => Province.value == province);
            let provinceLangString = provinceObj.txt[this.lang];

            allProvinceString = allProvinceString + provinceLangString + " ";
        }
        return allProvinceString;
    }
}