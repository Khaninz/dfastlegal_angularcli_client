﻿import {Component, OnInit, OnDestroy}from'@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

import {ParseObjService}from'../../services/parse-object.service';
import {Qualification}from'../../utilities/classes-definition';
import {ClientSideService}from'../../services/client-side.service';
import {AppLanguageService}from'../../services/app-language.service';
import {UserService}from'../../services/user.service';
import {EXPERTISES, DEGREE}from'../../utilities/constant'
@Component({
    selector: 'lawyer-profile',
    templateUrl: './lawyer-profile.component.html'


})

export class LawyerProfileComponent implements OnInit, OnDestroy {

    fullName: string;
    expertise1: string;
    expertise2: string;
    expertise3: string;
    qualifications: any[] = [];
    tel: string;
    email: string;
    experience: string;
    profileImageUrl: string;
    DEGREE: any[];
    EXPERTISES: any[];

    error: any;

    lawyer: any;
    lawyerProfile: any;

    lang: string;
    params$:any;

    constructor(
        private route: ActivatedRoute,
        private parseObjService: ParseObjService,
        private clientSideService: ClientSideService,
        public txt: AppLanguageService,
        private userService: UserService
    ) {

    }


    ngOnInit() {

        //subscribe to UserService
        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })

        let profileId = this.route.snapshot.params['profileId'];
        this.lawyerProfile = this.clientSideService.getViewingLawyer(profileId);

        console.log(this.lawyerProfile);

        if (this.lawyerProfile) {
            this.lawyer = this.parseObjService.getProfileUser(this.lawyerProfile);
            this.setTel(this.lawyer);

        } else {
            console.log('getting lawyer profile');
            let query = new Parse.Query('LawyerProfiles');
            query.include('createdBy');
            query.include('qualifications');
            query.get(profileId).then((lawyerProfile: any) => {
                this.lawyerProfile = lawyerProfile;
                this.lawyer = this.parseObjService.getProfileUser(this.lawyerProfile);
                this.setTel(this.lawyer);

            }, (error:any) => {
                console.log(error);
                });

        }
    }

    ngOnDestroy() {

    }

    //navigation
    goBack(): void {

        window.history.back();
    }
    
    setTel(user) {
        this.tel = this.parseObjService.getTelNumber(user);
    }
}