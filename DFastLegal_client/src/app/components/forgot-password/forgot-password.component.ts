﻿import {Component, OnInit, OnDestroy}from'@angular/core';

import {AppLanguageService}from'../../services/app-language.service';
import {UserService}from'../../services/user.service';
import {ActivatedRoute}from'@angular/router';

@Component({

    templateUrl: './forgot-password.component.html'

})

export class ForgotPasswordComponent {

    lang: string;
    params$:any;

    userEmail: string;
    isLoading: boolean;

    error: any;

    constructor(
        public txt: AppLanguageService,
        private user_S: UserService,
        private route: ActivatedRoute
    ) { }



    ngOnInit() {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })


    }

    ngOnDestroy() {
    }

    sendResetEmail() {

        this.isLoading = true;
        this.user_S.resetPassword(this.userEmail).then((success) => {

            this.isLoading = false;
            alert(this.txt.FORGOT_PW_SENT[this.lang]);
            this.userEmail = "";
            this.error = null;
        }, (error) => {
            this.isLoading = false;
            this.error = error;


        });

    }

}