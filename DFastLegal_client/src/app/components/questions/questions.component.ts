﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';


import { ParseObjService } from '../../services/parse-object.service';
import { UserService } from '../../services/user.service';
import { ClientSideService } from '../../services/client-side.service';
import { AppLanguageService } from '../../services/app-language.service';


declare var Parse: any; //must be declare to be able to use Parse JS sdk.
declare var $: any;

@Component({
    selector: 'questions',
    templateUrl: './questions.component.html',
    providers: [ParseObjService],
    styles: [`
     .selected {
      background-color: lightgray;
    }

    `]

})

export class QuestionsComponent implements OnInit, OnDestroy {

    lang: string;
    params$:any;

    currentUser: any;
    currentUserSub: any;
    currentQuestionNumber: number;
    currentQuestion: any;
    currentAnswers: any[];
    answers: any[];
    selectedAnswerId: string;
    selectedAnswer: any;
    mySubscription: any;
    isLoading: boolean;


    currentClientCase: any;
    currentClientCaseSub: any;

    allProgress: any[];
    latestProgress: any;


    constructor(
        private parseObjService: ParseObjService,
        private userService: UserService,
        private router: Router,
        private clientSideService: ClientSideService,
        public txt: AppLanguageService,
        private route:ActivatedRoute

    ) {

    }

    ngOnInit() {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
            console.log(this.lang);
        })

        this.isLoading = true;
        //1. get data from service
        this.currentUser = this.userService.getCurrentUser();

        //check if user is a lawyer
        if (this.currentUser) {
            this.checkIsLawyer(this.currentUser);
        }

        this.currentClientCase = this.clientSideService.getCurrentCase();

        //2. subscription
        this.currentUserSub = this.userService.currentUser$.subscribe((user) => {

            this.currentUser = user;
            this.checkIsLawyer(this.currentUser);

            this.isLoading = true;
            if(this.currentUser){
                this.clientSideService.queryClientCase(this.currentUser);
            }

        });

        this.currentClientCaseSub = this.clientSideService.currentClientCase$.subscribe((clientCase) => {

            console.log('observer working');
            this.currentClientCase = clientCase;
            this.updateQuestion();
        });



        //3. check
        if (this.currentUser) {
            //if have current case
            if (this.currentClientCase) {
                //load questions and progress
                console.log('already has case in service');
                console.log(this.currentClientCase);

                this.updateQuestion();

            } else {//else create new case

                //query from data base.
                console.log('no case in service yet');
                

                let isLawyer = this.currentUser.get('isLawyer')

                if (!isLawyer) {

                    this.clientSideService.queryClientCase(this.currentUser);

                }

            }

        } else {
            if (this.isQnADataLoaded()) {
                this.currentQuestion = this.clientSideService.getFirstQuestion();
                this.currentAnswers = this.currentQuestion.get('answers');
                this.isLoading = false;
                this.currentQuestionNumber = 1;

            } else {
                this.clientSideService.initQuestionsAnswersData().then(() => {
                    this.currentQuestion = this.clientSideService.getFirstQuestion();
                    this.currentAnswers = this.currentQuestion.get('answers');
                    this.isLoading = false;
                    this.currentQuestionNumber = 1;

                });
            }
        }

    }

    ngOnDestroy() {
        // console.log('component destroy!');
        this.currentUserSub.unsubscribe();
        this.currentClientCaseSub.unsubscribe();
        // console.log(this.mySubscription.isUnsubscribed);

    }

    updateQuestion() {

        console.log('updating question');
        document.body.scrollTop = 0;


        if (this.isQnADataLoaded()) {

            this.currentQuestion = this.clientSideService.getLastQuestion(this.currentClientCase);
            try {
                this.currentAnswers = this.currentQuestion.get('answers');

            } catch (error) {
                console.log(`${this}:${error.message}`);
            }
            this.isLoading = false;
            this.updateQuestionNumber();

        } else {

            this.clientSideService.initQuestionsAnswersData().then(() => {
                this.currentQuestion = this.clientSideService.getLastQuestion(this.currentClientCase);
                try {
                    this.currentAnswers = this.currentQuestion.get('answers');

                } catch (error) {
                    console.log(`${this}:${error.message}`);
                }

                this.isLoading = false;
                document.body.scrollTop = 0;

                this.updateQuestionNumber();

            });
        }

    }

    isQnADataLoaded(): boolean {

        if (this.clientSideService.getPreLoadedQuestions().length == 0
            || this.clientSideService.getPreLoadedAnswers().length == 0) {

            return false;
        } else {

            return true;
        }

    }

    submitAnswer() {

        if (Parse.User.current()) {
            if (this.selectedAnswer) {

                this.isLoading = true;
                this.clientSideService.submitAnswer(this.currentUser, this.currentQuestion, this.selectedAnswer, this.currentClientCase).then((nextQuestion: any) => {
                    this.currentQuestion = nextQuestion;
                    this.currentAnswers = this.currentQuestion.get('answers');
                    // console.log(this.currentAnswers);
                    this.selectedAnswer = null;
                    this.isLoading = false;
                    document.body.scrollTop = 0;
                    this.updateQuestionNumber();
                });

            } else {
                alert(this.txt.PLEASE_SELECT_ANSWER[this.lang]);
            }

        } else {

            alert(this.txt.PLEASE_LOGIN[this.lang]);
            $('#signUpModal').modal('toggle');
        }

    }

    toCaseDetail() {

        // let link = ['/case-detail-form', {}]
        // this.router.navigate(link);
        console.log('navagating to '+ `/${this.lang}/case-detail-form`);
        this.router.navigateByUrl(`/${this.lang}/case-detail-form`);
    }

    toPreviousQuestion() {

        let clientProgress = this.clientSideService.getClientProgress();


        if (clientProgress.length == 0) {
            this.router.navigateByUrl(`/${this.lang}/case-detail-form`);
        } else {

            this.isLoading = true;
            this.clientSideService.toPreviousQuestion();
        }

    }

    selectAnswer(answerObj: any) {
        this.selectedAnswer = answerObj;

    }

    isSelected(answer: any) {
        if (answer == this.selectedAnswer) {
            return true;
        } else {
            return false;
        }

    }

    updateQuestionNumber() {
        let clientProgress = this.clientSideService.getClientProgress();
        this.currentQuestionNumber = clientProgress.length + 1;
    }

    checkIsLawyer(user: any) {
        //if is a lawyer alert and redirect to home page.
        let isLawyer: boolean = this.parseObjService.getIsLawyer(user);

        if (isLawyer) {
            alert(this.txt.ONLY_FOR_CLIENT[this.lang]);

            this.toHome();
        } else {
            //do nothing.
        }

    }

    //GET METHODS
    getQuestionText(question: any) {
        return this.parseObjService.getQuestionText(question, this.lang.toUpperCase());
    }

    getQuestionHint(question: any) {
        return this.parseObjService.getQuestionHint(question, this.lang.toUpperCase());
    }

    getAnswerText(answer: any) {
        return this.parseObjService.getAnswerText(answer, this.lang.toUpperCase());

    }

    getAnswerHint(answer: any) {
        //return this.parseObjService.getAnswerHint(answer);
        return answer.get(`answerHint${this.lang.toUpperCase()}`);
    }

    getProgressImage() {
        return `img/progress bar-1-${this.lang.toUpperCase()}.svg`;
    }

    //navigation
    toHome() {
        this.router.navigateByUrl(`/${this.lang}`);
    }
}