﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Lawyer, Qualification } from '../../utilities/classes-definition';

import { UserService } from '../../services/user.service'
import { AppLanguageService } from '../../services/app-language.service';
import { EmailService } from '../../services/email.service'

import { EXPERTISES, TITLES, VERIFY_STATUS, DEGREE } from '../../utilities/constant';

import {Meta,Title} from'@angular/platform-browser';
import {TagsService} from'../../services/tags.service';


declare var Parse: any; //must be declare to be able to use Parse JS sdk.
declare var $: any;

@Component({
    selector: 'lawyer-profile-edit',
    templateUrl: './lawyer-profile-edit.component.html',
})

export class LawyerProfileEditComponent implements OnInit, OnDestroy {

    lang: string;
    params$:any;
    expertiseObjects: any[];
    isLoading: boolean;
    isLoadingSub: any;
    verifyStatusObjects: any[];
    DEGREE: any[];
    isToggled: boolean;

    //user common property
    title: string;
    firstnameTH: string;
    firstnameEN: string;
    lastnameTH: string;
    lastnameEN: string;
    email: string;
    phoneNumber: string;
    profileImageURL: string = "img/empty-profile.jpg";
    currentPassword: string;
    asCompany: boolean;
    companyNameTH: string;
    companyNameEN: string;
    companyID: string;

    //lawyer profile property
    expertise1: string;
    expertise2: string;
    expertise3: string;
    qualifications: any[] = [];
    experienceTH: string;
    experienceEN: string;
    verifyStatus: string;
    isVerified: boolean;
    useMatchingService: boolean;
    useListingService: boolean;
    hasLawyerTicket: boolean;
    lawyerTicket: any;

    //adding qualification
    addedDegree: string = "UNDER";
    addedInstitute: string;
    addedStudyField: string;
    shortDescTH: string;
    shortDescEN: string;

    //working object
    currentUser: any;
    currentUserSub: any;
    lawyerProfile: any;
    lawyerProfileSub: any;

    constructor(

        private userService: UserService,
        private router: Router,
        public txt: AppLanguageService,
        private email_S: EmailService,
        private route: ActivatedRoute,
        private meta:Meta,
        private titleTag:Title,
        private tags:TagsService

    ) {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');

            titleTag.setTitle(`${tags.LAWYER_PROFILE_EDIT.title[this.lang]}`);
            meta.addTags([
                { name: 'author', content: `${this.tags.AUTHOR}` },
                { name: 'description', content: `${this.tags.LAWYER_PROFILE_EDIT.description[this.lang]}` },
                
            ]);
        })


        this.currentUserSub = this.userService.currentUser$.subscribe((currentUser) => {

            //on client/lawyer profile edit only use subscription to change profile picture.
            if (currentUser) {
                var imageFile = currentUser.get("profilePicture");
                var imageFileURL = imageFile.url();
                this.profileImageURL = imageFileURL;
                this.isLoading = false;
                this.isToggled = false;


            }

        });

        this.isLoadingSub = this.userService.isLoading$.subscribe((isLoading) => {

            this.isLoading = isLoading;
        });


    }

    ngOnInit() {

        this.isLoading = true;
        this.isToggled = false;

        this.currentUser = this.userService.getCurrentUser();

        console.log(this.currentUser);
        if (this.currentUser) {
            console.log('have user!');
            if (this.currentUser.get('isLawyer')) {
                this.displayBasicInfo();

                this.verifyStatusObjects = VERIFY_STATUS;
                this.DEGREE = DEGREE;

                this.expertiseObjects = EXPERTISES;

                //this.lawyerProfile = this.userService.getCurrentLawyerProfile();
                //this.displayProfile(this.lawyerProfile);


                this.lawyerProfile = this.currentUser.get("lawyerProfile");

                let query = new Parse.Query("LawyerProfiles");
                query.include('qualifications');
                query.include('lawyerTicket');
                query.get(this.lawyerProfile.id).then((lawyerProfile: any) => {
                    this.lawyerProfile = lawyerProfile;
                    this.displayProfile(this.lawyerProfile);
                    this.isLoading = false;
                }, (error: any) => {
                    console.log(error.message);
                });


            } else {

                console.log('you are not a lawyer');
                this.toHome();

            }

        } else {

            console.log(`don't have user`);
            this.toHome();

        }

        //this.lawyerProfileSub = this.userService.currentLawyerProfile$.subscribe((lawyerProfile) => {
        //    this.lawyerProfile = lawyerProfile;
        //    this.displayProfile(this.lawyerProfile);
        //    console.log(this.lawyerProfile);
        //    this.isLoading = false;
        //});

    }

    ngOnDestroy() {
        //this.lawyerProfileSub.unsubscribe();
        this.currentUserSub.unsubscribe();
        this.isLoadingSub.unsubscribe();
    }



    //Utils
    displayBasicInfo() {
        console.log('displaying basic info...');
        this.title = this.currentUser.get("title");
        this.firstnameTH = this.currentUser.get("firstnameTH");
        this.firstnameEN = this.currentUser.get("firstnameEN");
        this.lastnameTH = this.currentUser.get("lastnameTH");
        this.lastnameEN = this.currentUser.get("lastnameEN");
        this.email = this.currentUser.get("username");
        this.phoneNumber = this.currentUser.get("phoneNumber");
        this.asCompany = this.currentUser.get('asCompany');
        this.companyNameTH = this.currentUser.get('companyName');
        this.companyNameEN = this.currentUser.get('companyNameEN');
        this.companyID = this.currentUser.get('companyID');


        let imageFile = this.currentUser.get("profilePicture");
        if (imageFile) {
            this.profileImageURL = imageFile.url();
        }
    }
    displayProfile(lawyerProfile: any) {
        this.expertise1 = lawyerProfile.get('expertise1');
        this.expertise2 = lawyerProfile.get('expertise2');
        this.expertise3 = lawyerProfile.get('expertise3');
        console.log(`${this.expertise1},${this.expertise2},${this.expertise3}`);
        this.qualifications = lawyerProfile.get('qualifications');
        this.experienceTH = lawyerProfile.get('experienceTH');
        this.experienceEN = lawyerProfile.get('experienceEN');
        this.verifyStatus = lawyerProfile.get('verifyStatus');
        this.isVerified = lawyerProfile.get('isVerified');
        this.useMatchingService = lawyerProfile.get('useMatchingService');
        this.useListingService = lawyerProfile.get('useListingService');
        this.shortDescTH = lawyerProfile.get('shortDescTH');
        this.shortDescEN = lawyerProfile.get('shortDescEN');
        this.hasLawyerTicket = lawyerProfile.get('hasLawyerTicket');
        this.lawyerTicket = lawyerProfile.get('lawyerTicket');
        console.log(this.lawyerTicket);
    }

    getVerifyStatus() {
        let status = this.verifyStatusObjects.find(status => status.value === this.verifyStatus)
        return status.txt[this.lang];
    }



    addQualification() {

        //add file here
        var fileUploadControl = $("#qualificationFile")[0];

        if (fileUploadControl.files.length > 0) {
            var file = fileUploadControl.files[0];
            file.crossOrigin = "Anonymous";


            if (file.size <= 3144319) {//less than than 3MB

                this.saveProfile().then(() => {

                    this.isLoading = true;
                    console.log(file);

                    let Qualifications = Parse.Object.extend('Qualifications');
                    let qualification = new Qualifications();

                    qualification.set("degree", this.addedDegree);
                    qualification.set("institute", this.addedInstitute);
                    qualification.set("studyField", this.addedStudyField);

                    var parseFile = new Parse.File(file.name, file);
                    qualification.set("document", parseFile);
                    qualification.set("documentName", file.name);

                    this.qualifications.push(qualification);

                    this.lawyerProfile.set('qualifications', this.qualifications);
                    this.lawyerProfile.set('verifyStatus', 'WAIT_FOR_DOCS');
                    this.lawyerProfile.set('isVerified', false);
                    this.lawyerProfile.set('verificationRequired', true);

                    this.lawyerProfile.save().then((lawyerProfile: any) => {
                        this.isLoading = false;
                        this.addedDegree = "UNDER";
                        this.addedInstitute = "";
                        this.addedStudyField = "";

                        this.displayProfile(lawyerProfile);

                        //unsub email
                        this.email_S.unsubscribe('not_yet_sumitted_docs_lawyers_list@mail.dfastlegal.com', this.currentUser.get('username'));

                        //clear file input
                        fileUploadControl.value = "";
                        console.log(lawyerProfile);

                    }, (error: any) => {
                        console.log(error.message);
                        this.qualifications.pop();
                        this.isLoading = false;
                    });

                });

            } else {
                alert('Please upload file less than 3MB');

            }

        } else {
            alert(this.txt.FILE_IS_REQUIRED[this.lang]);

        }

    }

    deleteQualification(qualification: any, index: any) {

        this.isLoading = true;
        qualification.destroy().then(() => {


            this.qualifications.splice(index, 1);
            this.lawyerProfile.set("qualifications", this.qualifications);
            return this.lawyerProfile.save();
        }).then(() => {
            this.isLoading = false;
        })
    }

    addLawyerTicket() {

        //add file here
        var fileUploadControl = $("#LawyerTicketFile")[0];

        if (fileUploadControl.files.length > 0) {
            var file = fileUploadControl.files[0];
            file.crossOrigin = "Anonymous";


            if (file.size <= 3144319) {//less than than 3MB

                this.saveProfile().then(() => {
                    this.isLoading = true;
                    console.log(file);

                    let LawyerTickets = Parse.Object.extend('LawyerTickets');
                    let lawyerTicket = new LawyerTickets();

                    var parseFile = new Parse.File(file.name, file);
                    lawyerTicket.set("document", parseFile);
                    lawyerTicket.set("documentName", file.name);


                    this.lawyerProfile.set('lawyerTicket', lawyerTicket);
                    this.lawyerProfile.set('verifyStatus', 'WAIT_FOR_DOCS');
                    this.lawyerProfile.set('isVerified', false);
                    this.lawyerProfile.set('verificationRequired', true);
                    this.lawyerProfile.set('hasLawyerTicket', true);

                    this.lawyerProfile.save().then((lawyerProfile: any) => {

                        this.displayProfile(lawyerProfile);

                        //clear file input
                        fileUploadControl.value = "";
                        console.log(lawyerProfile);
                        this.isLoading = false;
                    }, (error: any) => {
                        console.log(error.message);
                        this.isLoading = false;
                    });

                });


            } else {
                alert('Please upload file less than 3MB');

            }

        } else {
            alert(this.txt.FILE_IS_REQUIRED[this.lang]);

        }

    }

    deleteLawyerTicket(lawyerTicket: any) {

        this.isLoading = true;

        lawyerTicket.destroy().then(() => {
            this.lawyerProfile.set('lawyerTicket', undefined);
            this.lawyerProfile.set('hasLawyerTicket', false);
            return this.lawyerProfile.save();
        }).then((lawyerProfile) => {
            this.displayProfile(lawyerProfile);
            this.isLoading = false;
        }, (error) => {

            console.log(error.message);
            this.isLoading = false;
        });

        //destroy object
        //update profile?
        //has ticket to false

    }

    saveProfile(): Promise<any> {

        this.isLoading = true;
        this.lawyerProfile.set("useMatchingService", this.useMatchingService);
        this.lawyerProfile.set("useListingService", this.useListingService);
        this.lawyerProfile.set("expertise1", this.expertise1);
        this.lawyerProfile.set("expertise2", this.expertise2);
        this.lawyerProfile.set("expertise3", this.expertise3);
        this.lawyerProfile.set("shortDescTH", this.shortDescTH);
        this.lawyerProfile.set("shortDescEN", this.shortDescEN);
        this.lawyerProfile.set("experienceTH", this.experienceTH);
        this.lawyerProfile.set("experienceEN", this.experienceEN);

        this.currentUser.set("title", this.title);
        this.currentUser.set("firstnameTH", this.firstnameTH);
        this.currentUser.set("firstnameEN", this.firstnameEN);
        this.currentUser.set("lastnameTH", this.lastnameTH);
        this.currentUser.set("lastnameEN", this.lastnameEN);
        this.currentUser.set("username", this.email);
        this.currentUser.set("email", this.email);
        this.currentUser.set("phoneNumber", this.phoneNumber);


        return this.currentUser.save().then(() => {
            return this.lawyerProfile.save();
        }).then(() => {

            this.isLoading = false;
        });

    }

    changeProfilePicture() {

        this.userService.changeProfilePicture();

    }

    manualChangeProfilePic() {


        var fileUploadControl = $("#selectedProfilePicture")[0];

        if (fileUploadControl.files.length > 0) {
            var file = fileUploadControl.files[0];
            file.crossOrigin = "Anonymous";

            this.userService.manualChangeProfilePicture(file);
            //     if (file.size <= 3144319) {//less than than 3MB

            //         this.saveProfile().then(() => {

            //             this.isLoading = true;
            //             console.log(file);

            //             let Qualifications = Parse.Object.extend('Qualifications');
            //             let qualification = new Qualifications();

            //             qualification.set("degree", this.addedDegree);
            //             qualification.set("institute", this.addedInstitute);
            //             qualification.set("studyField", this.addedStudyField);

            //             var parseFile = new Parse.File(file.name, file);
            //             qualification.set("document", parseFile);
            //             qualification.set("documentName", file.name);

            //             this.qualifications.push(qualification);

            //             this.lawyerProfile.set('qualifications', this.qualifications);
            //             this.lawyerProfile.set('verifyStatus', 'WAIT_FOR_DOCS');
            //             this.lawyerProfile.set('isVerified', false);
            //             this.lawyerProfile.set('verificationRequired', true);

            //             this.lawyerProfile.save().then((lawyerProfile: any) => {
            //                 this.isLoading = false;
            //                 this.addedDegree = "UNDER";
            //                 this.addedInstitute = "";
            //                 this.addedStudyField = "";

            //                 this.displayProfile(lawyerProfile);


            //                 //clear file input
            //                 fileUploadControl.value = "";
            //                 console.log(lawyerProfile);
            //             }, (error: any) => {
            //                 console.log(error.message);
            //                 this.qualifications.pop();
            //                 this.isLoading = false;
            //             });

            //         });

            //     } else {
            //         alert('Please upload file less than 3MB');

            //     }

        } else {
            alert(`Please choose picture to upload first`);

        }

    }

    togglePictureUpload() {
        console.log(`toggling`);

        this.isToggled = !this.isToggled;
    }

    //GET methods
    getDegree(qualification: any): string {
        let degreeValue = qualification.get('degree');
        let degree = this.DEGREE.find(degree => degree.value === degreeValue);
        return degree.txt[this.lang];
    }
    getInstitute(qualification: any): string {
        return qualification.get('institute');
    }
    getStudyField(qualification: any): string {
        return qualification.get('studyField');
    }
    getDocumentUrl(qualification: any) {
        try {
            let file = qualification.get('document');
            let fileUrl = file.url();
            return fileUrl;
        } catch (error) {

        }

    }


    getDocumentName(qualification: any) {
        try {
            return qualification.get('documentName');
        } catch (error) {

        }

    }
    getTicketFileUrl(lawyerTicket) {
        try {
            let file = lawyerTicket.get('document');
            let fileUrl = file.url();
            return fileUrl;
        } catch (error) {
            console.log(error.message);
        }

    }
    getTicketFileName(lawyerTicket: any) {
        try {
            return this.lawyerTicket.get('documentName');
        } catch (error) {
            console.log(error.message);

        }

    }


    //State change
    isProfileChange() {
        if (
            this.useMatchingService != this.lawyerProfile.get("useMatchingService") ||
            this.useListingService != this.lawyerProfile.get("useListingService") ||
            this.title != this.currentUser.get("title") ||
            this.firstnameTH != this.currentUser.get("firstnameTH") ||
            this.firstnameEN != this.currentUser.get("firstnameEN") ||
            this.lastnameTH != this.currentUser.get("lastnameTH") ||
            this.lastnameEN != this.currentUser.get("lastnameEN") ||
            this.email != this.currentUser.get('username') ||
            this.phoneNumber != this.currentUser.get("phoneNumber") ||
            this.expertise1 != this.lawyerProfile.get("expertise1") ||
            this.expertise2 != this.lawyerProfile.get("expertise2") ||
            this.expertise3 != this.lawyerProfile.get("expertise3") ||
            this.shortDescTH != this.lawyerProfile.get("shortDescTH") ||
            this.shortDescEN != this.lawyerProfile.get("shortDescEN") ||
            this.experienceTH != this.lawyerProfile.get("experienceTH") ||
            this.experienceEN != this.lawyerProfile.get("experienceEN")
        ) {
            return true
        } else {
            return false
        }
    }

    isPasswordNotMatch() {
        if (this.currentPassword != this.currentUser.get('password')) {
            return true;
        } else {
            return false;
        }
    }

    resetUserPassword() {

        this.isLoading = true;

        let email = this.currentUser.get('username');
        console.log(`resetting password for ${email}`);
        this.userService.resetPassword(email).then((success) => {
            this.isLoading = false;

            alert(this.txt.FORGOT_PW_SENT[this.lang]);

        }, (error) => {
            this.isLoading = false;

            console.log(error.message);
        });

    }

    //navigation
    toHome() {
        let link = ['']
        this.router.navigate(link);
    }

    toSpot(elementId: string) {

        let element = document.getElementById(elementId);
        let rect = element.getBoundingClientRect();

        console.log(element);
        console.log(rect.top);
        console.log(rect.left);
        window.scrollTo(0, rect.top - 85);

    }
}