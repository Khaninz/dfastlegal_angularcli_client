﻿import { Component, Input, OnInit} from '@angular/core';
import {Router}from'@angular/router';

import {AppLanguageService}from'../../../services/app-language.service';
import {ParseObjService}from'../../../services/parse-object.service';
import {ClientSideService}from'../../../services/client-side.service';

import {EXPERTISES}from'../../../utilities/constant';

@Component({
    selector: 'short-lawyer-profile',
    templateUrl: './short-lawyer-profile.component.html',
    styles: [`
     .selected {
      background-color: gray;
    }

    `]
})
export class ShortLawyerProfileComponent {

    @Input() lawyerProfile: any;
    @Input() lang: string;
    @Input() withContact: boolean;
    @Input() forSelection: boolean;
    @Input() caseId: string;
    @Input() hideLink: boolean;

    expertiseObjects: any[] = [];

    constructor(
        public txt: AppLanguageService,
        private parseObjService: ParseObjService,
        private router: Router,
        private clientSide_S: ClientSideService
    ) {
        
    }

    ngOnInit() {
        this.expertiseObjects = EXPERTISES;
        console.log(this.withContact);
    }

    //for select-lawyers page
    toggleSelectedProfile(profile: any) {

        let tempSelectedIds = this.clientSide_S.toggleSelectedLawyerProfile(profile);
        console.log(tempSelectedIds);

    }

    isInSelectedList(profile: any) {
        //if lawyer is in selected list marked return true;
        let selectedLawyersId = this.clientSide_S.getSelectedLawyersId();
        if (selectedLawyersId.has(profile.id)) {
            return true;
        } else {
            //else return false
            return false;
        }
    }

    //get methods
    getFullName(lawyerProfile: any) {
        var user = this.parseObjService.getProfileUser(lawyerProfile);
        var firstName = user.get(`firstname${this.lang.toUpperCase()}`);
        var lastName = user.get(`lastname${this.lang.toUpperCase()}`);
        var fullName = firstName + ' ' + lastName;
        return fullName;
    }

    getExpertise(lawyerProfile: any, i: any) {
        try {
            let expValue = lawyerProfile.get(`expertise${i}`);
            let expertise = this.expertiseObjects.find(expertise => expertise.expValue === expValue);
            return expertise.txt[this.lang];
        } catch (error) {
            console.log(error.message);
        }

    }
    getProfileImage(lawyerProfile: any) {
        let lawyer = this.parseObjService.getProfileUser(lawyerProfile);
        return this.parseObjService.getProfileImageUrl(lawyer);
    }

    getShortDesc(lawyerProfile: any) {
        return lawyerProfile.get(`shortDesc${this.lang.toUpperCase()}`);
    }

    //navigation
    seeProfile(lawyerProfile: any) {

        if (this.withContact) {
            console.log(lawyerProfile.id);
            // let link = ['/lawyer-profile', lawyerProfile.id];
            // this.router.navigate(link);
            this.router.navigateByUrl(`/${this.lang}/lawyer-profile/${lawyerProfile.id}`);
        } else if (this.forSelection) {
            // let link = ['/view-lawyer-profile', this.caseId, lawyerProfile.id];
            // this.router.navigate(link);
            this.router.navigateByUrl(`/${this.lang}/view-lawyer-profile/${this.caseId}/${lawyerProfile.id}`);
            

        } else {
            // let link = ['/selected-lawyer-profile', lawyerProfile.id];
            // this.router.navigate(link);
            this.router.navigateByUrl(`/${this.lang}/selected-lawyer-profile/${lawyerProfile.id}`);
            
        }


    }


}