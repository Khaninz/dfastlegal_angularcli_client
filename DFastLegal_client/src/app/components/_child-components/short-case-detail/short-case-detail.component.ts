﻿import { Component, Input, OnInit} from '@angular/core';
import {Router}from'@angular/router';

import {AppLanguageService}from'../../../services/app-language.service'
import {ParseObjService}from'../../../services/parse-object.service';

import {EXPERTISES, PROVINCES_W_ALL}from'../../../utilities/constant';

declare var Parse: any;

@Component({
    selector: 'short-case-detail',
    templateUrl: './short-case-detail.component.html' 
})
export class ShortCaseDetailComponent {

    @Input() clientCase: any;
    @Input() lang: string;
    @Input() isForLawyer: boolean;
    @Input() lawyerResponseId: string;

    caseDetail: string;
    casePreferredLawyers: number;
    caseAcceptedLawyers: number;
    caseProvinces: string;
    caseCreatedAt: string;

    expertiseObjects: any[] = [];
    PROVINCES: any[] = [];

    constructor(
        public txt: AppLanguageService,
        private parseObj_S: ParseObjService,
        private router: Router
    ) {
        
    }

    ngOnInit() {
        this.expertiseObjects = EXPERTISES;
        this.PROVINCES = PROVINCES_W_ALL;

        this.caseDetail = this.getCaseDetail(this.clientCase);

        this.caseProvinces = this.getCaseProvinces(this.clientCase);
        this.caseCreatedAt = this.getCreatedAt(this.clientCase);
        try {
            this.casePreferredLawyers = this.clientCase.get('preferredLawyer');
            this.caseAcceptedLawyers = this.clientCase.get('lawyerAccepted');
        } catch (error){
            console.log(error.message);
        }


    }


    //Get METHODS
    getCaseDetail(clientCase: any) {
        return this.parseObj_S.getCaseDetail(clientCase);
    }

    getCaseProvinces(clientCase: any) {
        let provinces = this.parseObj_S.getCaseProvinces(clientCase);
        let allProvinceString: string = ""; 
        for (let province of provinces) {

            try {
                let provinceObj = this.PROVINCES.find(Province => Province.value == province);
                let provinceLangString = provinceObj.txt[this.lang];

                allProvinceString = allProvinceString + provinceLangString + " ";
            } catch (error) {

            }

        }
        return allProvinceString;
    }

    getCreatedAt(clientCase: any) {
        let createdAt = this.parseObj_S.getCreatedAt(clientCase);

        let date = new Date(createdAt);


        return date.toLocaleString('en-US');

        //String s = formatter.format(List.get(i).getCreatedAt());

    }

    //navigation
    toCaseDetail(clientCase: any) {

        if (this.isForLawyer) {//if is for lawyer

            console.log('for lawyer');

            this.router.navigateByUrl(`/${this.lang}/case-response/${this.lawyerResponseId}`);

        } else {//else for client

            console.log(clientCase);
            let caseId = clientCase.id;

            this.router.navigateByUrl(`/${this.lang}/case-detail/${caseId}`);
        }


    }

}