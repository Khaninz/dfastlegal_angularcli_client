﻿import { Component, Input, OnInit} from '@angular/core';
import {Router}from'@angular/router';

import {AppLanguageService}from'../../../services/app-language.service';
import {ParseObjService}from'../../../services/parse-object.service';
import {ClientSideService}from'../../../services/client-side.service';

import {EXPERTISES, TITLES}from'../../../utilities/constant';

@Component({
    selector: 'full-address',
    templateUrl: './full-address.component.html',
    styles: [`
     .selected {
      background-color: gray;
    }

    `]
})
export class FullAddressComponent {

    @Input() address: any;
    @Input() lang: string;

    TITLES: any[];

    expertiseObjects: any[] = [];

    constructor(
        public txt: AppLanguageService,
        private parseObjService: ParseObjService,
        private router: Router,
        private clientSide_S: ClientSideService
    ) {

    }

    ngOnInit() {
        this.expertiseObjects = EXPERTISES;
        this.TITLES = TITLES;
        console.log(this.address);
    }





}