import { Component, OnInit,Input } from '@angular/core';

import {AppLanguageService}from'../../../services/app-language.service';
import {ParseObjService}from'../../../services/parse-object.service';

@Component({
  selector: 'contact-fee',
  templateUrl: './contact-fee.component.html',
  styleUrls: ['./contact-fee.component.css']
})
export class ContactFeeComponent implements OnInit {

    @Input() lawyerResponse: any;
    @Input() lang: any;

    constructor(

        public txt: AppLanguageService,
        private parseObj_S: ParseObjService

    ) { }

    ngOnInit() {
        
    }

    getContactFee(lawyerResponse: any) {
        return this.parseObj_S.getResponseContactFee(lawyerResponse);
    }

}
