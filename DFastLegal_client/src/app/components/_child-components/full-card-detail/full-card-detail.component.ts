﻿import { Component, Input, OnInit} from '@angular/core';
import {Router}from'@angular/router';

import {AppLanguageService}from'../../../services/app-language.service';
import {ParseObjService}from'../../../services/parse-object.service';
import {ClientSideService}from'../../../services/client-side.service';

import {EXPERTISES, TITLES}from'../../../utilities/constant';

@Component({
    selector: 'full-card-detail',
    templateUrl: './full-card-detail.component.html',
    styles: [`
     .selected {
      background-color: gray;
    }

    `]
})
export class FullCardDetailComponent {

    @Input() card: any;
    @Input() lang: string;

    TITLES: any[];

    expertiseObjects: any[] = [];

    constructor(
        public txt: AppLanguageService,
        private parseObjService: ParseObjService,
        private router: Router,
        private clientSide_S: ClientSideService
    ) {

    }

    ngOnInit() {
        this.expertiseObjects = EXPERTISES;
        this.TITLES = TITLES;
    }


    getCardLogoUrl = function (card) {

        let brand = card.brand;

        if (brand == 'Visa') {
            return '../../../img/cards/visa_icon.png';
        } if (brand == 'MasterCard') {
            return '../../../img/cards/mc_accpt_103_png.png'
        } else {
            return '../../../img/cards/blank_creditcard.png'
        }
    }


}