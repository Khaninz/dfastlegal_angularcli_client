﻿import { Component, Input, OnInit} from '@angular/core';
import {Router}from'@angular/router';

import {AppLanguageService}from'../../../services/app-language.service';
import {ParseObjService}from'../../../services/parse-object.service';
import {ClientSideService}from'../../../services/client-side.service';

import {EXPERTISES, TITLES}from'../../../utilities/constant';

@Component({
    selector: 'client-profile',
    templateUrl: './client-profile.component.html',
    styles: [`
     .selected {
      background-color: gray;
    }

    `]
})
export class ClientProfileComponent {

    @Input() client: any;
    @Input() lang: string;

    TITLES: any[];
    
    expertiseObjects: any[] = [];

    constructor(
        public txt: AppLanguageService,
        private parseObjService: ParseObjService,
        private router: Router,
        private clientSide_S: ClientSideService
    ) {
        
    }

    ngOnInit() {
        this.expertiseObjects = EXPERTISES;
        this.TITLES = TITLES;
    }

    

    //get methods
    getFullName(client: any) {
        var titleValue = client.get('title');
        var title = this.TITLES.find(title => title.value == titleValue);

        var firstName = client.get(`firstname${this.lang.toLocaleUpperCase()}`);
        var lastName = client.get(`lastname${this.lang.toLocaleUpperCase()}`);

        var fullName = `${title.txt[this.lang]} ${firstName} ${lastName}`;
        return fullName;
    }

    getProfileImage(client: any) {
        return this.parseObjService.getProfileImageUrl(client);
    }

    getPersonalID(client: any) {
        return this.parseObjService.getPersonalId(client);
    }

    

}