﻿import { Component, Input, OnInit} from '@angular/core';
import {Router}from'@angular/router';

import {AppLanguageService}from'../../../services/app-language.service'
import {ParseObjService}from'../../../services/parse-object.service';

import {EXPERTISES, DEGREE}from'../../../utilities/constant';

declare var Parse: any;

@Component({
    selector: 'full-lawyer-profile',
    templateUrl: './full-lawyer-profile.component.html' 
})
export class FullLawyerProfileComponent {

    @Input()lawyerProfile: any;
    @Input() lang: string;

    qualifications: any[] = [];
    profileImageUrl: string;
    DEGREE: any[] = [];
    EXPERTISES: any[] = [];


    constructor(
        public txt: AppLanguageService,
        private parseObj_S: ParseObjService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.DEGREE = DEGREE;
        this.EXPERTISES = EXPERTISES;

        this.displayProfile(this.lawyerProfile);

    }


    displayProfile(lawyerProfile: any) {

        let lawyer = this.parseObj_S.getProfileUser(lawyerProfile);

        this.qualifications = this.parseObj_S.getQualifications(lawyerProfile);
        console.log(this.qualifications);

        this.profileImageUrl = this.parseObj_S.getProfileImageUrl(lawyer);
    }

    getExpertise(lawyerProfile: any, i: any) {
        let expValue = lawyerProfile.get(`expertise${i}`);
        let expertise = this.EXPERTISES.find(expertise => expertise.expValue === expValue);
        return expertise.txt[this.lang];
    }

    getDegree(qualification: any): string {
        let degreeValue = qualification.get('degree');
        let degree = this.DEGREE.find(degree => degree.value === degreeValue);
        return degree.txt[this.lang];
    }
    getInstitute(qualification: any): string {
        return qualification.get('institute');
    }
    getStudyField(qualification: any): string {
        return qualification.get('studyField');
    }

    getExperience(lawyerProfile: any) {
        return this.parseObj_S.getExperience(lawyerProfile, this.lang);
    }

    getFullname(lawyerProfile) {
        let lawyer = this.parseObj_S.getProfileUser(lawyerProfile);
        let firstName = this.parseObj_S.getUserFirstName(lawyer, this.lang);
        let lastName = this.parseObj_S.getUserLastName(lawyer, this.lang);
        let fullName = `${firstName} ${lastName}`;

        return fullName;
    }

}