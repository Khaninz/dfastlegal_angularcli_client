﻿import { Component, Input, OnInit} from '@angular/core';
import {Router}from'@angular/router';

import {AppLanguageService}from'../../../services/app-language.service'
import {ParseObjService}from'../../../services/parse-object.service';

import {EXPERTISES, PROVINCES_W_ALL, PROVINCES}from'../../../utilities/constant';

declare var Parse: any;

@Component({
    selector: 'full-case-detail',
    templateUrl: './full-case-detail.component.html'
})
export class FullCaseDetailComponent {

    @Input() clientCase: any;
    @Input() lang: string;
    @Input() hideStatus: boolean;
    @Input() isForClient: boolean;
    @Input() isForLawyer: boolean;

    caseDetail: string;
    casePreferredLawyers: number;
    caseAcceptedLawyers: number;
    caseProvinces: string;
    caseCreatedAt: string;

    caseType: any;
    caseTypeText: string;
    minFee: string;
    minFeeExp: string;
    caseQuestionsProgress: any[] = [];

    expertiseObjects: any[] = [];
    PROVINCES: any[] = [];

    constructor(
        public txt: AppLanguageService,
        private parseObj_S: ParseObjService,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.expertiseObjects = EXPERTISES;
        this.PROVINCES = PROVINCES;

        this.caseDetail = this.getCaseDetail(this.clientCase);

        this.caseProvinces = this.getCaseProvinces(this.clientCase);
        this.caseCreatedAt = this.getCreatedAt(this.clientCase);
        try {
            this.casePreferredLawyers = this.clientCase.get('preferredLawyer');
            this.caseAcceptedLawyers = this.clientCase.get('lawyerAccepted');
            this.caseType = this.clientCase.get('caseType');
            this.minFee = this.caseType.get('minimumFee');
            this.minFeeExp = this.caseType.get('costExplain');
            this.caseQuestionsProgress = this.clientCase.get('questionsProgress');
            console.log(this.caseQuestionsProgress);
            // this.testProgress(this.caseQuestionsProgress);
        } catch (error) {
            console.log(error.message);
        }


    }


    //Get METHODS

    getCaseType() {
        return this.caseType.get(`type${this.lang.toLocaleUpperCase()}`);
    }

    getCaseDetail(clientCase: any) {
        return this.parseObj_S.getCaseDetail(clientCase);
    }

    getCaseProvinces(clientCase: any) {
        let provinces = this.parseObj_S.getCaseProvinces(clientCase);
        let allProvinceString: string = "";
        for (let province of provinces) {

            try {
                let provinceObj = this.PROVINCES.find(Province => Province.value == province);
                let provinceLangString = provinceObj.txt[this.lang];

                allProvinceString = allProvinceString + provinceLangString + " ";
            } catch (error) {

            }

        }
        return allProvinceString;
    }

    getCreatedAt(clientCase: any) {
        let createdAt = this.parseObj_S.getCreatedAt(clientCase);

        let date = new Date(createdAt);


        return date.toLocaleString('en-US');

        //String s = formatter.format(List.get(i).getCreatedAt());

    }

    getMinFeeExp(){
        return this.caseType.get(`costExplain${this.lang.toUpperCase()}`)
    }

    //navigation
    toCaseDetail(clientCase: any) {
        console.log(clientCase);
        let caseId = clientCase.id;
        let link = ['/case-detail', caseId]
        this.router.navigate(link);
    }

    getPurpose(){//in first question
        let progressOne = this.caseQuestionsProgress.find(progress => progress.get('inQuestion').id == '5bM4U2IN0s');
        let answerOne = progressOne.get('hasAnswered');
        return answerOne.get(`answerText${this.lang.toUpperCase()}`);
    }
        getPersonType(){//in Second question
        let progressTwo = this.caseQuestionsProgress.find(progress => progress.get('inQuestion').id == 'vF0xwyQd8L');
        let answerTwo = progressTwo.get('hasAnswered');
        return answerTwo.get(`answerText${this.lang.toUpperCase()}`);
    }


    testProgress(allProgress){

        for(let progress of allProgress){
            console.log(progress.id);
            console.log(progress.get('hasAnswered'));
            console.log(progress.get('inQuestion'));
            console.log(progress.get('hasAnswered').get('answerTextTH'));
        }

    }

}