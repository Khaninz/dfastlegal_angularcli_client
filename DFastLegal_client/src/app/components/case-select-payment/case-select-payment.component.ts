﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


import { AppLanguageService } from '../../services/app-language.service';
import { UserService } from '../../services/user.service';
import { LawyerSideService } from '../../services/lawyer-side.service';
import { PaymentService } from '../../services/payment.service';

import { Address, Card } from '../../utilities/classes-definition';

declare var $: any; //hack to use bootstrap jquery selector
declare var Omise: any;

@Component({
    templateUrl: './case-select-payment.component.html'
})

export class CaseSelectPaymentComponent implements OnInit, OnDestroy {
    //common property
    lang: string;
    params$: any;
    isLoading: boolean;
    cardLoading: boolean;
    addressLoading: boolean;

    //address
    billingAddresses: any[] = [];
    billingAddressesSub: any;

    //current user
    currentUser: any;
    currentUserSub: any;

    //user card
    userCards: any[] = [];
    userCardsSub: any;
    selectedCard: any;

    //lawyerResponse
    lawyerResponseId: string;
    lawyerResponse: any;
    lawyerResponseSub: any;

    //AllLawyerResponse
    allLawyerResponse: any[] = [];
    allLawyerResponseSub: any;

    //Errors
    addCardError: any;

    inputAddress: Address;
    inputCard: Card;

    selectedAddress: any;

    constructor(

        private userService: UserService,

        public txt: AppLanguageService,
        private lawyerSide_S: LawyerSideService,
        private payment_S: PaymentService,
        private route: ActivatedRoute,
        private router: Router
    ) { //call a service here, not New HeroService
    }

    ngOnInit() {
        this.isLoading = false;
        this.inputAddress = new Address();
        this.inputCard = new Card();
        this.inputCard.expiration_month = '';
        this.inputCard.expiration_year = '';

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');
        })


        //get
        this.currentUser = this.userService.getCurrentUser();
        this.billingAddresses = this.lawyerSide_S.getBillingAddresses();
        this.userCards = this.payment_S.getUserCards();
        this.selectedAddress = this.payment_S.getSelectedAddress();
        this.selectedCard = this.payment_S.getSelectedCard();

        this.lawyerResponseId = this.route.snapshot.params['responseId'];
        console.log(this.lawyerResponseId);
        this.allLawyerResponse = this.lawyerSide_S.getAllLawyerResponse();

        //subscription
        this.currentUserSub = this.userService.currentUser$.subscribe((currentUser) => {
            this.currentUser = currentUser;
        });

        this.billingAddressesSub = this.lawyerSide_S.billingAddresses$.subscribe((billingAddresses) => {
            this.billingAddresses = billingAddresses;
            this.isLoading = false;
            this.inputAddress = new Address();
            this.addressLoading = false;
            console.log(`number of billiing address ${this.billingAddresses.length}`);
        }, error => {
            this.addressLoading = false;
            console.log(error.message);
        });

        this.userCardsSub = this.payment_S.userCards$.subscribe((userCards) => {
            console.log(`updating card list`);
            this.userCards = userCards;
            this.inputCard = new Card();
            this.inputCard.expiration_month = '';
            this.inputCard.expiration_year = '';

            this.cardLoading = false;
        });

        this.allLawyerResponseSub = this.lawyerSide_S.allLawyerResponse$.subscribe((allLawyerResponse) => {
            this.isLoading = false;

            this.allLawyerResponse = allLawyerResponse;
            this.getCurrentResponse(this.lawyerResponseId);
        });

        //check empty
        if (this.billingAddresses.length == 0) {
            //load
            this.addressLoading = true;
            console.log('loading');
            this.lawyerSide_S.loadBillingAddresses(this.currentUser);
        } else {

        }

        if (this.userCards.length == 0) {
            //load
            this.cardLoading = true;
            this.payment_S.loadUserCards(this.currentUser);
        }

        if (this.allLawyerResponse.length == 0) {

            this.isLoading = true;
            this.lawyerSide_S.loadAllLawyerResponse(this.currentUser);
        } else {
            this.getCurrentResponse(this.lawyerResponseId);
        }
    }

    getCurrentResponse(responseId: string) {
        this.lawyerResponse = this.allLawyerResponse.find(response => response.id == responseId);
        console.log('get from all');
        console.log(this.lawyerResponse);
    }

    getAddressObj(address) {
        return address.get('addressObj');
    }

    ngOnDestroy() {
        this.billingAddressesSub.unsubscribe();
        this.currentUserSub.unsubscribe();
        this.allLawyerResponseSub.unsubscribe();
        this.userCardsSub.unsubscribe();
    }

    goBack() {
        this.router.navigateByUrl(`/${this.lang}/case-response/${this.lawyerResponseId}`);
    }

    addAddress() {
        console.log(this.inputAddress);
        this.isLoading = true;
        this.lawyerSide_S.addAddress(this.inputAddress, this.currentUser);
    }

    deleteAddress(address: any, index: number) {
        console.log(index);
        this.lawyerSide_S.deleteAddress(address, index);
    }

    selectAddress(address) {
        this.selectedAddress = address;
        $('#selectAddressModal').modal('toggle');
    }

    addCard() {
        console.log(this.inputCard);

        this.isLoading = true;
        this.addCardError = "";
        Omise.createToken("card", this.inputCard, (statusCode, response) => {
            if (statusCode == 200) {
                console.log(`Create token success, add card to user`);
                this.isLoading = false;
                let cardTokenId = response.id;

                this.payment_S.addCard(this.currentUser, cardTokenId);
            } else {
                this.isLoading = false;
                console.log(response.message);
                this.addCardError = response.message;
            }
        });
    }

    deleteCard(card: any, index: number) {//this cannot declare as Card class because card here is from omise, which has so many param.
        this.isLoading = true;

        this.payment_S.deleteCard(this.currentUser, card, index).then((deletedCard) => {
            console.log(`delete card success`);
            this.isLoading = false;

        }, (error) => {
            this.isLoading = false;

            console.log(error.message);
        });
    }

    selectCard(card: any) {
        this.selectedCard = card;
        $('#selectCardModal').modal('toggle');
    }

    toggleModal(modalID: string) {

        $(modalID).modal('toggle');
    }

    toConfirmPage() {

        if (this.selectedAddress && this.selectedCard) {
            console.log(this.lawyerResponse);
            this.payment_S.setCardDetail(this.lawyerResponse, this.selectedCard, this.selectedAddress);

            this.router.navigateByUrl(`/${this.lang}/case-confirm-payment/${this.lawyerResponse.id}`);


        } else {
            alert(`Please select both credit card and billing address to proceed.`);
        }


    }

    getCardLogoUrl = function (card) {

        let brand = card.brand;

        if (brand == 'Visa') {
            return '../../../img/cards/visa_icon.png';
        } if (brand == 'MasterCard') {
            return '../../../img/cards/mc_accpt_103_png.png'
        } else {
            return '../../../img/cards/blank_creditcard.png'
        }
    }
}