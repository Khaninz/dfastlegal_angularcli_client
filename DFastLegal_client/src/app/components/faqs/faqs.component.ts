﻿import { Component, OnInit, OnDestroy } from '@angular/core';

import { AppLanguageService } from '../../services/app-language.service';
import { UserService } from '../../services/user.service';
import { ActivatedRoute } from '@angular/router';

import { Meta, Title } from '@angular/platform-browser';
import { TagsService } from '../../services/tags.service';

@Component({

    templateUrl: './faqs.component.html'

})

export class FAQSComponent {

    lang: string;
    params$: any;

    constructor(
        public txt: AppLanguageService,
        private user_S: UserService,
        private route: ActivatedRoute,
        private meta: Meta,
        private title: Title,
        private tags: TagsService

    ) {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');

            title.setTitle(`${tags.FAQS.title[this.lang]}`);
            meta.addTags([
                { name: 'author', content: `${this.tags.AUTHOR}` },
                { name: 'description', content: `${this.tags.FAQS.description[this.lang]}` },

            ]);
        })


    }

    ngOnInit() {


    }

    ngOnDestroy() {
    }

}