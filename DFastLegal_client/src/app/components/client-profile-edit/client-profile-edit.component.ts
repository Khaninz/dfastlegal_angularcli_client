﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Lawyer, Qualification } from '../../utilities/classes-definition';

import { UserService } from '../../services/user.service'
import { AppLanguageService } from '../../services/app-language.service';

import { EXPERTISES, TITLES, VERIFY_STATUS, DEGREE } from '../../utilities/constant';
import { Meta, Title } from '@angular/platform-browser';
import { TagsService } from '../../services/tags.service';


declare var Parse: any; //must be declare to be able to use Parse JS sdk.

@Component({
    selector: 'lawyer-profile-edit',
    templateUrl: './client-profile-edit.component.html',
})

export class ClientProfileEditComponent implements OnInit, OnDestroy {

    lang: string;
    expertiseObjects: any[];
    isLoading: boolean;
    isLoadingSub: any;
    verifyStatusObjects: any[];
    DEGREE: any[];
    params$: any;

    //user common property
    title: string;
    firstnameTH: string;
    firstnameEN: string;
    lastnameTH: string;
    lastnameEN: string;
    email: string;
    phoneNumber: string;
    profileImageURL: string = "img/empty-profile.jpg";
    currentPassword: string;

    //lawyer profile property
    expertise1: string;
    expertise2: string;
    expertise3: string;
    qualifications: any[] = [];
    experienceTH: string;
    experienceEN: string;
    verifyStatus: string;
    isVerified: boolean;
    useMatchingService: boolean;
    useListingService: boolean;
    //adding qualification
    addedDegree: string = "UNDER";
    addedInstitute: string;
    addedStudyField: string;
    shortDescTH: string;
    shortDescEN: string;

    //working object
    currentUser: any;
    currentUserSub: any;

    constructor(

        private userService: UserService,
        private router: Router,
        public txt: AppLanguageService,
        private route: ActivatedRoute,
        private meta: Meta,
        private titleTag: Title,
        private tags: TagsService

    ) {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');

            titleTag.setTitle(`${tags.CLIENT_PROFILE_EDIT.title[this.lang]}`);
            meta.addTags([
                { name: 'author', content: `${this.tags.AUTHOR}` },
                { name: 'description', content: `${this.tags.CLIENT_PROFILE_EDIT.description[this.lang]}` },

            ]);
        })


        this.currentUserSub = this.userService.currentUser$.subscribe((currentUser) => {

            //on client/lawyer profile edit only use subscription to change profile picture.
            if (currentUser) {
                var imageFile = currentUser.get("profilePicture");
                var imageFileURL = imageFile.url();
                this.profileImageURL = imageFileURL;
                this.isLoading = false;


            }

        });

        this.isLoadingSub = this.userService.isLoading$.subscribe((isLoading) => {

            this.isLoading = isLoading;
        });

    }

    ngOnInit() {


        this.currentUser = this.userService.getCurrentUser();
        this.displayBasicInfo();

    }

    ngOnDestroy() {
        this.currentUserSub.unsubscribe();
        this.isLoadingSub.unsubscribe();
        //this.lawyerProfileSub.unsubscribe();
    }



    //Utils
    displayBasicInfo() {
        console.log('displaying basic info...');
        this.title = this.currentUser.get("title");
        this.firstnameTH = this.currentUser.get("firstnameTH");
        this.firstnameEN = this.currentUser.get("firstnameEN");
        this.lastnameTH = this.currentUser.get("lastnameTH");
        this.lastnameEN = this.currentUser.get("lastnameEN");
        this.email = this.currentUser.get("username");
        this.phoneNumber = this.currentUser.get("phoneNumber");

        let imageFile = this.currentUser.get("profilePicture");
        if (imageFile) {
            this.profileImageURL = imageFile.url();
        }
    }


    saveProfile() {

        this.isLoading = true;

        this.currentUser.set("title", this.title);
        this.currentUser.set("firstnameTH", this.firstnameTH);
        this.currentUser.set("firstnameEN", this.firstnameEN);
        this.currentUser.set("lastnameTH", this.lastnameTH);
        this.currentUser.set("lastnameEN", this.lastnameEN);
        this.currentUser.set("username", this.email);
        this.currentUser.set("email", this.email);
        this.currentUser.set("phoneNumber", this.phoneNumber);


        this.currentUser.save().then(() => {
            this.isLoading = false;
        });
    }

    changeProfilePicture() {

        this.userService.changeProfilePicture();


        // var component = this;

        // var uploadElement = document.createElement("INPUT");
        // uploadElement.setAttribute("type", "file");
        // uploadElement.setAttribute("id", "chooseFileInput");
        // uploadElement.setAttribute("accept", "image/*");
        // // uploadElement.setAttribute("multiple","true");

        // uploadElement.click();

        // uploadElement.onchange = function (fileInput: any) {
        //     // console.log('file selected.')
        //     component.isLoading = true;
        //     var files = fileInput.target.files; //files input will always come in list wether single or multiple.
        //     var file = files[0];
        //     var fileURL = window.URL.createObjectURL(file);
        //     // console.log(fileURL);

        //     //create image var to contain src of file
        //     var image = new Image();
        //     image.src = fileURL;
        //     image.crossOrigin = "Anonymous";

        //     image.onload = function () {

        //         var canvas = <HTMLCanvasElement>document.createElement('CANVAS');
        //         let targetSize = 300;

        //         canvas.height = targetSize; 
        //         canvas.width = targetSize;

        //         let ctx = <CanvasRenderingContext2D>canvas.getContext('2d');

        //         let W = image.width;
        //         let H = image.height;

        //         if (W < H) {

        //             let gap = (H - W) / 2;
        //             ctx.drawImage(image, 0, gap, W, W, 0, 0, targetSize, targetSize);

        //         } else if (H < W) {

        //             let gap = (W - H) / 2;
        //             ctx.drawImage(image, gap, 0, H, H, 0, 0, targetSize, targetSize);


        //         } else {
        //             ctx.drawImage(image, 0, 0, targetSize, targetSize); 
        //         }

        //         var imageDataViaCanvas = canvas.toDataURL();

        //         var parseImage = new Parse.File("profile_image", { base64: imageDataViaCanvas });

        //         component.currentUser.set("profilePicture", parseImage);
        //         component.currentUser.save(null, {
        //             success: function (user: any) {
        //                 console.log('upload profile picture success');
        //                 component.isLoading = false;
        //                 var imageFile = user.get("profilePicture");
        //                 var imageFileURL = imageFile.url();
        //                 component.profileImageURL = imageFileURL;

        //             }, error: function (user: any, error: any) {
        //                 console.log('faile upload profile picture.')
        //                 console.log(error.message);
        //             }

        //         });
        //     }

        // }

    }

    //GET methods
    //getDegree(qualification: any):string {
    //    let degreeValue = qualification.get('degree');
    //    let degree = this.DEGREE.find(degree => degree.value === degreeValue);
    //    return degree.txt[this.lang];
    //}
    //getInstitute(qualification: any): string {
    //    return qualification.get('institute');
    //}
    //getStudyField(qualification: any): string {
    //    return qualification.get('studyField');
    //}

    //State change
    isProfileChange() {
        if (
            //this.useMatchingService != this.lawyerProfile.get("useMatchingService") ||
            //this.useListingService != this.lawyerProfile.get("useListingService") ||
            this.title != this.currentUser.get("title") ||
            this.firstnameTH != this.currentUser.get("firstnameTH") ||
            this.firstnameEN != this.currentUser.get("firstnameEN") ||
            this.lastnameTH != this.currentUser.get("lastnameTH") ||
            this.lastnameEN != this.currentUser.get("lastnameEN") ||
            this.email != this.currentUser.get('username') ||
            this.phoneNumber != this.currentUser.get("phoneNumber")
            //this.expertise1 != this.lawyerProfile.get("expertise1") ||
            //this.expertise2 != this.lawyerProfile.get("expertise2") ||
            //this.expertise3 != this.lawyerProfile.get("expertise3") ||
            //this.shortDescTH != this.lawyerProfile.get("shortDescTH") ||
            //this.shortDescEN != this.lawyerProfile.get("shortDescEN") ||
            //this.experienceTH != this.lawyerProfile.get("experienceTH") ||
            //this.experienceEN != this.lawyerProfile.get("experienceEN")
        ) {
            return true
        } else {
            return false
        }
    }

    resetUserPassword() {

        this.isLoading = true;

        let email = this.currentUser.get('username');
        console.log(`resetting password for ${email}`);
        this.userService.resetPassword(email).then((success) => {
            this.isLoading = false;

            alert(this.txt.FORGOT_PW_SENT[this.lang]);

        }, (error) => {
            this.isLoading = false;

            console.log(error.message);
        });

    }

    //navigation
    toHome() {
        let link = ['Home', {}]
        this.router.navigate(link);
    }
}