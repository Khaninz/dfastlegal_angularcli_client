﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

import { ParseObjService } from '../../services/parse-object.service';
import { ClientSideService } from '../../services/client-side.service';
import { UserService } from '../../services/user.service';
import { AppLanguageService } from '../../services/app-language.service';

import { EXPERTISES } from '../../utilities/constant';

@Component({
    selector: 'select-lawyers',
    templateUrl: './select-lawyers.component.html',
    styles: [`
     .selected {
      background-color: gray;
    }

    `]
})

export class SelectLawyersComponent implements OnInit, OnDestroy {

    //common prop in most components
    lang: string;
    params$: any;
    EXPERTISES: any[];
    isLoading: boolean;
    currentUser: any;
    caseId: string;

    //specific prop for this component
    lawyerProfiles: any[] = [];
    selectedLawyerProfilesId: Set<any>;
    currentClientCase: any;
    currentClientCaseSub: any;
    lawyersForCase: any[] = [];
    lawyersForCaseSub: any;

    //for pagination
    p: any;

    constructor(
        private route: ActivatedRoute,
        private parseObjService: ParseObjService,
        private router: Router,
        private clientSideService: ClientSideService,
        private userService: UserService,
        public txt: AppLanguageService
    ) {
    }

    ngOnInit() {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');
        })

        //constant
        this.EXPERTISES = EXPERTISES;
        this.caseId = this.route.snapshot.params['caseId'];
        this.isLoading = true;

        //1. get data from service
        this.currentUser = this.userService.getCurrentUser();
        this.currentClientCase = this.clientSideService.getCurrentCase();
        this.lawyersForCase = this.clientSideService.getLawyersForCase();
        console.log(this.lawyersForCase);
        // this.selectedLawyerProfilesId = this.clientSideService.getSelectedLawyersId();

        //2.subscriptions
        this.currentClientCaseSub = this.clientSideService.currentClientCase$.subscribe((clientCase) => {

            this.currentClientCase = clientCase;
            this.clientSideService.findLawyersForCase(this.currentClientCase).then(() => {

            });

            this.updateSelectedProfilesId(this.currentClientCase);
            //this.isLoading = false;

        }, (error) => { console.log(error.message) });

        this.lawyersForCaseSub = this.clientSideService.lawyersForCase$.subscribe((lawyerProfiles) => {
            this.lawyersForCase = lawyerProfiles;
            this.isLoading = false;
        }, (error) => { error.message });
        //!subscriptions


        //3.check
        if (this.currentUser) {
            console.log('have user!');
            if (this.currentClientCase) {

                this.clientSideService.findLawyersForCase(this.currentClientCase);
            } else {
                this.isLoading = true;
                console.log(`no case in service yet`);
                this.clientSideService.queryClientCase(this.currentUser);

            }
        } else {
            console.log('no user navigating home!');
            this.router.navigateByUrl(`/${this.lang}`);
        }

    }

    ngOnDestroy() {
        this.lawyersForCaseSub.unsubscribe();
        this.currentClientCaseSub.unsubscribe();
    }

    isInSelectedList(profile: any) {
        //if lawyer is in selected list marked return true;
        if (this.selectedLawyerProfilesId.has(profile.id)) {
            return true;
        } else {
            //else return false
            return false;
        }
    }

    toConfirmPage() {
        //if user has not selected yet, not allow to pass.

        this.selectedLawyerProfilesId = this.clientSideService.getSelectedLawyersId();

        console.log(this.selectedLawyerProfilesId.size);
        if (this.selectedLawyerProfilesId.size > 0) {
            //convert id to array of pointer first
            this.isLoading = true;
            this.clientSideService.toSummaryPage().then((clientCase) => {

                this.isLoading = false;

                this.router.navigateByUrl(`/${this.lang}/case-confirm/${clientCase.id}`);

            }, (error: any) => {
                this.isLoading = false;
                console.log(error.message);
            });
        } else {
            alert(this.txt.PLS_SELECT_LAWYER[this.lang]);
        }
    }

    toCaseDetail() {

        this.router.navigateByUrl(`/${this.lang}/case-detail-form`);
    }

    toLastQuestion() {
        console.log('goinng to last page');

        this.clientSideService.clearSelectedLawyerIds();

        this.isLoading = true;
        this.clientSideService.deleteLatestProgress().then(() => {
            this.isLoading = false;
            this.router.navigateByUrl(`/${this.lang}/questions`);
        }, (error) => {
            console.log(error.message);
        });

    }

    //UTILS
    updateSelectedProfilesId(clientCase: any) {
        let selectedLawyersProfile = clientCase.get('selectedLawyers');

        let selectedId = new Set();
        for (let profile of selectedLawyersProfile) {
            selectedId.add(profile.id)
        }

        this.selectedLawyerProfilesId = selectedId;
    }

    getProgressImage() {
        return `img/progress bar-2-${this.lang.toLocaleUpperCase()}.svg`;
    }
}