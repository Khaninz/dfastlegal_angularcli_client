﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title, Meta} from'@angular/platform-browser';

import { AppLanguageService } from '../../services/app-language.service';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import {TagsService}from'../../services/tags.service';

@Component({

    templateUrl: './our-services.component.html'

})

export class OurServicesComponent {

    lang: string;
    params$: any;

    constructor(
        public txt: AppLanguageService,
        private user_S: UserService,
        private router: Router,
        private route: ActivatedRoute,
        private meta:Meta,
        private title:Title,
        private tags:TagsService
    ) {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');

            title.setTitle(`${tags.SERVICES.title[this.lang]}`);
            meta.addTags([
                { name: 'author', content: `${this.tags.AUTHOR}` },
                { name: 'description', content: `${this.tags.SERVICES.description[this.lang]}` },
            ]);
        })

        console.log(`${this.tags.SERVICES.description[this.lang]}`);

    }

    ngOnInit() {


    }

    ngOnDestroy() {
    }



}