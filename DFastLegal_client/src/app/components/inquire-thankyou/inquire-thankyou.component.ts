﻿import {Component,OnInit,OnDestroy} from'@angular/core';

import {AppLanguageService} from'../../services/app-language.service';
import {UserService}from'../../services/user.service';

@Component({
    selector: 'inquire-thankyou',
    templateUrl: './inquire-thankyou.component.html'
})

export class InquireThankyouComponent implements OnInit, OnDestroy {

    langSub: any;
    lang: string;

    constructor(

        private userService: UserService,

        public txt: AppLanguageService
    ) { //call a service here, not New HeroService
    }

    ngOnInit() {
        this.lang = this.userService.getCurrentLang();
        this.langSub = this.userService.userLang$.subscribe((lang) => {
            this.lang = lang;
        });
    }

    ngOnDestroy() {
        this.langSub.unsubscribe();
    }

}