﻿import {Component, OnInit, OnDestroy}from'@angular/core';
import { ActivatedRoute, Params, Router} from '@angular/router';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.

import {ParseObjService}from'../../services/parse-object.service';
import {Qualification}from'../../utilities/classes-definition';
import {ClientSideService}from'../../services/client-side.service';
import {AppLanguageService}from'../../services/app-language.service';
import {UserService}from'../../services/user.service';
import {EXPERTISES, DEGREE}from'../../utilities/constant'
@Component({
    selector: 'lawyer-profile',
    templateUrl: './view-lawyer-profile.component.html'
    
    
})

export class ViewLawyerProfileComponent implements OnInit, OnDestroy {

    caseId: string;

    error: any;
    isLoading: boolean;


    lawyer: any;
    lawyerProfile: any;

    lang: string;
    params$:any;
    
    //for pagination
    p:any;

    constructor(
        private route: ActivatedRoute,
        private parseObjService: ParseObjService,
        private clientSideService: ClientSideService,
        public txt: AppLanguageService,
        private userService: UserService,
        private router: Router
    ) {

    }
    

    ngOnInit() {

        //subscribe to UserService
        this.params$ = this.route.paramMap;
        this.params$.subscribe((params)=>{

            this.lang = params.get('lang');
        })

        this.caseId = this.route.snapshot.params['caseId'];

        let profileId = this.route.snapshot.params['profileId'];
        
        console.log(profileId);
        this.lawyerProfile = this.clientSideService.getViewingLawyerInCase(profileId);

        if (this.lawyerProfile) {

        } else {
            this.toSelectLawyers();
        }

        console.log(this.lawyerProfile);
        console.log(this.lawyer);



    }

    ngOnDestroy() {
    }

    //navigation
    goBack():void{
        
        window.history.back();
    }

    selectLawyer() {
        let profileId = this.lawyerProfile.id;
        this.clientSideService.selectLawyerProfile(profileId);
    }

    toSelectLawyers() {
        let link = ['select-lawyers', this.caseId];
        this.router.navigate(link);
    }
    

}