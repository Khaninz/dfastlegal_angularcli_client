import {Component,OnInit,OnDestroy}from'@angular/core';

import {AppLanguageService}from'../../services/app-language.service';
import {UserService}from'../../services/user.service';

@Component({
    selector: 'footer',
    templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit, OnDestroy { 

    lang: string;
    langSub: any;

    constructor(
        private userService: UserService,
        public txt: AppLanguageService
    ) {

    }

    ngOnInit() {

        this.lang = this.userService.getCurrentLang();

        this.langSub = this.userService.userLang$.subscribe((lang) => {
            this.lang = lang;
        });
    }

    ngOnDestroy() {
        this.langSub.unsubscribe();
    }
}