﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../../services/user.service';
import { ParseObjService } from '../../services/parse-object.service';
import { ClientSideService } from '../../services/client-side.service';
import { AppLanguageService } from '../../services/app-language.service';

import { PROVINCES, PROVINCES_W_ALL } from '../../utilities/constant';

declare var Parse: any; //must be declare to be able to use Parse JS sdk.
declare var $: any; //for jquery


@Component({
    selector: 'case-detail-form',
    templateUrl: './case-detail-form.component.html',
})

export class CaseDetailFormComponent implements OnInit, OnDestroy {

    isLoading: boolean;

    lang: string;
    params$: any;

    currentUser: any;
    currentUserSub: any;
    currentClientCase: any;
    currentClientCaseSub: any;
    numberRange: string[];
    provinces: string[];
    selectedProvince: string;
    clientProgress: any[];

    caseDetail: string;
    caseProvinces: string[];
    caseLawyerNumber: number;
    caseLawyerNumberString: string; //because value from <select> always return as string....
    testValue: any;


    progresses: any[];

    constructor(
        private userService: UserService,
        private router: Router,
        private parseObjServ: ParseObjService,
        private clientSideService: ClientSideService,
        public txt: AppLanguageService,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {

        this.params$ = this.route.paramMap;
        this.params$.subscribe((params) => {

            this.lang = params.get('lang');
        })

        this.numberRange = ['1', '2', '3', '4', '5'];
        this.provinces = PROVINCES;
        this.caseLawyerNumberString = '3';
        this.selectedProvince = 'BANGKOK';

        //1. get from service
        this.currentUser = this.userService.getCurrentUser();

        this.currentClientCase = this.clientSideService.getCurrentCase();

        //2. subscribe to data in service
        this.currentClientCaseSub = this.clientSideService.currentClientCase$.subscribe((clientCase) => {

            console.log('observer working');
            this.currentClientCase = clientCase;
            this.clientProgress = this.currentClientCase.get('questionsProgress');

            this.isLoading = false;

            if (this.clientProgress.length >= 0) {

                //display data
                this.caseDetail = this.parseObjServ.getCaseDetail(this.currentClientCase);
                let selectedProvinces = this.parseObjServ.getCaseProvinces(this.currentClientCase);//array of provinces, for future use
                this.selectedProvince = selectedProvinces[0];
                this.caseLawyerNumber = this.currentClientCase.get('preferredLawyer');
                this.caseLawyerNumberString = this.caseLawyerNumber.toString();//drop down value only accept string
                this.isLoading = false;

            } else {

                // this.toQuestions();
            }

        });

        this.currentUserSub = this.userService.currentUser$.subscribe((currentUser) => {
            this.currentUser = currentUser;
            //get case created by user
            this.clientSideService.queryClientCase(this.currentUser);            

        });

        //3. check
        if (this.currentUser) {//if there is user and has progress

            if (this.currentClientCase) {//if current case in service
                console.log('already has case');
                this.clientProgress = this.currentClientCase.get('questionsProgress');

                if (this.clientProgress.length >= 0) {

                    //display data
                    this.caseDetail = this.parseObjServ.getCaseDetail(this.currentClientCase);
                    let selectedProvinces = this.parseObjServ.getCaseProvinces(this.currentClientCase);//array of provinces, for future use
                    this.selectedProvince = selectedProvinces[0];
                    this.caseLawyerNumber = this.currentClientCase.get('preferredLawyer');
                    this.caseLawyerNumberString = this.caseLawyerNumber.toString();//drop down value only accept string
                    this.isLoading = false;

                } else {

                    this.toQuestions();
                }

            } else {
                this.isLoading = true;
                this.clientSideService.queryClientCase(this.currentUser);
            }

            // this.isLoading = true;
            // this.clientSideService.getExistingClientCase(this.currentUser).then(() => {

            //     this.currentClientCase = this.clientSideService.getCurrentCase();
            //     if (this.currentClientCase) {
            //         console.log('updating info');
            //         this.caseDetail = this.parseObjServ.getCaseDetail(this.currentClientCase);
            //         let selectedProvinces = this.parseObjServ.getCaseProvinces(this.currentClientCase);
            //         this.selectedProvince = selectedProvinces[0];
            //         this.caseLawyerNumber = this.parseObjServ.getCaseLawyerNumber(this.currentClientCase);
            //         this.caseLawyerNumberString = this.caseLawyerNumber.toString();
            //         this.isLoading = false;
            //     } else {
            //         console.log(`[case-detail-form.component]: no client case return`);
            //         this.isLoading = false;
            //     }
            // }, (error: any) => {
            //     console.log(error.message)
            // });


        } else {
            //no current user
            //or no progress
            //direct to home.

        }

    }

    ngOnDestroy() {
        this.currentClientCaseSub.unsubscribe();
    }

    submitCaseDetail() {

        if (this.currentUser) {
            console.log(this.caseDetail);
            console.log(this.selectedProvince);


            let selectedProvinces: string[] = [this.selectedProvince]; //for future feature, eg. multi provinces for case.
            console.log(selectedProvinces);
            this.caseLawyerNumber = parseInt(this.caseLawyerNumberString);
            console.log(this.caseLawyerNumber);

            this.isLoading = true;
            this.clientSideService.submitCaseDetail(this.currentClientCase, this.currentUser, this.caseDetail, selectedProvinces, this.caseLawyerNumber)
                .then(() => {

                    let clientCase = this.clientSideService.getCurrentCase();


                    // this.router.navigate(['/select-lawyers', clientCase.id]);
                    this.router.navigateByUrl(`/${this.lang}/select-lawyers/${clientCase.id}`);
                    this.isLoading = false;

                }, (error: any) => {
                    this.isLoading = false;
                    console.log(error.message);
                });

        } else {
            //no user
            //sign up modal
            alert(this.txt.PLEASE_LOGIN[this.lang]);
            $('#signUpModal').modal('toggle');

        }



    }

    draftCaseDetail() {//similar to submit but no redirect


        if (this.currentUser) {

            console.log(this.caseDetail);
            console.log(this.selectedProvince);


            let selectedProvinces: string[] = [this.selectedProvince]; //for future feature, eg. multi provinces for case.
            console.log(selectedProvinces);
            this.caseLawyerNumber = parseInt(this.caseLawyerNumberString);
            console.log(this.caseLawyerNumber);

            this.isLoading = true;
            this.clientSideService.submitCaseDetail(this.currentClientCase, this.currentUser, this.caseDetail, selectedProvinces, this.caseLawyerNumber)
                .then(() => {

                    let clientCase = this.clientSideService.getCurrentCase();


                    this.isLoading = false;
                    //this.router.navigate(['/select-lawyers', clientCase.id]);

                }, (error: any) => {
                    this.isLoading = false;
                    console.log(error.message);
                });


        } else {
            //no user
            //sign up modal
            alert(this.txt.PLEASE_LOGIN[this.lang]);
            $('#signUpModal').modal('toggle');

        }

    }

    // getCase() {
    //     if (this.currentClientCase) {
    //         console.log('already has case, update it');
    //         return this.currentClientCase;
    //     } else {
    //         console.log('creating new case...');
    //         let ClientCases = Parse.Object.extend('ClientCases');
    //         let clientCase = new ClientCases();
    //         return clientCase;
    //     }
    // }

    toHome() {
        // let link = ['/'];
        // this.router.navigate(link);
        this.router.navigateByUrl(`/${this.lang}`);
    }

    toQuestions() {

        //clear selected Lawyers Id in service, because user can change their case type


        // let link = ['/questions'];
        // this.router.navigate(link);
        this.router.navigateByUrl(`/${this.lang}/questions`);

    }

    // toLastQuestion() {
    //     console.log('goinng to last page');

    //     this.clientSideService.clearSelectedLawyerIds();

    //     this.isLoading = true;
    //     this.clientSideService.toPreviousQuestion().then(() => {
    //         this.isLoading = false;
    //         this.router.navigateByUrl(`/${this.lang}/questions`);
    //     }, (error) => {
    //         console.log(error.message);
    //     });

    // }

    getProgressImage() {
        return `img/progress bar-1-${this.lang.toUpperCase()}.svg`;
    }

}