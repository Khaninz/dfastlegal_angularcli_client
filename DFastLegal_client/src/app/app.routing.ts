﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ClientInquireComponent } from './components/client-inquire/client-inquire.component';
import { InquireThankyouComponent } from './components/inquire-thankyou/inquire-thankyou.component';
import { LawyerInquireComponent } from './components/lawyer-inquire/lawyer-inquire.component';
import { LawyersComponent } from './components/lawyers/lawyers.component';
import { LawyerProfileComponent } from './components/lawyer-profile/lawyer-profile.component';
import { LawyerSignUpComponent } from './components/lawyer-signup/lawyer-signup.component';
import { LawyerProfileEditComponent } from './components/lawyer-profile-edit/lawyer-profile-edit.component';
import { ClientProfileEditComponent } from './components/client-profile-edit/client-profile-edit.component';
import { QuestionsComponent } from './components/questions/questions.component';
import { CaseDetailFormComponent } from './components/case-detail-form/case-detail-form.component';
import { SelectLawyersComponent } from './components/select-lawyers/select-lawyers.component';
import { ViewLawyerProfileComponent } from './components/view-lawyer-profile/view-lawyer-profile.component';
import { CaseConfirmComponent } from './components/case-confirm/case-confirm.component';
import { CaseIssuedComponent } from './components/case-issued/case-issued.component';
import { ClientCasesComponent } from './components/client-cases/client-cases.component';
import { CaseDetailComponent } from './components/case-detail/case-detail.component';
import { CaseResponseComponent } from './components/case-response/case-response.component';
import { CaseDeclinedComponent } from './components/case-declined/case-declined.component';
import { CaseSelectPaymentComponent } from './components/case-select-payment/case-select-payment.component';
import { CaseConfirmPaymentComponent } from './components/case-confirm-payment/case-confirm-payment.component';
import { PaymentSuccessComponent } from './components/payment-success/payment-success.component';
import { LawyerCaseComponent } from './components/lawyer-case/lawyer-case.component';
import { SelectedLawyerProfileComponent } from './components/selected-lawyer-profile/selected-lawyer-profile.component';
import { HelpAndSupportComponent } from './components/help-and-support/help-and-support.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { OurServicesComponent } from './components/our-services/our-services.component';
import { PricingComponent } from './components/pricing/pricing.component';
import { FAQSComponent } from './components/faqs/faqs.component';
import { UnsubscribedComponent } from './components/unsubscribed/unsubscribed.component';


const appRoutes: Routes = [

    //redirect
    { path: '', redirectTo: '/th', pathMatch: 'full' },    
    { path: 'client-cases', redirectTo: '/th/client-cases', pathMatch: 'full' },    
    { path: 'case-response/:responseId', redirectTo: '/th/case-response/:responseId', pathMatch: 'full' },    
    
    
    //Common user route
    { path: ':lang', component: HomeComponent },
    { path: ':lang/help-and-support/:content', component: HelpAndSupportComponent },
    { path: ':lang/forgot-password', component: ForgotPasswordComponent },
    { path: ':lang/services', component: OurServicesComponent },
    { path: ':lang/pricing', component: PricingComponent },
    { path: ':lang/faqs', component: FAQSComponent },
    { path: ':lang/unsubscribed', component: UnsubscribedComponent },
    //client user route
    { path: ':lang/client-profile-edit', component: ClientProfileEditComponent },
    { path: ':lang/questions', component: QuestionsComponent },
    { path: ':lang/case-detail-form', component: CaseDetailFormComponent },
    { path: ':lang/select-lawyers/:caseId', component: SelectLawyersComponent },
    { path: ':lang/view-lawyer-profile/:caseId/:profileId', component: ViewLawyerProfileComponent },
    { path: ':lang/case-confirm/:caseId', component: CaseConfirmComponent },
    { path: ':lang/case-issued', component: CaseIssuedComponent },
    { path: ':lang/client-cases', component: ClientCasesComponent },
    { path: ':lang/lawyers', component: LawyersComponent },
    { path: ':lang/lawyer-profile/:profileId', component: LawyerProfileComponent },
    { path: ':lang/selected-lawyer-profile/:profileId', component: SelectedLawyerProfileComponent },    
    { path: ':lang/case-detail/:caseId', component: CaseDetailComponent },    
    
    //lawyer user route
    { path: ':lang/lawyer-signup', component: LawyerSignUpComponent },
    { path: ':lang/lawyer-profile-edit', component: LawyerProfileEditComponent },
    { path: ':lang/case-response/:responseId', component: CaseResponseComponent },
    { path: ':lang/case-declined', component: CaseDeclinedComponent },
    { path: ':lang/case-select-payment/:responseId', component: CaseSelectPaymentComponent },
    { path: ':lang/case-confirm-payment/:responseId', component: CaseConfirmPaymentComponent },
    { path: ':lang/payment-success/:responseId', component: PaymentSuccessComponent },
    { path: ':lang/lawyer-case', component: LawyerCaseComponent },



];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);